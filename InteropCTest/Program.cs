﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using SimpleJni;

namespace InteropCTest
{
    public class Program
    {
        public delegate void hello();

        public delegate void hello1(string name);

        public unsafe delegate void hello2(char* name);

        public unsafe delegate void hello3(byte* name);

        public struct Point
        {
            public int x;
            public int y;
        }

        public delegate void fire1(string command, Point[] points);
        public unsafe delegate void fire2(string command, Point* points);
        public delegate void fire3(string command, params Point[] points);
        
        
        [return: MarshalAs(UnmanagedType.LPStr)]
        public delegate string goodbye1(string name);
        public unsafe delegate char* goodbye2(string name);
        public unsafe delegate byte* goodbye3(string name);

        public unsafe delegate void helloOut(int* result);

        public static void Main()
        {
            var library = DynamicLibrary.Load("../../../interop_cs_test/cmake-build-debug/libinterop_cs_test.dll");
            library.GetProc<hello>().Invoke();
            library.GetProc<hello1>().Invoke("Name1");
            unsafe
            {
                fixed (char* name = "Name2") library.GetProc<hello2>().Invoke(name);
            }

            unsafe
            {
                fixed (byte* name = Encoding.UTF8.GetBytes("Name3")) library.GetProc<hello3>().Invoke(name);
            }

            library.GetProc<fire1>().Invoke("Attack1", new[]
            {
                new Point {x = 1, y = 2},
                new Point {x = 3, y = 4},
                new Point {x = 5, y = 6},
                new Point()
            });

            unsafe
            {
                Point[] points = {
                    new Point {x = 1, y = 2},
                    new Point {x = 3, y = 4},
                    new Point {x = 5, y = 6},
                    new Point()
                };
                fixed(Point* pts = points) library.GetProc<fire2>().Invoke("Attack2", pts);
            }
            
            library.GetProc<fire3>().Invoke("Attack3", new[]
            {
                new Point {x = 1, y = 2},
                new Point {x = 3, y = 4},
                new Point {x = 5, y = 6},
                new Point()
            });
            
            // crashes
            // Console.WriteLine(library.GetProc<goodbye1>().Invoke("1"));
            unsafe
            {
                var chars = new char[50];
                var s = library.GetProc<goodbye2>().Invoke("2");
                Marshal.Copy(new IntPtr(s), chars, 0, 50);
                Console.WriteLine(new string(s));
            }
            unsafe
            {
                var bytes = new byte[50];
                var s = library.GetProc<goodbye3>().Invoke("3");
                Marshal.Copy(new IntPtr(s), bytes, 0, 50);
                Console.WriteLine(Encoding.ASCII.GetString(bytes));
            }

            unsafe
            {
                int result = 0;
                library.GetProc<helloOut>().Invoke(&result);
                Console.WriteLine(result);
            }
            
        }
    }
}