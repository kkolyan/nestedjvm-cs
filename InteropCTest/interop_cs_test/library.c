#include "library.h"

#include <stdio.h>
#include <stdlib.h>

void hello(void) {
    printf("Hello, World!\n");
}

void hello1(const char *name) {
    printf("Hello, %s!\n", name);
}

void hello2(const char *name) {
    hello1(name);
}

void hello3(const char *name) {
    hello1(name);
}

typedef struct {
    int x;
    int y;
} Point;

const char *goodbye1(const char *name);

void fire1(const char *command, Point *points) {
    while (*(int *) points) {
        printf("fire %s {%d, %d}\n", command, points->x, points->y);
        points++;
    }
}

void fire2(const char *command, Point *points) {
    fire1(command, points);
}

void fire3(const char *command, Point *points) {
    fire1(command, points);
}

char buffer[1024];

const char *goodbye1(const char *name) {
    sprintf(buffer, "Hello, %s!\n", name);
    return buffer;
}

const char* goodbye2(const char *name) {
    return goodbye1(name);
}

const char* goodbye3(const char *name) {
    return goodbye1(name);
}

void helloOut(int* result) {
    *result = 42;
}
