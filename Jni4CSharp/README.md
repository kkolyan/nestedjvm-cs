
# Summary

**Jni4CSharp** is C# library to interoperate with Java via Java Native Interface (JNI). 

## Architecture
Consists of two layers:

1.  Bindings for JNI - straightforward JNI bindings, that simply delegates 
all calls to JVM library without any sugar and conversion, providing C-like 
experience when working with JNI.
2.  Decorated JNI - fluent wrappers above previous layer, 
that allow to manipulate Java objects with minimal amount of code in style of
`java.util.reflect` or `System.Reflect`.

## Comparison with **jni4net**
This library was greatly inspired by [**jni4net**](https://github.com/jni4net/jni4net/).

_Advantages of **jni4net**:_

1.  **jni4net** offers typesafe proxy implementation of Java standard library 
for C# and vice versa (you can literally call java code from C# and vice versa). Jni4CSharp does not.
2.  **jni4net** offers a mechanism to generate typesafe proxies of custom Java classes
to use in C# and vice versa.

_Advantages of  Jni4CSharp:_

1.  Jni4CSharp doesn't require JVM to have special classes in classpath.
 **jni4net** have a Java part, that should be deployed to embedded Java runtime.
3.  Jni4CSharp Decorated API calls Java methods with minimal overhead comparing to **jni4net**'s proxies of Java Reflection API.
2.  Jni4CSharp offers direct access to very close-to-C bindings. **jni4net**'s bindings are
 not so straightforward and are hidden as `internal`.
3.  **jni4net** doesn't work on Java 9+.

## Usage
Just add `Jni4CSharp.dll` to you project as library and do the same as done in
examples ([Jni4CSharp_Showcase](../Jni4CSharp_Showcase/) and [Jni4CSharp_xUnit](../Jni4CSharp_xUnit/) modules).
