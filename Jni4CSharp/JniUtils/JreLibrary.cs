using Jni4CSharp.CommonUtils;

namespace Jni4CSharp.JniUtils
{
    public static class JreLibrary
    {
        public static DynamicLibrary Load(string jreHome)
        {
            return DynamicLibrary.Load(jreHome + "/bin/server/jvm.dll");
        }
    }
}