using System;

namespace Jni4CSharp.JniUtils
{
    public class JavaThrowable : Exception
    {
        public JavaThrowable(string message) : base(message)
        {
        }
    }
}