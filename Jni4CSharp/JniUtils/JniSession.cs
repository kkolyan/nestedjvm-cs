using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Jni4CSharp.Bindings;
using Jni4CSharp.CommonUtils;

namespace Jni4CSharp.JniUtils
{
    public class JniSession
    {
        
        public readonly JNIEnv Env;
        public readonly JavaVM Vm;

        private JniSession(JNIEnv env, JavaVM vm)
        {
            Env = env;
            Vm = vm;
        }
        
        public static JniSession DetectExistingJvm(DynamicLibrary jvmLibrary)
        {
            Console.WriteLine("DetectExistingJvm");
            var invocationApiWrapper = new InvocationAPI(jvmLibrary);
            unsafe
            {
                JavaVM_* vm;
                int foundVms;
                
                Console.WriteLine("JNI_GetCreatedJavaVMs");
                var result = invocationApiWrapper.JNI_GetCreatedJavaVMs(&vm, 1, &foundVms);
                if (result != JniResult.JNI_OK)
                {
                    throw new Exception("JVM lookup failed: " + result);
                }

                if (foundVms <= 0)
                {
                    return null;
                }

                if (vm == null)
                {
                    throw new Exception("vm is null");
                }

                JNIEnv_* env;
                var javaVmWrapper = new JavaVM(vm);
                
                Console.WriteLine("AttachCurrentThread");
                javaVmWrapper.AttachCurrentThread(&env, null);

                if (env == null)
                {
                    throw new Exception("env is null");
                }

                return new JniSession(new JNIEnv(env), javaVmWrapper);
            }
        }

        public static JniSession CreateJvm(DynamicLibrary jvmLibrary, IList<string> jvmOpts)
        {
            Console.WriteLine("CreateJvm");
            var invocationApiWrapper = new InvocationAPI(jvmLibrary);
            unsafe
            {
                JavaVM_* vm;
                JNIEnv_* env;
                JniResult result;

                var args = new JavaVMInitArgs();
                fixed (JavaVMOption* options = new JavaVMOption[jvmOpts.Count])
                {
                    for (int i = 0; i < jvmOpts.Count; i++)
                    {
                        options[i].optionString = (byte*) Marshal.StringToHGlobalAnsi(jvmOpts[i]).ToPointer();
                    }

                    args.version = JniVersion.JNI_VERSION_1_8;
                    args.nOptions = jvmOpts.Count;
                    args.options = options;
                    args.ignoreUnrecognized = jboolean.JNI_FALSE;

                    Console.WriteLine("JNI_CreateJavaVM");
                    result = invocationApiWrapper.JNI_CreateJavaVM(&vm, &env, &args);
                    if (result != JniResult.JNI_OK)
                    {
                        throw new Exception("JVM creation failed: " + result);
                    }
                }

                if (vm == null)
                {
                    throw new Exception("vm is null");
                }

                if (env == null)
                {
                    throw new Exception("env is null");
                }

                var javaVmWrapper = new JavaVM(vm);
                return new JniSession(
                    new JNIEnv(env),
                    javaVmWrapper
                );
            }
        }
    }
}