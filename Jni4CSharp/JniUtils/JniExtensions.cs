using System.Text;
using Jni4CSharp.Bindings;

namespace Jni4CSharp.JniUtils
{
    public static class JniExtensions
    {
        /**
         * C# doesn't have required by Modified UTF-8 encoder, which is required by these JNI methods.
         * So let's just restrict calls to these methods to ASCII character set. For multibyte strings - just
         * create them on JVM side and use byte arrays for interop.
         */
        public static unsafe jstring NewStringAnsi(this JNIEnv env, string text)
        {
            var array = Encoding.ASCII.GetBytes(text);
            fixed (byte* bytes = array)
                return env.NewStringUTF(bytes);
        }

        public static unsafe void EscalateException(this JNIEnv env)
        {
            env.PushLocalFrame(32);
            var ex = env.ExceptionOccurred();
            if (ex.IsNull())
            {
                return;
            }

            env.ExceptionClear();

            var buffer_class = env.FindClass("java/io/ByteArrayOutputStream");
            var buffer = env.NewObject("java/io/ByteArrayOutputStream", "()V");
            var encoding = env.NewStringAnsi("UTF-8");
            var writer = env.NewObject(
                "java/io/PrintStream",
                "(Ljava/io/OutputStream;ZLjava/lang/String;)V",
                new jvalue(buffer),
                new jvalue(1),
                new jvalue(encoding)
            );
            env.DeleteLocalRef(encoding);

            var clsEx = env.FindClass("java/lang/Throwable");
            var ex_printStackTrace = env.GetMethodID(clsEx, "printStackTrace", "(Ljava/io/PrintStream;)V");
            fixed (jvalue* args = new[] {new jvalue(writer)})
                env.CallVoidMethodA(ex, ex_printStackTrace, args);

            var buf_toArray = env.GetMethodID(buffer_class, "toByteArray", "()[B");
            var traceBytes = env.CallObjectMethodA(buffer, buf_toArray, null).AsByteArray();
            var length = env.GetArrayLength(traceBytes);

            byte[] bytes = new byte[length];
            fixed (byte* b = bytes)
                env.GetByteArrayRegion(traceBytes, 0, length, b);

            env.PopLocalFrame(jobject.NULL);

            throw new JavaThrowable($"Java exception detected: {Encoding.UTF8.GetString(bytes)}");
        }

        public static unsafe string GetText(this JNIEnv env, jstring s)
        {
            var utf8 = env.NewStringAnsi("UTF-8");
            var cls_String = env.FindClass("java/lang/String");
            var method_getBytes = env.GetMethodID(cls_String, "getBytes", "(Ljava/lang/String;)[B");
            jbyteArray j_byteArray;
            fixed (jvalue* pargs = new[] {new jvalue(utf8)})
                j_byteArray = env.CallObjectMethodA(s, method_getBytes, pargs).AsByteArray();
            env.DeleteLocalRef(utf8);
            var length = env.GetArrayLength(j_byteArray);
            byte[] byteArray = new byte[length];
            fixed (byte* b = byteArray)
                env.GetByteArrayRegion(j_byteArray, 0, length, b);
            
            env.EscalateException();
            
            return Encoding.UTF8.GetString(byteArray);
        }

        public static unsafe jobject NewObject(this JNIEnv env, string jniClassName, string constructorSignature,
            params jvalue[] args)
        {
            var cls = env.FindClass(jniClassName);
            var ctor = env.GetMethodID(cls, "<init>", constructorSignature);
            fixed (jvalue* argsPtr = args)
                return env.NewObjectA(cls, ctor, argsPtr);
        }
    }
}