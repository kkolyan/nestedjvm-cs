// ReSharper disable InconsistentNaming
// ReSharper disable CommentTypo
// ReSharper disable IdentifierTypo
// ReSharper disable UnusedMember.Global

using System.Runtime.InteropServices;
// ReSharper disable once UnusedType.Global

using jbyte = System.Byte;
using jshort = System.Int16;
using jint = System.Int32;
using jlong = System.Int64;
using jfloat = System.Single;
using jdouble = System.Double;
using jchar = System.UInt16;
using jsize = System.Int32;
// ReSharper disable BuiltInTypeReferenceStyle

namespace Jni4CSharp.Bindings
{
  
//  Regex used to ocnvert original jni.h:
// ([A-z][A-z\ \*]+)\(JNICALL \*([A-z]+)\)(\s*)\((.*[\n]?.*)\);
// public unsafe delegate $1 $2$3($4);
// /* $0  */
//   public void* $2;
    
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jint  GetVersion(JNIEnv_ *env);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jclass  DefineClass
      (JNIEnv_ *env, string name, jobject loader, jbyte *buf,
       jsize len);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jclass  FindClass
      (JNIEnv_ *env, string name);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jmethodID  FromReflectedMethod
      (JNIEnv_ *env, jobject method);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jfieldID  FromReflectedField
      (JNIEnv_ *env, jobject field);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jobject  ToReflectedMethod
      (JNIEnv_ *env, jclass cls, jmethodID methodID, jboolean isStatic);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jclass  GetSuperclass
      (JNIEnv_ *env, jclass sub);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jboolean  IsAssignableFrom
      (JNIEnv_ *env, jclass sub, jclass sup);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jobject  ToReflectedField
      (JNIEnv_ *env, jclass cls, jfieldID fieldID, jboolean isStatic);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jint  Throw
      (JNIEnv_ *env, jthrowable obj);
    /**
     * see comment at NewStringUTF in this file with explanation why byte* instead of string
     */
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jint  ThrowNew
      (JNIEnv_ *env, jclass clazz, byte* msg);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jthrowable  ExceptionOccurred
      (JNIEnv_ *env);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  ExceptionDescribe
      (JNIEnv_ *env);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  ExceptionClear
      (JNIEnv_ *env);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  FatalError
      (JNIEnv_ *env, string msg);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jint  PushLocalFrame
      (JNIEnv_ *env, jint capacity);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jobject  PopLocalFrame
      (JNIEnv_ *env, jobject result);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jobject  NewGlobalRef
      (JNIEnv_ *env, jobject lobj);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  DeleteGlobalRef
      (JNIEnv_ *env, jobject gref);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  DeleteLocalRef
      (JNIEnv_ *env, jobject obj);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jboolean  IsSameObject
      (JNIEnv_ *env, jobject obj1, jobject obj2);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jobject  NewLocalRef
      (JNIEnv_ *env, jobject _ref);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jint  EnsureLocalCapacity
      (JNIEnv_ *env, jint capacity);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jobject  AllocObject
      (JNIEnv_ *env, jclass clazz);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jobject  NewObject
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jobject  NewObjectV
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jobject  NewObjectA
      (JNIEnv_ *env, jclass clazz, jmethodID methodID, jvalue *args);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jclass  GetObjectClass
      (JNIEnv_ *env, jobject obj);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jboolean  IsInstanceOf
      (JNIEnv_ *env, jobject obj, jclass clazz);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jmethodID  GetMethodID
      (JNIEnv_ *env, jclass clazz, string name, string sig);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jobject  CallObjectMethod
    //   (JNIEnv *env, jobject obj, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jobject  CallObjectMethodV
    //   (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jobject  CallObjectMethodA
      (JNIEnv_ *env, jobject obj, jmethodID methodID, jvalue * args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jboolean  CallBooleanMethod
    //   (JNIEnv *env, jobject obj, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jboolean  CallBooleanMethodV
    //   (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jboolean  CallBooleanMethodA
      (JNIEnv_ *env, jobject obj, jmethodID methodID, jvalue * args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jbyte  CallByteMethod
    //   (JNIEnv *env, jobject obj, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jbyte  CallByteMethodV
    //   (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jbyte  CallByteMethodA
      (JNIEnv_ *env, jobject obj, jmethodID methodID, jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jchar  CallCharMethod
    //   (JNIEnv *env, jobject obj, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jchar  CallCharMethodV
    //   (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jchar  CallCharMethodA
      (JNIEnv_ *env, jobject obj, jmethodID methodID, jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jshort  CallShortMethod
    //   (JNIEnv *env, jobject obj, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jshort  CallShortMethodV
    //   (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jshort  CallShortMethodA
      (JNIEnv_ *env, jobject obj, jmethodID methodID, jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jint  CallIntMethod
    //   (JNIEnv *env, jobject obj, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jint  CallIntMethodV
    //   (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jint  CallIntMethodA
      (JNIEnv_ *env, jobject obj, jmethodID methodID, jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jlong  CallLongMethod
    //   (JNIEnv *env, jobject obj, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jlong  CallLongMethodV
    //   (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jlong  CallLongMethodA
      (JNIEnv_ *env, jobject obj, jmethodID methodID, jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jfloat  CallFloatMethod
    //   (JNIEnv *env, jobject obj, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jfloat  CallFloatMethodV
    //   (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jfloat  CallFloatMethodA
      (JNIEnv_ *env, jobject obj, jmethodID methodID, jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jdouble  CallDoubleMethod
    //   (JNIEnv *env, jobject obj, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jdouble  CallDoubleMethodV
    //   (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jdouble  CallDoubleMethodA
      (JNIEnv_ *env, jobject obj, jmethodID methodID, jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate void  CallVoidMethod
    //   (JNIEnv *env, jobject obj, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate void  CallVoidMethodV
    //   (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  CallVoidMethodA
      (JNIEnv_ *env, jobject obj, jmethodID methodID, jvalue * args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jobject  CallNonvirtualObjectMethod
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jobject  CallNonvirtualObjectMethodV
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
    //    va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jobject  CallNonvirtualObjectMethodA
      (JNIEnv_ *env, jobject obj, jclass clazz, jmethodID methodID,
       jvalue * args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jboolean  CallNonvirtualBooleanMethod
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jboolean  CallNonvirtualBooleanMethodV
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
    //    va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jboolean  CallNonvirtualBooleanMethodA
      (JNIEnv_ *env, jobject obj, jclass clazz, jmethodID methodID,
       jvalue * args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jbyte  CallNonvirtualByteMethod
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jbyte  CallNonvirtualByteMethodV
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
    //    va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jbyte  CallNonvirtualByteMethodA
      (JNIEnv_ *env, jobject obj, jclass clazz, jmethodID methodID,
       jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jchar  CallNonvirtualCharMethod
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jchar  CallNonvirtualCharMethodV
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
    //    va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jchar  CallNonvirtualCharMethodA
      (JNIEnv_ *env, jobject obj, jclass clazz, jmethodID methodID,
       jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jshort  CallNonvirtualShortMethod
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jshort  CallNonvirtualShortMethodV
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
    //    va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jshort  CallNonvirtualShortMethodA
      (JNIEnv_ *env, jobject obj, jclass clazz, jmethodID methodID,
       jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jint  CallNonvirtualIntMethod
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jint  CallNonvirtualIntMethodV
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
    //    va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jint  CallNonvirtualIntMethodA
      (JNIEnv_ *env, jobject obj, jclass clazz, jmethodID methodID,
       jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jlong  CallNonvirtualLongMethod
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jlong  CallNonvirtualLongMethodV
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
    //    va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jlong  CallNonvirtualLongMethodA
      (JNIEnv_ *env, jobject obj, jclass clazz, jmethodID methodID,
       jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jfloat  CallNonvirtualFloatMethod
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jfloat  CallNonvirtualFloatMethodV
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
    //    va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jfloat  CallNonvirtualFloatMethodA
      (JNIEnv_ *env, jobject obj, jclass clazz, jmethodID methodID,
       jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jdouble  CallNonvirtualDoubleMethod
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jdouble  CallNonvirtualDoubleMethodV
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
    //    va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jdouble  CallNonvirtualDoubleMethodA
      (JNIEnv_ *env, jobject obj, jclass clazz, jmethodID methodID,
       jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate void  CallNonvirtualVoidMethod
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate void  CallNonvirtualVoidMethodV
    //   (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
    //    va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  CallNonvirtualVoidMethodA
      (JNIEnv_ *env, jobject obj, jclass clazz, jmethodID methodID,
       jvalue * args);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jfieldID  GetFieldID
      (JNIEnv_ *env, jclass clazz, string name, string sig);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jobject  GetObjectField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jboolean  GetBooleanField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jbyte  GetByteField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jchar  GetCharField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jshort  GetShortField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jint  GetIntField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jlong  GetLongField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jfloat  GetFloatField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jdouble  GetDoubleField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetObjectField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID, jobject val);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetBooleanField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID, jboolean val);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetByteField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID, jbyte val);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetCharField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID, jchar val);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetShortField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID, jshort val);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetIntField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID, jint val);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetLongField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID, jlong val);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetFloatField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID, jfloat val);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetDoubleField
      (JNIEnv_ *env, jobject obj, jfieldID fieldID, jdouble val);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jmethodID  GetStaticMethodID
      (JNIEnv_ *env, jclass clazz, string name, string sig);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jobject  CallStaticObjectMethod
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jobject  CallStaticObjectMethodV
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jobject  CallStaticObjectMethodA
      (JNIEnv_ *env, jclass clazz, jmethodID methodID, jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jboolean  CallStaticBooleanMethod
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jboolean  CallStaticBooleanMethodV
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jboolean  CallStaticBooleanMethodA
      (JNIEnv_ *env, jclass clazz, jmethodID methodID, jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jbyte  CallStaticByteMethod
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jbyte  CallStaticByteMethodV
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jbyte  CallStaticByteMethodA
      (JNIEnv_ *env, jclass clazz, jmethodID methodID, jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jchar  CallStaticCharMethod
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jchar  CallStaticCharMethodV
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jchar  CallStaticCharMethodA
      (JNIEnv_ *env, jclass clazz, jmethodID methodID, jvalue *args);
    //
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jshort  CallStaticShortMethod
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jshort  CallStaticShortMethodV
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jshort  CallStaticShortMethodA
      (JNIEnv_ *env, jclass clazz, jmethodID methodID, jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jint  CallStaticIntMethod
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jint  CallStaticIntMethodV
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jint  CallStaticIntMethodA
      (JNIEnv_ *env, jclass clazz, jmethodID methodID, jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jlong  CallStaticLongMethod
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jlong  CallStaticLongMethodV
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jlong  CallStaticLongMethodA
      (JNIEnv_ *env, jclass clazz, jmethodID methodID, jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jfloat  CallStaticFloatMethod
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jfloat  CallStaticFloatMethodV
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jfloat  CallStaticFloatMethodA
      (JNIEnv_ *env, jclass clazz, jmethodID methodID, jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jdouble  CallStaticDoubleMethod
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate jdouble  CallStaticDoubleMethodV
    //   (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jdouble  CallStaticDoubleMethodA
      (JNIEnv_ *env, jclass clazz, jmethodID methodID, jvalue *args);

    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate void  CallStaticVoidMethod
    //   (JNIEnv *env, jclass cls, jmethodID methodID, ...);
    // [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    // public unsafe delegate void  CallStaticVoidMethodV
    //   (JNIEnv *env, jclass cls, jmethodID methodID, va_list args);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  CallStaticVoidMethodA
      (JNIEnv_ *env, jclass cls, jmethodID methodID, jvalue * args);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jfieldID  GetStaticFieldID
      (JNIEnv_ *env, jclass clazz, string name, string sig);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jobject  GetStaticObjectField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jboolean  GetStaticBooleanField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jbyte  GetStaticByteField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jchar  GetStaticCharField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jshort  GetStaticShortField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jint  GetStaticIntField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jlong  GetStaticLongField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jfloat  GetStaticFloatField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jdouble  GetStaticDoubleField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetStaticObjectField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID, jobject value);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetStaticBooleanField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID, jboolean value);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetStaticByteField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID, jbyte value);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetStaticCharField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID, jchar value);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetStaticShortField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID, jshort value);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetStaticIntField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID, jint value);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetStaticLongField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID, jlong value);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetStaticFloatField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID, jfloat value);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetStaticDoubleField
      (JNIEnv_ *env, jclass clazz, jfieldID fieldID, jdouble value);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jstring  NewString
      (JNIEnv_ *env, jchar *unicode, jsize len);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jsize  GetStringLength
      (JNIEnv_ *env, jstring str);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jchar * GetStringChars
      (JNIEnv_ *env, jstring str, jboolean *isCopy);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  ReleaseStringChars
      (JNIEnv_ *env, jstring str, jchar *chars);

    /**
     * byte* used instead of string intentionally. this argument should be "Modified UTF"
     * and very probably will contain non-ASCII symbols, so we can't just let CLR marshal it,
     * because it can't marshal this format. It's look a good idea to to avoid this method at all and pass create
     * strings on JVM side using passed byte arrays.
     *
     * Note that there are places where string type is used - all these places are usually used with ASCII
     * symbols only, so it's good tradeoff - relatively safe and convenient to call. 
     */
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jstring  NewStringUTF
      (JNIEnv_ *env, byte* utf);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jsize  GetStringUTFLength
      (JNIEnv_ *env, jstring str);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate byte*  GetStringUTFChars
      (JNIEnv_ *env, jstring str, jboolean *isCopy);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  ReleaseStringUTFChars
      (JNIEnv_ *env, jstring str, byte* chars);


    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jsize  GetArrayLength
      (JNIEnv_ *env, jarray array);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jobjectArray  NewObjectArray
      (JNIEnv_ *env, jsize len, jclass clazz, jobject init);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jobject  GetObjectArrayElement
      (JNIEnv_ *env, jobjectArray array, jsize index);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetObjectArrayElement
      (JNIEnv_ *env, jobjectArray array, jsize index, jobject val);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jbooleanArray  NewBooleanArray
      (JNIEnv_ *env, jsize len);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jbyteArray  NewByteArray
      (JNIEnv_ *env, jsize len);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jcharArray  NewCharArray
      (JNIEnv_ *env, jsize len);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jshortArray  NewShortArray
      (JNIEnv_ *env, jsize len);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jintArray  NewIntArray
      (JNIEnv_ *env, jsize len);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jlongArray  NewLongArray
      (JNIEnv_ *env, jsize len);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jfloatArray  NewFloatArray
      (JNIEnv_ *env, jsize len);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jdoubleArray  NewDoubleArray
      (JNIEnv_ *env, jsize len);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jboolean *  GetBooleanArrayElements
      (JNIEnv_ *env, jbooleanArray array, jboolean *isCopy);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jbyte *  GetByteArrayElements
      (JNIEnv_ *env, jbyteArray array, jboolean *isCopy);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jchar *  GetCharArrayElements
      (JNIEnv_ *env, jcharArray array, jboolean *isCopy);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jshort *  GetShortArrayElements
      (JNIEnv_ *env, jshortArray array, jboolean *isCopy);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jint *  GetIntArrayElements
      (JNIEnv_ *env, jintArray array, jboolean *isCopy);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jlong *  GetLongArrayElements
      (JNIEnv_ *env, jlongArray array, jboolean *isCopy);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jfloat *  GetFloatArrayElements
      (JNIEnv_ *env, jfloatArray array, jboolean *isCopy);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jdouble *  GetDoubleArrayElements
      (JNIEnv_ *env, jdoubleArray array, jboolean *isCopy);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  ReleaseBooleanArrayElements
      (JNIEnv_ *env, jbooleanArray array, jboolean *elems, ScalarArrayReleaseMode mode);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  ReleaseByteArrayElements
      (JNIEnv_ *env, jbyteArray array, jbyte *elems, ScalarArrayReleaseMode mode);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  ReleaseCharArrayElements
      (JNIEnv_ *env, jcharArray array, jchar *elems, ScalarArrayReleaseMode mode);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  ReleaseShortArrayElements
      (JNIEnv_ *env, jshortArray array, jshort *elems, ScalarArrayReleaseMode mode);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  ReleaseIntArrayElements
      (JNIEnv_ *env, jintArray array, jint *elems, ScalarArrayReleaseMode mode);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  ReleaseLongArrayElements
      (JNIEnv_ *env, jlongArray array, jlong *elems, ScalarArrayReleaseMode mode);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  ReleaseFloatArrayElements
      (JNIEnv_ *env, jfloatArray array, jfloat *elems, ScalarArrayReleaseMode mode);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  ReleaseDoubleArrayElements
      (JNIEnv_ *env, jdoubleArray array, jdouble *elems, ScalarArrayReleaseMode mode);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  GetBooleanArrayRegion
      (JNIEnv_ *env, jbooleanArray array, jsize start, jsize l, jboolean *buf);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  GetByteArrayRegion
      (JNIEnv_ *env, jbyteArray array, jsize start, jsize len, jbyte *buf);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  GetCharArrayRegion
      (JNIEnv_ *env, jcharArray array, jsize start, jsize len, jchar *buf);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  GetShortArrayRegion
      (JNIEnv_ *env, jshortArray array, jsize start, jsize len, jshort *buf);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  GetIntArrayRegion
      (JNIEnv_ *env, jintArray array, jsize start, jsize len, jint *buf);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  GetLongArrayRegion
      (JNIEnv_ *env, jlongArray array, jsize start, jsize len, jlong *buf);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  GetFloatArrayRegion
      (JNIEnv_ *env, jfloatArray array, jsize start, jsize len, jfloat *buf);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  GetDoubleArrayRegion
      (JNIEnv_ *env, jdoubleArray array, jsize start, jsize len, jdouble *buf);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetBooleanArrayRegion
      (JNIEnv_ *env, jbooleanArray array, jsize start, jsize l, jboolean *buf);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetByteArrayRegion
      (JNIEnv_ *env, jbyteArray array, jsize start, jsize len, jbyte *buf);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetCharArrayRegion
      (JNIEnv_ *env, jcharArray array, jsize start, jsize len, jchar *buf);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetShortArrayRegion
      (JNIEnv_ *env, jshortArray array, jsize start, jsize len, jshort *buf);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetIntArrayRegion
      (JNIEnv_ *env, jintArray array, jsize start, jsize len, jint *buf);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetLongArrayRegion
      (JNIEnv_ *env, jlongArray array, jsize start, jsize len, jlong *buf);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetFloatArrayRegion
      (JNIEnv_ *env, jfloatArray array, jsize start, jsize len, jfloat *buf);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  SetDoubleArrayRegion
      (JNIEnv_ *env, jdoubleArray array, jsize start, jsize len, jdouble *buf);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jint  RegisterNatives
      (JNIEnv_ *env, jclass clazz, JNINativeMethod *methods,
       jint nMethods);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jint  UnregisterNatives
      (JNIEnv_ *env, jclass clazz);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jint  MonitorEnter
      (JNIEnv_ *env, jobject obj);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jint  MonitorExit
      (JNIEnv_ *env, jobject obj);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate JniResult  GetJavaVM
      (JNIEnv_ *env, JavaVM_ **vm);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  GetStringRegion
      (JNIEnv_ *env, jstring str, jsize start, jsize len, jchar *buf);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  GetStringUTFRegion
      (JNIEnv_ *env, jstring str, jsize start, jsize len, byte *buf);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void *  GetPrimitiveArrayCritical
      (JNIEnv_ *env, jarray array, jboolean *isCopy);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  ReleasePrimitiveArrayCritical
      (JNIEnv_ *env, jarray array, void *carray, ScalarArrayReleaseMode mode);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jchar *  GetStringCritical
      (JNIEnv_ *env, jstring _string, jboolean *isCopy);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  ReleaseStringCritical
      (JNIEnv_ *env, jstring s, jchar *cstring);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jweak  NewWeakGlobalRef
       (JNIEnv_ *env, jobject obj);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void  DeleteWeakGlobalRef
       (JNIEnv_ *env, jweak _ref);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jboolean  ExceptionCheck
       (JNIEnv_ *env);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jobject  NewDirectByteBuffer
       (JNIEnv_* env, void* address, jlong capacity);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void*  GetDirectBufferAddress
       (JNIEnv_* env, jobject buf);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jlong  GetDirectBufferCapacity
       (JNIEnv_* env, jobject buf);
    //New JNI 1.6 Features 
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jobjectRefType  GetObjectRefType
        (JNIEnv_* env, jobject obj);
    
    
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate JniResult  DestroyJavaVM(JavaVM_ *vm);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate JniResult  AttachCurrentThread(JavaVM_ *vm, JNIEnv_ **penv, JavaVMAttachArgs *args);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate JniResult  DetachCurrentThread(JavaVM_ *vm);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate JniResult  GetEnv(JavaVM_ *vm, void **penv, jint version);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate JniResult  AttachCurrentThreadAsDaemon(JavaVM_ *vm, void **penv, void *args);
    
    
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate JniResult JNI_GetDefaultJavaVMInitArgs(void *args);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate JniResult JNI_CreateJavaVM(JavaVM_ **pvm, JNIEnv_ **penv, JavaVMInitArgs *args);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate JniResult JNI_GetCreatedJavaVMs(JavaVM_ **vmBuf, jsize bufLen, jsize * nVMs);

    /* Defined by native libraries. */
    
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate jint JNI_OnLoad(JavaVM_ *vm, void *reserved);

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate void JNI_OnUnload(JavaVM_ *vm, void *reserved);

}