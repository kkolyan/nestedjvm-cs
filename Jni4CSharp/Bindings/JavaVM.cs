using Jni4CSharp.Bindings.Functions;

namespace Jni4CSharp.Bindings
{
    // ReSharper disable InconsistentNaming
    // ReSharper disable CommentTypo
    // ReSharper disable IdentifierTypo
    // ReSharper disable UnusedMember.Global
    // ReSharper disable once UnusedType.Global

    using jint = System.Int32;

    public readonly unsafe struct JavaVM
    {
        private readonly JavaVM_* _vm;
        private readonly JavaVMFunctions _functions;

        public JavaVM(JavaVM_* vm)
        {
            _vm = vm;
            _functions = new JavaVMFunctions(vm);
        }
        
        public JniResult DestroyJavaVM() {
            return _functions.DestroyJavaVM(_vm);
        }
        public JniResult AttachCurrentThread(JNIEnv_ **penv, JavaVMAttachArgs *args) {
            return _functions.AttachCurrentThread(_vm, penv, args);
        }
        public JniResult DetachCurrentThread() {
            return _functions.DetachCurrentThread(_vm);
        }

        public JniResult GetEnv(void **penv, jint version) {
            return _functions.GetEnv(_vm, penv, version);
        }
        public JniResult AttachCurrentThreadAsDaemon(void **penv, void *args) {
            return _functions.AttachCurrentThreadAsDaemon(_vm, penv, args);
        }
    }
}