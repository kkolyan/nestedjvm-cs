using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// ReSharper disable BuiltInTypeReferenceStyle
// ReSharper disable EnumUnderlyingTypeIsInt

// ReSharper disable UnassignedField.Global
// ReSharper disable CommentTypo
// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable NotAccessedField.Global
// ReSharper disable UnusedType.Global

namespace Jni4CSharp.Bindings
{/*
 * Copyright (c) 1996, 2013, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

/*
 * We used part of Netscape's Java Runtime Interface (JRI) as the starting
 * point of our design and implementation.
 */

/******************************************************************************
 * Java Runtime Interface
 * Copyright (c) 1996 Netscape Communications Corporation. All rights reserved.
 *****************************************************************************/


/* jni_md.h contains the machine-dependent typedefs for jbyte, jint
   and jlong */

/*
 * JNI Types
 */

    using jbyte = System.SByte;
    using jshort = System.Int16;
    using jint = System.Int32;
    using jlong = System.Int64;
    using jfloat = System.Single;
    using jdouble = System.Double;
    using jchar = System.UInt16; // System.Char is not blittable

    public enum jboolean: System.Byte
    {
      JNI_FALSE = 0,
      JNI_TRUE = 1,
    }
    
    [NativeCppClass]
    public readonly struct jobject
    {
        private readonly int _value;
        
        public static readonly jobject NULL = new jobject(0);

        public override string ToString()
        {
          return $"{nameof(jobject)}(x{_value:x8})";
        }

        public int GetBinaryValue()
        {
          return _value;
        }

        public jobject(int value)
        {
            _value = value;
        }
        
        public bool IsNull()
        {
          return _value == 0;
        }

        public static implicit operator jobject(jclass v)
        {
            return new jobject(v._value);
        }

        public static implicit operator jobject(jthrowable v)
        {
            return new jobject(v._value);
        }

        public static implicit operator jobject(jstring v)
        {
            return new jobject(v._value);
        }

        public static implicit operator jobject(jarray v)
        {
            return new jobject(v._value);
        }
        
        public jclass AsClass()
        {
          return new jclass(_value);
        }
        
        public jthrowable AsThrowable()
        {
          return new jthrowable(_value);
        }

        public jstring AsString()
        {
          return new jstring(_value);
        }

        public jbyteArray AsByteArray()
        {
          return new jbyteArray(_value);
        }

        public jarray AsArray()
        {
          return new jarray(_value);
        }

        public jobjectArray AsObjectArray()
        {
          return new jobjectArray(_value);
        }

        public static implicit operator jobject(jbooleanArray v)
        {
          return new jarray(v._value);
        }

        public static implicit operator jobject(jbyteArray v)
        {
          return new jarray(v._value);
        }

        public static implicit operator jobject(jcharArray v)
        {
          return new jarray(v._value);
        }

        public static implicit operator jobject(jshortArray v)
        {
          return new jarray(v._value);
        }

        public static implicit operator jobject(jfloatArray v)
        {
          return new jarray(v._value);
        }

        public static implicit operator jobject(jintArray v)
        {
          return new jarray(v._value);
        }

        public static implicit operator jobject(jdoubleArray v)
        {
          return new jarray(v._value);
        }

        public static implicit operator jobject(jobjectArray v)
        {
          return new jarray(v._value);
        }

        public static implicit operator jobject(jlongArray v)
        {
          return new jarray(v._value);
        }
    }

    
    [NativeCppClass]
    public readonly struct jclass
    {
      internal readonly int _value;

      public jclass(int value)
      {
        _value = value;
      }

      public bool IsNull()
      {
        return _value == 0;
      }

      public override string ToString()
      {
        return $"{nameof(jclass)}(x{_value:x8})";
      }
    }

    
    [NativeCppClass]
    public readonly struct jthrowable
    {
      internal readonly int _value;

      public jthrowable(int value)
      {
          _value = value;
      }

      public bool IsNull()
      {
        return _value == 0;
      }

      public override string ToString()
      {
        return $"{nameof(jthrowable)}(x{_value:x8})";
      }
    }

    
    [NativeCppClass]
    public readonly struct jstring
    {
      internal readonly int _value;

      public jstring(int value)
      {
        _value = value;
      }

      public override string ToString()
      {
        return $"{nameof(jstring)}(x{_value:x8})";
      }
    }

    
    [NativeCppClass]
    public readonly struct jarray
    {
      internal readonly int _value;

      public jarray(int value)
      {
        _value = value;
      }

      public override string ToString()
      {
        return $"{nameof(jarray)}(x{_value:x8})";
      }

      public static implicit operator jarray(jbooleanArray v)
      {
          return new jarray(v._value);
      }

      public static implicit operator jarray(jbyteArray v)
      {
          return new jarray(v._value);
      }

      public static implicit operator jarray(jcharArray v)
      {
          return new jarray(v._value);
      }

      public static implicit operator jarray(jshortArray v)
      {
          return new jarray(v._value);
      }

      public static implicit operator jarray(jfloatArray v)
      {
          return new jarray(v._value);
      }

      public static implicit operator jarray(jintArray v)
      {
          return new jarray(v._value);
      }

      public static implicit operator jarray(jdoubleArray v)
      {
          return new jarray(v._value);
      }

      public static implicit operator jarray(jobjectArray v)
      {
          return new jarray(v._value);
      }

      public static implicit operator jarray(jlongArray v)
      {
          return new jarray(v._value);
      }
    }
    

    
    [NativeCppClass]
    public readonly struct jbooleanArray
    {
      internal readonly int _value;

      public jbooleanArray(int value)
      {
        _value = value;
      }

      public override string ToString()
      {
        return $"{nameof(jbooleanArray)}(x{_value:x8})";
      }
    }

    
    [NativeCppClass]
    public readonly struct jbyteArray
    {
      internal readonly int _value;

      public jbyteArray(int value)
      {
        _value = value;
      }

      public override string ToString()
      {
        return $"{nameof(jbyteArray)}(x{_value:x8})";
      }
    }

    
    [NativeCppClass]
    public readonly struct jcharArray
    {
      internal readonly int _value;

      public jcharArray(int value)
      {
        _value = value;
      }

      public override string ToString()
      {
        return $"{nameof(jcharArray)}(x{_value:x8})";
      }
    }

    
    [NativeCppClass]
    public readonly struct jshortArray
    {
      internal readonly int _value;

      public jshortArray(int value)
      {
        _value = value;
      }

      public override string ToString()
      {
        return $"{nameof(jshortArray)}(x{_value:x8})";
      }
    }

    
    [NativeCppClass]
    public readonly struct jintArray
    {
      internal readonly int _value;

      public jintArray(int value)
      {
        _value = value;
      }

      public override string ToString()
      {
        return $"{nameof(jintArray)}(x{_value:x8})";
      }
    }

    
    [NativeCppClass]
    public readonly struct jlongArray
    {
      internal readonly int _value;

      public jlongArray(int value)
      {
        _value = value;
      }

      public override string ToString()
      {
        return $"{nameof(jlongArray)}(x{_value:x8})";
      }
    }

    
    [NativeCppClass]
    public readonly struct jfloatArray
    {
      internal readonly int _value;

      public jfloatArray(int value)
      {
        _value = value;
      }

      public override string ToString()
      {
        return $"{nameof(jfloatArray)}(x{_value:x8})";
      }
    }

    
    [NativeCppClass]
    public readonly struct jdoubleArray
    {
      internal readonly int _value;

      public jdoubleArray(int value)
      {
        _value = value;
      }

      public override string ToString()
      {
        return $"{nameof(jdoubleArray)}(x{_value:x8})";
      }
    }

    
    [NativeCppClass]
    public readonly struct jobjectArray
    {
      internal readonly int _value;

      public jobjectArray(int value)
      {
        _value = value;
      }

      public override string ToString()
      {
        return $"{nameof(jobjectArray)}(x{_value:x8})";
      }
    }

    
    [NativeCppClass]
    public readonly struct jweak
    {
      internal readonly int _value;

      public jweak(int value)
      {
        _value = value;
      }

      public override string ToString()
      {
        return $"{nameof(jweak)}(x{_value:x8})";
      }
    }
    
    [NativeCppClass]
    [StructLayout(LayoutKind.Explicit, Size = 8)]
    public readonly struct jvalue
    {
        [FieldOffset(0)] public readonly jboolean Bool;
        [FieldOffset(0)] public readonly jbyte Byte;
        [FieldOffset(0)] public readonly jchar Char;
        [FieldOffset(0)] public readonly jshort Short;
        [FieldOffset(0)] public readonly jint Int;
        [FieldOffset(0)] public readonly jlong Long;
        [FieldOffset(0)] public readonly jfloat Float;
        [FieldOffset(0)] public readonly jdouble Double;
        [FieldOffset(0)] public readonly jobject Ref;
        [FieldOffset(0)] private readonly long _raw;

        public override string ToString()
        {
          return $"{nameof(jvalue)}(x{_raw:x8})";
        }
        
        public jvalue(jboolean b) : this()
        {
          Bool = b;
        }

        public jvalue(jbyte b) : this()
        {
          Byte = b;
        }

        public jvalue(jchar c) : this()
        {
          Char = c;
        }

        public jvalue(jshort s) : this()
        {
          Short = s;
        }

        public jvalue(jint i) : this()
        {
          Int = i;
        }

        public jvalue(jlong l) : this()
        {
          Long = l;
        }

        public jvalue(jfloat f) : this()
        {
          Float = f;
        }

        public jvalue(jdouble d) : this()
        {
          Double = d;
        }

        public jvalue(jobject o) : this()
        {
          Ref = o;
        }
    }

    
    [NativeCppClass]
    public readonly struct jfieldID
    {
      private readonly int _value;

      public jfieldID(int value)
      {
          _value = value;
      }

      public bool IsNull()
      {
        return _value == 0;
      }

      public override string ToString()
      {
        return $"{nameof(jfieldID)}(x{_value:x8})";
      }
    }

    
    [NativeCppClass]
    public readonly struct jmethodID
    {
      private readonly int _value;

      public jmethodID(int value)
      {
        _value = value;
      }

      public bool IsNull()
      {
        return _value == 0;
      }

      public override string ToString()
      {
        return $"{nameof(jmethodID)}(x{_value:x8})";
      }
    }


/* Return values from jobjectRefType */
public enum jobjectRefType {
     JNIInvalidRefType    = 0,
     JNILocalRefType      = 1,
     JNIGlobalRefType     = 2,
     JNIWeakGlobalRefType = 3
}

public enum JniVersion : jint
{
  JNI_VERSION_1_1 = 0x00010001,
  JNI_VERSION_1_2 = 0x00010002,
  JNI_VERSION_1_4 = 0x00010004,
  JNI_VERSION_1_6 = 0x00010006,
  JNI_VERSION_1_8 = 0x00010008,
}
    
public enum JniResult : jint
{
  /*
  * possible return values for JNI functions.
  */
  JNI_OK           =   0 ,              /* success */
  JNI_ERR          = (-1),              /* unknown error */
  JNI_EDETACHED    = (-2),              /* thread detached from the VM */
  JNI_EVERSION     = (-3),              /* JNI version error */
  JNI_ENOMEM       = (-4),              /* not enough memory */
  JNI_EEXIST       = (-5),              /* VM already created */
  JNI_EINVAL       = (-6),              /* invalid arguments */
}
    
/*
* used in ReleaseScalarArrayElements
*/

public enum ScalarArrayReleaseMode : System.Int32
{
  JNI_COMMIT = 1,
  JNI_ABORT = 2,
}

/*
 * used in RegisterNatives to describe native method name, signature,
 * and function pointer.
 */
[NativeCppClass]
public unsafe struct JNINativeMethod{
    public byte *name;
    public byte *signature;
    public void *fnPtr;
}


[NativeCppClass]
public unsafe struct JNINativeInterface_ {
    #pragma warning disable 169
    void *reserved0;
    void *reserved1;
    void *reserved2;

    void *reserved3;
    #pragma warning restore 169
    /*
    /* jint (JNICALL *GetVersion)(JNIEnv *env);  */
    public void* GetVersion;

    /* jclass (JNICALL *DefineClass)
      (JNIEnv *env, const char *name, jobject loader, const jbyte *buf,
       jsize len);  */
    public void* DefineClass;
    /* jclass (JNICALL *FindClass)
      (JNIEnv *env, const char *name);  */
    public void* FindClass;

    /* jmethodID (JNICALL *FromReflectedMethod)
      (JNIEnv *env, jobject method);  */
    public void* FromReflectedMethod;
    /* jfieldID (JNICALL *FromReflectedField)
      (JNIEnv *env, jobject field);  */
    public void* FromReflectedField;

    /* jobject (JNICALL *ToReflectedMethod)
      (JNIEnv *env, jclass cls, jmethodID methodID, jboolean isStatic);  */
    public void* ToReflectedMethod;

    /* jclass (JNICALL *GetSuperclass)
      (JNIEnv *env, jclass sub);  */
    public void* GetSuperclass;
    /* jboolean (JNICALL *IsAssignableFrom)
      (JNIEnv *env, jclass sub, jclass sup);  */
    public void* IsAssignableFrom;

    /* jobject (JNICALL *ToReflectedField)
      (JNIEnv *env, jclass cls, jfieldID fieldID, jboolean isStatic);  */
    public void* ToReflectedField;

    /* jint (JNICALL *Throw)
      (JNIEnv *env, jthrowable obj);  */
    public void* Throw;
    /* jint (JNICALL *ThrowNew)
      (JNIEnv *env, jclass clazz, const char *msg);  */
    public void* ThrowNew;
    /* jthrowable (JNICALL *ExceptionOccurred)
      (JNIEnv *env);  */
    public void* ExceptionOccurred;
    /* void (JNICALL *ExceptionDescribe)
      (JNIEnv *env);  */
    public void* ExceptionDescribe;
    /* void (JNICALL *ExceptionClear)
      (JNIEnv *env);  */
    public void* ExceptionClear;
    /* void (JNICALL *FatalError)
      (JNIEnv *env, const char *msg);  */
    public void* FatalError;

    /* jint (JNICALL *PushLocalFrame)
      (JNIEnv *env, jint capacity);  */
    public void* PushLocalFrame;
    /* jobject (JNICALL *PopLocalFrame)
      (JNIEnv *env, jobject result);  */
    public void* PopLocalFrame;

    /* jobject (JNICALL *NewGlobalRef)
      (JNIEnv *env, jobject lobj);  */
    public void* NewGlobalRef;
    /* void (JNICALL *DeleteGlobalRef)
      (JNIEnv *env, jobject gref);  */
    public void* DeleteGlobalRef;
    /* void (JNICALL *DeleteLocalRef)
      (JNIEnv *env, jobject obj);  */
    public void* DeleteLocalRef;
    /* jboolean (JNICALL *IsSameObject)
      (JNIEnv *env, jobject obj1, jobject obj2);  */
    public void* IsSameObject;
    /* jobject (JNICALL *NewLocalRef)
      (JNIEnv *env, jobject ref);  */
    public void* NewLocalRef;
    /* jint (JNICALL *EnsureLocalCapacity)
      (JNIEnv *env, jint capacity);  */
    public void* EnsureLocalCapacity;

    /* jobject (JNICALL *AllocObject)
      (JNIEnv *env, jclass clazz);  */
    public void* AllocObject;
    /* jobject (JNICALL *NewObject)
      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);  */
    public void* NewObject;
    /* jobject (JNICALL *NewObjectV)
      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);  */
    public void* NewObjectV;
    /* jobject (JNICALL *NewObjectA)
      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);  */
    public void* NewObjectA;

    /* jclass (JNICALL *GetObjectClass)
      (JNIEnv *env, jobject obj);  */
    public void* GetObjectClass;
    /* jboolean (JNICALL *IsInstanceOf)
      (JNIEnv *env, jobject obj, jclass clazz);  */
    public void* IsInstanceOf;

    /* jmethodID (JNICALL *GetMethodID)
      (JNIEnv *env, jclass clazz, const char *name, const char *sig);  */
    public void* GetMethodID;

    /* jobject (JNICALL *CallObjectMethod)
      (JNIEnv *env, jobject obj, jmethodID methodID, ...);  */
    public void* CallObjectMethod;
    /* jobject (JNICALL *CallObjectMethodV)
      (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);  */
    public void* CallObjectMethodV;
    /* jobject (JNICALL *CallObjectMethodA)
      (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue * args);  */
    public void* CallObjectMethodA;

    /* jboolean (JNICALL *CallBooleanMethod)
      (JNIEnv *env, jobject obj, jmethodID methodID, ...);  */
    public void* CallBooleanMethod;
    /* jboolean (JNICALL *CallBooleanMethodV)
      (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);  */
    public void* CallBooleanMethodV;
    /* jboolean (JNICALL *CallBooleanMethodA)
      (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue * args);  */
    public void* CallBooleanMethodA;

    /* jbyte (JNICALL *CallByteMethod)
      (JNIEnv *env, jobject obj, jmethodID methodID, ...);  */
    public void* CallByteMethod;
    /* jbyte (JNICALL *CallByteMethodV)
      (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);  */
    public void* CallByteMethodV;
    /* jbyte (JNICALL *CallByteMethodA)
      (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);  */
    public void* CallByteMethodA;

    /* jchar (JNICALL *CallCharMethod)
      (JNIEnv *env, jobject obj, jmethodID methodID, ...);  */
    public void* CallCharMethod;
    /* jchar (JNICALL *CallCharMethodV)
      (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);  */
    public void* CallCharMethodV;
    /* jchar (JNICALL *CallCharMethodA)
      (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);  */
    public void* CallCharMethodA;

    /* jshort (JNICALL *CallShortMethod)
      (JNIEnv *env, jobject obj, jmethodID methodID, ...);  */
    public void* CallShortMethod;
    /* jshort (JNICALL *CallShortMethodV)
      (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);  */
    public void* CallShortMethodV;
    /* jshort (JNICALL *CallShortMethodA)
      (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);  */
    public void* CallShortMethodA;

    /* jint (JNICALL *CallIntMethod)
      (JNIEnv *env, jobject obj, jmethodID methodID, ...);  */
    public void* CallIntMethod;
    /* jint (JNICALL *CallIntMethodV)
      (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);  */
    public void* CallIntMethodV;
    /* jint (JNICALL *CallIntMethodA)
      (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);  */
    public void* CallIntMethodA;

    /* jlong (JNICALL *CallLongMethod)
      (JNIEnv *env, jobject obj, jmethodID methodID, ...);  */
    public void* CallLongMethod;
    /* jlong (JNICALL *CallLongMethodV)
      (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);  */
    public void* CallLongMethodV;
    /* jlong (JNICALL *CallLongMethodA)
      (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);  */
    public void* CallLongMethodA;

    /* jfloat (JNICALL *CallFloatMethod)
      (JNIEnv *env, jobject obj, jmethodID methodID, ...);  */
    public void* CallFloatMethod;
    /* jfloat (JNICALL *CallFloatMethodV)
      (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);  */
    public void* CallFloatMethodV;
    /* jfloat (JNICALL *CallFloatMethodA)
      (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);  */
    public void* CallFloatMethodA;

    /* jdouble (JNICALL *CallDoubleMethod)
      (JNIEnv *env, jobject obj, jmethodID methodID, ...);  */
    public void* CallDoubleMethod;
    /* jdouble (JNICALL *CallDoubleMethodV)
      (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);  */
    public void* CallDoubleMethodV;
    /* jdouble (JNICALL *CallDoubleMethodA)
      (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);  */
    public void* CallDoubleMethodA;

    /* void (JNICALL *CallVoidMethod)
      (JNIEnv *env, jobject obj, jmethodID methodID, ...);  */
    public void* CallVoidMethod;
    /* void (JNICALL *CallVoidMethodV)
      (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);  */
    public void* CallVoidMethodV;
    /* void (JNICALL *CallVoidMethodA)
      (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue * args);  */
    public void* CallVoidMethodA;

    /* jobject (JNICALL *CallNonvirtualObjectMethod)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);  */
    public void* CallNonvirtualObjectMethod;
    /* jobject (JNICALL *CallNonvirtualObjectMethodV)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       va_list args);  */
    public void* CallNonvirtualObjectMethodV;
    /* jobject (JNICALL *CallNonvirtualObjectMethodA)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       const jvalue * args);  */
    public void* CallNonvirtualObjectMethodA;

    /* jboolean (JNICALL *CallNonvirtualBooleanMethod)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);  */
    public void* CallNonvirtualBooleanMethod;
    /* jboolean (JNICALL *CallNonvirtualBooleanMethodV)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       va_list args);  */
    public void* CallNonvirtualBooleanMethodV;
    /* jboolean (JNICALL *CallNonvirtualBooleanMethodA)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       const jvalue * args);  */
    public void* CallNonvirtualBooleanMethodA;

    /* jbyte (JNICALL *CallNonvirtualByteMethod)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);  */
    public void* CallNonvirtualByteMethod;
    /* jbyte (JNICALL *CallNonvirtualByteMethodV)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       va_list args);  */
    public void* CallNonvirtualByteMethodV;
    /* jbyte (JNICALL *CallNonvirtualByteMethodA)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       const jvalue *args);  */
    public void* CallNonvirtualByteMethodA;

    /* jchar (JNICALL *CallNonvirtualCharMethod)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);  */
    public void* CallNonvirtualCharMethod;
    /* jchar (JNICALL *CallNonvirtualCharMethodV)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       va_list args);  */
    public void* CallNonvirtualCharMethodV;
    /* jchar (JNICALL *CallNonvirtualCharMethodA)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       const jvalue *args);  */
    public void* CallNonvirtualCharMethodA;

    /* jshort (JNICALL *CallNonvirtualShortMethod)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);  */
    public void* CallNonvirtualShortMethod;
    /* jshort (JNICALL *CallNonvirtualShortMethodV)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       va_list args);  */
    public void* CallNonvirtualShortMethodV;
    /* jshort (JNICALL *CallNonvirtualShortMethodA)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       const jvalue *args);  */
    public void* CallNonvirtualShortMethodA;

    /* jint (JNICALL *CallNonvirtualIntMethod)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);  */
    public void* CallNonvirtualIntMethod;
    /* jint (JNICALL *CallNonvirtualIntMethodV)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       va_list args);  */
    public void* CallNonvirtualIntMethodV;
    /* jint (JNICALL *CallNonvirtualIntMethodA)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       const jvalue *args);  */
    public void* CallNonvirtualIntMethodA;

    /* jlong (JNICALL *CallNonvirtualLongMethod)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);  */
    public void* CallNonvirtualLongMethod;
    /* jlong (JNICALL *CallNonvirtualLongMethodV)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       va_list args);  */
    public void* CallNonvirtualLongMethodV;
    /* jlong (JNICALL *CallNonvirtualLongMethodA)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       const jvalue *args);  */
    public void* CallNonvirtualLongMethodA;

    /* jfloat (JNICALL *CallNonvirtualFloatMethod)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);  */
    public void* CallNonvirtualFloatMethod;
    /* jfloat (JNICALL *CallNonvirtualFloatMethodV)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       va_list args);  */
    public void* CallNonvirtualFloatMethodV;
    /* jfloat (JNICALL *CallNonvirtualFloatMethodA)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       const jvalue *args);  */
    public void* CallNonvirtualFloatMethodA;

    /* jdouble (JNICALL *CallNonvirtualDoubleMethod)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);  */
    public void* CallNonvirtualDoubleMethod;
    /* jdouble (JNICALL *CallNonvirtualDoubleMethodV)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       va_list args);  */
    public void* CallNonvirtualDoubleMethodV;
    /* jdouble (JNICALL *CallNonvirtualDoubleMethodA)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       const jvalue *args);  */
    public void* CallNonvirtualDoubleMethodA;

    /* void (JNICALL *CallNonvirtualVoidMethod)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);  */
    public void* CallNonvirtualVoidMethod;
    /* void (JNICALL *CallNonvirtualVoidMethodV)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       va_list args);  */
    public void* CallNonvirtualVoidMethodV;
    /* void (JNICALL *CallNonvirtualVoidMethodA)
      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
       const jvalue * args);  */
    public void* CallNonvirtualVoidMethodA;

    /* jfieldID (JNICALL *GetFieldID)
      (JNIEnv *env, jclass clazz, const char *name, const char *sig);  */
    public void* GetFieldID;

    /* jobject (JNICALL *GetObjectField)
      (JNIEnv *env, jobject obj, jfieldID fieldID);  */
    public void* GetObjectField;
    /* jboolean (JNICALL *GetBooleanField)
      (JNIEnv *env, jobject obj, jfieldID fieldID);  */
    public void* GetBooleanField;
    /* jbyte (JNICALL *GetByteField)
      (JNIEnv *env, jobject obj, jfieldID fieldID);  */
    public void* GetByteField;
    /* jchar (JNICALL *GetCharField)
      (JNIEnv *env, jobject obj, jfieldID fieldID);  */
    public void* GetCharField;
    /* jshort (JNICALL *GetShortField)
      (JNIEnv *env, jobject obj, jfieldID fieldID);  */
    public void* GetShortField;
    /* jint (JNICALL *GetIntField)
      (JNIEnv *env, jobject obj, jfieldID fieldID);  */
    public void* GetIntField;
    /* jlong (JNICALL *GetLongField)
      (JNIEnv *env, jobject obj, jfieldID fieldID);  */
    public void* GetLongField;
    /* jfloat (JNICALL *GetFloatField)
      (JNIEnv *env, jobject obj, jfieldID fieldID);  */
    public void* GetFloatField;
    /* jdouble (JNICALL *GetDoubleField)
      (JNIEnv *env, jobject obj, jfieldID fieldID);  */
    public void* GetDoubleField;

    /* void (JNICALL *SetObjectField)
      (JNIEnv *env, jobject obj, jfieldID fieldID, jobject val);  */
    public void* SetObjectField;
    /* void (JNICALL *SetBooleanField)
      (JNIEnv *env, jobject obj, jfieldID fieldID, jboolean val);  */
    public void* SetBooleanField;
    /* void (JNICALL *SetByteField)
      (JNIEnv *env, jobject obj, jfieldID fieldID, jbyte val);  */
    public void* SetByteField;
    /* void (JNICALL *SetCharField)
      (JNIEnv *env, jobject obj, jfieldID fieldID, jchar val);  */
    public void* SetCharField;
    /* void (JNICALL *SetShortField)
      (JNIEnv *env, jobject obj, jfieldID fieldID, jshort val);  */
    public void* SetShortField;
    /* void (JNICALL *SetIntField)
      (JNIEnv *env, jobject obj, jfieldID fieldID, jint val);  */
    public void* SetIntField;
    /* void (JNICALL *SetLongField)
      (JNIEnv *env, jobject obj, jfieldID fieldID, jlong val);  */
    public void* SetLongField;
    /* void (JNICALL *SetFloatField)
      (JNIEnv *env, jobject obj, jfieldID fieldID, jfloat val);  */
    public void* SetFloatField;
    /* void (JNICALL *SetDoubleField)
      (JNIEnv *env, jobject obj, jfieldID fieldID, jdouble val);  */
    public void* SetDoubleField;

    /* jmethodID (JNICALL *GetStaticMethodID)
      (JNIEnv *env, jclass clazz, const char *name, const char *sig);  */
    public void* GetStaticMethodID;

    /* jobject (JNICALL *CallStaticObjectMethod)
      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);  */
    public void* CallStaticObjectMethod;
    /* jobject (JNICALL *CallStaticObjectMethodV)
      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);  */
    public void* CallStaticObjectMethodV;
    /* jobject (JNICALL *CallStaticObjectMethodA)
      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);  */
    public void* CallStaticObjectMethodA;

    /* jboolean (JNICALL *CallStaticBooleanMethod)
      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);  */
    public void* CallStaticBooleanMethod;
    /* jboolean (JNICALL *CallStaticBooleanMethodV)
      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);  */
    public void* CallStaticBooleanMethodV;
    /* jboolean (JNICALL *CallStaticBooleanMethodA)
      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);  */
    public void* CallStaticBooleanMethodA;

    /* jbyte (JNICALL *CallStaticByteMethod)
      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);  */
    public void* CallStaticByteMethod;
    /* jbyte (JNICALL *CallStaticByteMethodV)
      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);  */
    public void* CallStaticByteMethodV;
    /* jbyte (JNICALL *CallStaticByteMethodA)
      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);  */
    public void* CallStaticByteMethodA;

    /* jchar (JNICALL *CallStaticCharMethod)
      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);  */
    public void* CallStaticCharMethod;
    /* jchar (JNICALL *CallStaticCharMethodV)
      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);  */
    public void* CallStaticCharMethodV;
    /* jchar (JNICALL *CallStaticCharMethodA)
      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);  */
    public void* CallStaticCharMethodA;

    /* jshort (JNICALL *CallStaticShortMethod)
      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);  */
    public void* CallStaticShortMethod;
    /* jshort (JNICALL *CallStaticShortMethodV)
      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);  */
    public void* CallStaticShortMethodV;
    /* jshort (JNICALL *CallStaticShortMethodA)
      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);  */
    public void* CallStaticShortMethodA;

    /* jint (JNICALL *CallStaticIntMethod)
      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);  */
    public void* CallStaticIntMethod;
    /* jint (JNICALL *CallStaticIntMethodV)
      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);  */
    public void* CallStaticIntMethodV;
    /* jint (JNICALL *CallStaticIntMethodA)
      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);  */
    public void* CallStaticIntMethodA;

    /* jlong (JNICALL *CallStaticLongMethod)
      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);  */
    public void* CallStaticLongMethod;
    /* jlong (JNICALL *CallStaticLongMethodV)
      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);  */
    public void* CallStaticLongMethodV;
    /* jlong (JNICALL *CallStaticLongMethodA)
      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);  */
    public void* CallStaticLongMethodA;

    /* jfloat (JNICALL *CallStaticFloatMethod)
      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);  */
    public void* CallStaticFloatMethod;
    /* jfloat (JNICALL *CallStaticFloatMethodV)
      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);  */
    public void* CallStaticFloatMethodV;
    /* jfloat (JNICALL *CallStaticFloatMethodA)
      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);  */
    public void* CallStaticFloatMethodA;

    /* jdouble (JNICALL *CallStaticDoubleMethod)
      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);  */
    public void* CallStaticDoubleMethod;
    /* jdouble (JNICALL *CallStaticDoubleMethodV)
      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);  */
    public void* CallStaticDoubleMethodV;
    /* jdouble (JNICALL *CallStaticDoubleMethodA)
      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);  */
    public void* CallStaticDoubleMethodA;

    /* void (JNICALL *CallStaticVoidMethod)
      (JNIEnv *env, jclass cls, jmethodID methodID, ...);  */
    public void* CallStaticVoidMethod;
    /* void (JNICALL *CallStaticVoidMethodV)
      (JNIEnv *env, jclass cls, jmethodID methodID, va_list args);  */
    public void* CallStaticVoidMethodV;
    /* void (JNICALL *CallStaticVoidMethodA)
      (JNIEnv *env, jclass cls, jmethodID methodID, const jvalue * args);  */
    public void* CallStaticVoidMethodA;

    /* jfieldID (JNICALL *GetStaticFieldID)
      (JNIEnv *env, jclass clazz, const char *name, const char *sig);  */
    public void* GetStaticFieldID;
    /* jobject (JNICALL *GetStaticObjectField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID);  */
    public void* GetStaticObjectField;
    /* jboolean (JNICALL *GetStaticBooleanField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID);  */
    public void* GetStaticBooleanField;
    /* jbyte (JNICALL *GetStaticByteField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID);  */
    public void* GetStaticByteField;
    /* jchar (JNICALL *GetStaticCharField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID);  */
    public void* GetStaticCharField;
    /* jshort (JNICALL *GetStaticShortField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID);  */
    public void* GetStaticShortField;
    /* jint (JNICALL *GetStaticIntField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID);  */
    public void* GetStaticIntField;
    /* jlong (JNICALL *GetStaticLongField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID);  */
    public void* GetStaticLongField;
    /* jfloat (JNICALL *GetStaticFloatField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID);  */
    public void* GetStaticFloatField;
    /* jdouble (JNICALL *GetStaticDoubleField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID);  */
    public void* GetStaticDoubleField;

    /* void (JNICALL *SetStaticObjectField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID, jobject value);  */
    public void* SetStaticObjectField;
    /* void (JNICALL *SetStaticBooleanField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID, jboolean value);  */
    public void* SetStaticBooleanField;
    /* void (JNICALL *SetStaticByteField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID, jbyte value);  */
    public void* SetStaticByteField;
    /* void (JNICALL *SetStaticCharField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID, jchar value);  */
    public void* SetStaticCharField;
    /* void (JNICALL *SetStaticShortField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID, jshort value);  */
    public void* SetStaticShortField;
    /* void (JNICALL *SetStaticIntField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID, jint value);  */
    public void* SetStaticIntField;
    /* void (JNICALL *SetStaticLongField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID, jlong value);  */
    public void* SetStaticLongField;
    /* void (JNICALL *SetStaticFloatField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID, jfloat value);  */
    public void* SetStaticFloatField;
    /* void (JNICALL *SetStaticDoubleField)
      (JNIEnv *env, jclass clazz, jfieldID fieldID, jdouble value);  */
    public void* SetStaticDoubleField;

    /* jstring (JNICALL *NewString)
      (JNIEnv *env, const jchar *unicode, jsize len);  */
    public void* NewString;
    /* jsize (JNICALL *GetStringLength)
      (JNIEnv *env, jstring str);  */
    public void* GetStringLength;
    /* const jchar *(JNICALL *GetStringChars)
      (JNIEnv *env, jstring str, jboolean *isCopy);  */
    public void* GetStringChars;
    /* void (JNICALL *ReleaseStringChars)
      (JNIEnv *env, jstring str, const jchar *chars);  */
    public void* ReleaseStringChars;

    /* jstring (JNICALL *NewStringUTF)
      (JNIEnv *env, const char *utf);  */
    public void* NewStringUTF;
    /* jsize (JNICALL *GetStringUTFLength)
      (JNIEnv *env, jstring str);  */
    public void* GetStringUTFLength;
    /* const char* (JNICALL *GetStringUTFChars)
      (JNIEnv *env, jstring str, jboolean *isCopy);  */
    public void* GetStringUTFChars;
    /* void (JNICALL *ReleaseStringUTFChars)
      (JNIEnv *env, jstring str, const char* chars);  */
    public void* ReleaseStringUTFChars;


    /* jsize (JNICALL *GetArrayLength)
      (JNIEnv *env, jarray array);  */
    public void* GetArrayLength;

    /* jobjectArray (JNICALL *NewObjectArray)
      (JNIEnv *env, jsize len, jclass clazz, jobject init);  */
    public void* NewObjectArray;
    /* jobject (JNICALL *GetObjectArrayElement)
      (JNIEnv *env, jobjectArray array, jsize index);  */
    public void* GetObjectArrayElement;
    /* void (JNICALL *SetObjectArrayElement)
      (JNIEnv *env, jobjectArray array, jsize index, jobject val);  */
    public void* SetObjectArrayElement;

    /* jbooleanArray (JNICALL *NewBooleanArray)
      (JNIEnv *env, jsize len);  */
    public void* NewBooleanArray;
    /* jbyteArray (JNICALL *NewByteArray)
      (JNIEnv *env, jsize len);  */
    public void* NewByteArray;
    /* jcharArray (JNICALL *NewCharArray)
      (JNIEnv *env, jsize len);  */
    public void* NewCharArray;
    /* jshortArray (JNICALL *NewShortArray)
      (JNIEnv *env, jsize len);  */
    public void* NewShortArray;
    /* jintArray (JNICALL *NewIntArray)
      (JNIEnv *env, jsize len);  */
    public void* NewIntArray;
    /* jlongArray (JNICALL *NewLongArray)
      (JNIEnv *env, jsize len);  */
    public void* NewLongArray;
    /* jfloatArray (JNICALL *NewFloatArray)
      (JNIEnv *env, jsize len);  */
    public void* NewFloatArray;
    /* jdoubleArray (JNICALL *NewDoubleArray)
      (JNIEnv *env, jsize len);  */
    public void* NewDoubleArray;

    /* jboolean * (JNICALL *GetBooleanArrayElements)
      (JNIEnv *env, jbooleanArray array, jboolean *isCopy);  */
    public void* GetBooleanArrayElements;
    /* jbyte * (JNICALL *GetByteArrayElements)
      (JNIEnv *env, jbyteArray array, jboolean *isCopy);  */
    public void* GetByteArrayElements;
    /* jchar * (JNICALL *GetCharArrayElements)
      (JNIEnv *env, jcharArray array, jboolean *isCopy);  */
    public void* GetCharArrayElements;
    /* jshort * (JNICALL *GetShortArrayElements)
      (JNIEnv *env, jshortArray array, jboolean *isCopy);  */
    public void* GetShortArrayElements;
    /* jint * (JNICALL *GetIntArrayElements)
      (JNIEnv *env, jintArray array, jboolean *isCopy);  */
    public void* GetIntArrayElements;
    /* jlong * (JNICALL *GetLongArrayElements)
      (JNIEnv *env, jlongArray array, jboolean *isCopy);  */
    public void* GetLongArrayElements;
    /* jfloat * (JNICALL *GetFloatArrayElements)
      (JNIEnv *env, jfloatArray array, jboolean *isCopy);  */
    public void* GetFloatArrayElements;
    /* jdouble * (JNICALL *GetDoubleArrayElements)
      (JNIEnv *env, jdoubleArray array, jboolean *isCopy);  */
    public void* GetDoubleArrayElements;

    /* void (JNICALL *ReleaseBooleanArrayElements)
      (JNIEnv *env, jbooleanArray array, jboolean *elems, jint mode);  */
    public void* ReleaseBooleanArrayElements;
    /* void (JNICALL *ReleaseByteArrayElements)
      (JNIEnv *env, jbyteArray array, jbyte *elems, jint mode);  */
    public void* ReleaseByteArrayElements;
    /* void (JNICALL *ReleaseCharArrayElements)
      (JNIEnv *env, jcharArray array, jchar *elems, jint mode);  */
    public void* ReleaseCharArrayElements;
    /* void (JNICALL *ReleaseShortArrayElements)
      (JNIEnv *env, jshortArray array, jshort *elems, jint mode);  */
    public void* ReleaseShortArrayElements;
    /* void (JNICALL *ReleaseIntArrayElements)
      (JNIEnv *env, jintArray array, jint *elems, jint mode);  */
    public void* ReleaseIntArrayElements;
    /* void (JNICALL *ReleaseLongArrayElements)
      (JNIEnv *env, jlongArray array, jlong *elems, jint mode);  */
    public void* ReleaseLongArrayElements;
    /* void (JNICALL *ReleaseFloatArrayElements)
      (JNIEnv *env, jfloatArray array, jfloat *elems, jint mode);  */
    public void* ReleaseFloatArrayElements;
    /* void (JNICALL *ReleaseDoubleArrayElements)
      (JNIEnv *env, jdoubleArray array, jdouble *elems, jint mode);  */
    public void* ReleaseDoubleArrayElements;

    /* void (JNICALL *GetBooleanArrayRegion)
      (JNIEnv *env, jbooleanArray array, jsize start, jsize l, jboolean *buf);  */
    public void* GetBooleanArrayRegion;
    /* void (JNICALL *GetByteArrayRegion)
      (JNIEnv *env, jbyteArray array, jsize start, jsize len, jbyte *buf);  */
    public void* GetByteArrayRegion;
    /* void (JNICALL *GetCharArrayRegion)
      (JNIEnv *env, jcharArray array, jsize start, jsize len, jchar *buf);  */
    public void* GetCharArrayRegion;
    /* void (JNICALL *GetShortArrayRegion)
      (JNIEnv *env, jshortArray array, jsize start, jsize len, jshort *buf);  */
    public void* GetShortArrayRegion;
    /* void (JNICALL *GetIntArrayRegion)
      (JNIEnv *env, jintArray array, jsize start, jsize len, jint *buf);  */
    public void* GetIntArrayRegion;
    /* void (JNICALL *GetLongArrayRegion)
      (JNIEnv *env, jlongArray array, jsize start, jsize len, jlong *buf);  */
    public void* GetLongArrayRegion;
    /* void (JNICALL *GetFloatArrayRegion)
      (JNIEnv *env, jfloatArray array, jsize start, jsize len, jfloat *buf);  */
    public void* GetFloatArrayRegion;
    /* void (JNICALL *GetDoubleArrayRegion)
      (JNIEnv *env, jdoubleArray array, jsize start, jsize len, jdouble *buf);  */
    public void* GetDoubleArrayRegion;

    /* void (JNICALL *SetBooleanArrayRegion)
      (JNIEnv *env, jbooleanArray array, jsize start, jsize l, const jboolean *buf);  */
    public void* SetBooleanArrayRegion;
    /* void (JNICALL *SetByteArrayRegion)
      (JNIEnv *env, jbyteArray array, jsize start, jsize len, const jbyte *buf);  */
    public void* SetByteArrayRegion;
    /* void (JNICALL *SetCharArrayRegion)
      (JNIEnv *env, jcharArray array, jsize start, jsize len, const jchar *buf);  */
    public void* SetCharArrayRegion;
    /* void (JNICALL *SetShortArrayRegion)
      (JNIEnv *env, jshortArray array, jsize start, jsize len, const jshort *buf);  */
    public void* SetShortArrayRegion;
    /* void (JNICALL *SetIntArrayRegion)
      (JNIEnv *env, jintArray array, jsize start, jsize len, const jint *buf);  */
    public void* SetIntArrayRegion;
    /* void (JNICALL *SetLongArrayRegion)
      (JNIEnv *env, jlongArray array, jsize start, jsize len, const jlong *buf);  */
    public void* SetLongArrayRegion;
    /* void (JNICALL *SetFloatArrayRegion)
      (JNIEnv *env, jfloatArray array, jsize start, jsize len, const jfloat *buf);  */
    public void* SetFloatArrayRegion;
    /* void (JNICALL *SetDoubleArrayRegion)
      (JNIEnv *env, jdoubleArray array, jsize start, jsize len, const jdouble *buf);  */
    public void* SetDoubleArrayRegion;

    /* jint (JNICALL *RegisterNatives)
      (JNIEnv *env, jclass clazz, const JNINativeMethod *methods,
       jint nMethods);  */
    public void* RegisterNatives;
    /* jint (JNICALL *UnregisterNatives)
      (JNIEnv *env, jclass clazz);  */
    public void* UnregisterNatives;

    /* jint (JNICALL *MonitorEnter)
      (JNIEnv *env, jobject obj);  */
    public void* MonitorEnter;
    /* jint (JNICALL *MonitorExit)
      (JNIEnv *env, jobject obj);  */
    public void* MonitorExit;

    /* jint (JNICALL *GetJavaVM)
      (JNIEnv *env, JavaVM **vm);  */
    public void* GetJavaVM;

    /* void (JNICALL *GetStringRegion)
      (JNIEnv *env, jstring str, jsize start, jsize len, jchar *buf);  */
    public void* GetStringRegion;
    /* void (JNICALL *GetStringUTFRegion)
      (JNIEnv *env, jstring str, jsize start, jsize len, char *buf);  */
    public void* GetStringUTFRegion;

    /* void * (JNICALL *GetPrimitiveArrayCritical)
      (JNIEnv *env, jarray array, jboolean *isCopy);  */
    public void* GetPrimitiveArrayCritical;
    /* void (JNICALL *ReleasePrimitiveArrayCritical)
      (JNIEnv *env, jarray array, void *carray, jint mode);  */
    public void* ReleasePrimitiveArrayCritical;

    /* const jchar * (JNICALL *GetStringCritical)
      (JNIEnv *env, jstring string, jboolean *isCopy);  */
    public void* GetStringCritical;
    /* void (JNICALL *ReleaseStringCritical)
      (JNIEnv *env, jstring string, const jchar *cstring);  */
    public void* ReleaseStringCritical;

    /* jweak (JNICALL *NewWeakGlobalRef)
       (JNIEnv *env, jobject obj);  */
    public void* NewWeakGlobalRef;
    /* void (JNICALL *DeleteWeakGlobalRef)
       (JNIEnv *env, jweak ref);  */
    public void* DeleteWeakGlobalRef;

    /* jboolean (JNICALL *ExceptionCheck)
       (JNIEnv *env);  */
    public void* ExceptionCheck;

    /* jobject (JNICALL *NewDirectByteBuffer)
       (JNIEnv* env, void* address, jlong capacity);  */
    public void* NewDirectByteBuffer;
    /* void* (JNICALL *GetDirectBufferAddress)
       (JNIEnv* env, jobject buf);  */
    public void* GetDirectBufferAddress;
    /* jlong (JNICALL *GetDirectBufferCapacity)
       (JNIEnv* env, jobject buf);  */
    public void* GetDirectBufferCapacity;
    //New JNI 1.6 Features 
    /* jobjectRefType (JNICALL *GetObjectRefType)
        (JNIEnv* env, jobject obj);  */
    public void* GetObjectRefType;
}

/*
 * We use inlined functions for C++ so that programmers can write:
 *
 *    env->FindClass("java/lang/String")
 *
 * in C++ rather than:
 *
 *    (*env)->FindClass(env, "java/lang/String")
 *
 * in C.
 */

[NativeCppClass]
public unsafe struct JNIEnv_ {
    public JNINativeInterface_ *functions;
};

[NativeCppClass]
public unsafe struct JavaVMOption {
    public byte *optionString;
    public void *extraInfo;
}

[NativeCppClass]
public unsafe struct JavaVMInitArgs {
    public JniVersion version;

    public jint nOptions;
    public JavaVMOption *options;
    public jboolean ignoreUnrecognized;
}

[NativeCppClass]
public unsafe struct JavaVMAttachArgs {
    public jint version;

    public char *name;
    public jobject group;
}

[NativeCppClass]
public unsafe struct JNIInvokeInterface_ {
    #pragma warning disable 169
    void *reserved0;
    void *reserved1;
    void *reserved2;
    #pragma warning restore 169

    /* jint (JNICALL *DestroyJavaVM)(JavaVM *vm);  */
    public void* DestroyJavaVM;

    /* jint (JNICALL *AttachCurrentThread)(JavaVM *vm, void **penv, void *args);  */
    public void* AttachCurrentThread;

    /* jint (JNICALL *DetachCurrentThread)(JavaVM *vm);  */
    public void* DetachCurrentThread;

    /* jint (JNICALL *GetEnv)(JavaVM *vm, void **penv, jint version);  */
    public void* GetEnv;

    /* jint (JNICALL *AttachCurrentThreadAsDaemon)(JavaVM *vm, void **penv, void *args);  */
    public void* AttachCurrentThreadAsDaemon;
};

[NativeCppClass]
public readonly unsafe struct JavaVM_ {
    // ReSharper disable once UnassignedReadonlyField
    public readonly JNIInvokeInterface_ *functions;
}

}