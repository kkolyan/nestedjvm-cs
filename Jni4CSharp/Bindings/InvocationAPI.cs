using Jni4CSharp.Bindings.Functions;
using Jni4CSharp.CommonUtils;

namespace Jni4CSharp.Bindings
{
    // ReSharper disable InconsistentNaming
    // ReSharper disable CommentTypo
    // ReSharper disable IdentifierTypo
    // ReSharper disable UnusedMember.Global
    // ReSharper disable once UnusedType.Global

    using jsize = System.Int32;
    
    public readonly struct InvocationAPI
    {
        private readonly InvocationAPIFunctions _functions;

        public InvocationAPI(DynamicLibrary library)
        {
            _functions = new InvocationAPIFunctions(library);
        }

        public unsafe JniResult JNI_GetDefaultJavaVMInitArgs(void* args)
        {
            return _functions.JNI_GetDefaultJavaVMInitArgs(args);
        }

        public unsafe JniResult JNI_CreateJavaVM(JavaVM_** pvm, JNIEnv_** penv, JavaVMInitArgs* args)
        {
            return _functions.JNI_CreateJavaVM(pvm, penv, args);
        }

        public unsafe JniResult JNI_GetCreatedJavaVMs(JavaVM_** vmBuf, jsize bufLen, jsize* nVMs)
        {
            return _functions.JNI_GetCreatedJavaVMs(vmBuf, bufLen, nVMs);
        }
    }
}