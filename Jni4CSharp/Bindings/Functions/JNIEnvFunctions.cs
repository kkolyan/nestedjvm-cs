using Jni4CSharp.CommonUtils;

namespace Jni4CSharp.Bindings.Functions
{
    // ReSharper disable InconsistentNaming
    // ReSharper disable CommentTypo
    // ReSharper disable IdentifierTypo
    public unsafe class JNIEnvFunctions
    {
        public JNIEnvFunctions(JNIEnv_* env)
        {
            GetVersion = Ptrs.Proc<GetVersion>(env->functions->GetVersion);

            DefineClass = Ptrs.Proc<DefineClass>(env->functions->DefineClass);
            FindClass = Ptrs.Proc<FindClass>(env->functions->FindClass);

            FromReflectedMethod = Ptrs.Proc<FromReflectedMethod>(env->functions->FromReflectedMethod);
            FromReflectedField = Ptrs.Proc<FromReflectedField>(env->functions->FromReflectedField);

            ToReflectedMethod = Ptrs.Proc<ToReflectedMethod>(env->functions->ToReflectedMethod);

            GetSuperclass = Ptrs.Proc<GetSuperclass>(env->functions->GetSuperclass);
            IsAssignableFrom = Ptrs.Proc<IsAssignableFrom>(env->functions->IsAssignableFrom);

            ToReflectedField = Ptrs.Proc<ToReflectedField>(env->functions->ToReflectedField);

            Throw = Ptrs.Proc<Throw>(env->functions->Throw);
            ThrowNew = Ptrs.Proc<ThrowNew>(env->functions->ThrowNew);
            ExceptionOccurred = Ptrs.Proc<ExceptionOccurred>(env->functions->ExceptionOccurred);
            ExceptionDescribe = Ptrs.Proc<ExceptionDescribe>(env->functions->ExceptionDescribe);
            ExceptionClear = Ptrs.Proc<ExceptionClear>(env->functions->ExceptionClear);
            FatalError = Ptrs.Proc<FatalError>(env->functions->FatalError);

            PushLocalFrame = Ptrs.Proc<PushLocalFrame>(env->functions->PushLocalFrame);
            PopLocalFrame = Ptrs.Proc<PopLocalFrame>(env->functions->PopLocalFrame);

            NewGlobalRef = Ptrs.Proc<NewGlobalRef>(env->functions->NewGlobalRef);
            DeleteGlobalRef = Ptrs.Proc<DeleteGlobalRef>(env->functions->DeleteGlobalRef);
            DeleteLocalRef = Ptrs.Proc<DeleteLocalRef>(env->functions->DeleteLocalRef);
            IsSameObject = Ptrs.Proc<IsSameObject>(env->functions->IsSameObject);
            NewLocalRef = Ptrs.Proc<NewLocalRef>(env->functions->NewLocalRef);
            EnsureLocalCapacity = Ptrs.Proc<EnsureLocalCapacity>(env->functions->EnsureLocalCapacity);

            AllocObject = Ptrs.Proc<AllocObject>(env->functions->AllocObject);
            // NewObject = Ptrs.Proc<NewObject>(env->functions->NewObject);
            // NewObjectV = Ptrs.Proc<NewObjectV>(env->functions->NewObjectV);
            NewObjectA = Ptrs.Proc<NewObjectA>(env->functions->NewObjectA);

            GetObjectClass = Ptrs.Proc<GetObjectClass>(env->functions->GetObjectClass);
            IsInstanceOf = Ptrs.Proc<IsInstanceOf>(env->functions->IsInstanceOf);

            GetMethodID = Ptrs.Proc<GetMethodID>(env->functions->GetMethodID);

            // CallObjectMethod = Ptrs.Proc<CallObjectMethod>(env->functions->CallObjectMethod);
            // CallObjectMethodV = Ptrs.Proc<CallObjectMethodV>(env->functions->CallObjectMethodV);
            CallObjectMethodA = Ptrs.Proc<CallObjectMethodA>(env->functions->CallObjectMethodA);

            // CallBooleanMethod = Ptrs.Proc<CallBooleanMethod>(env->functions->CallBooleanMethod);
            // CallBooleanMethodV = Ptrs.Proc<CallBooleanMethodV>(env->functions->CallBooleanMethodV);
            CallBooleanMethodA = Ptrs.Proc<CallBooleanMethodA>(env->functions->CallBooleanMethodA);

            // CallByteMethod = Ptrs.Proc<CallByteMethod>(env->functions->CallByteMethod);
            // CallByteMethodV = Ptrs.Proc<CallByteMethodV>(env->functions->CallByteMethodV);
            CallByteMethodA = Ptrs.Proc<CallByteMethodA>(env->functions->CallByteMethodA);

            // CallCharMethod = Ptrs.Proc<CallCharMethod>(env->functions->CallCharMethod);
            // CallCharMethodV = Ptrs.Proc<CallCharMethodV>(env->functions->CallCharMethodV);
            CallCharMethodA = Ptrs.Proc<CallCharMethodA>(env->functions->CallCharMethodA);

            // CallShortMethod = Ptrs.Proc<CallShortMethod>(env->functions->CallShortMethod);
            // CallShortMethodV = Ptrs.Proc<CallShortMethodV>(env->functions->CallShortMethodV);
            CallShortMethodA = Ptrs.Proc<CallShortMethodA>(env->functions->CallShortMethodA);

            // CallIntMethod = Ptrs.Proc<CallIntMethod>(env->functions->CallIntMethod);
            // CallIntMethodV = Ptrs.Proc<CallIntMethodV>(env->functions->CallIntMethodV);
            CallIntMethodA = Ptrs.Proc<CallIntMethodA>(env->functions->CallIntMethodA);

            // CallLongMethod = Ptrs.Proc<CallLongMethod>(env->functions->CallLongMethod);
            // CallLongMethodV = Ptrs.Proc<CallLongMethodV>(env->functions->CallLongMethodV);
            CallLongMethodA = Ptrs.Proc<CallLongMethodA>(env->functions->CallLongMethodA);

            // CallFloatMethod = Ptrs.Proc<CallFloatMethod>(env->functions->CallFloatMethod);
            // CallFloatMethodV = Ptrs.Proc<CallFloatMethodV>(env->functions->CallFloatMethodV);
            CallFloatMethodA = Ptrs.Proc<CallFloatMethodA>(env->functions->CallFloatMethodA);

            // CallDoubleMethod = Ptrs.Proc<CallDoubleMethod>(env->functions->CallDoubleMethod);
            // CallDoubleMethodV = Ptrs.Proc<CallDoubleMethodV>(env->functions->CallDoubleMethodV);
            CallDoubleMethodA = Ptrs.Proc<CallDoubleMethodA>(env->functions->CallDoubleMethodA);

            // CallVoidMethod = Ptrs.Proc<CallVoidMethod>(env->functions->CallVoidMethod);
            // CallVoidMethodV = Ptrs.Proc<CallVoidMethodV>(env->functions->CallVoidMethodV);
            CallVoidMethodA = Ptrs.Proc<CallVoidMethodA>(env->functions->CallVoidMethodA);

            // CallNonvirtualObjectMethod = Ptrs.Proc<CallNonvirtualObjectMethod>(env->functions->CallNonvirtualObjectMethod);
            // CallNonvirtualObjectMethodV = Ptrs.Proc<CallNonvirtualObjectMethodV>(env->functions->CallNonvirtualObjectMethodV);
            CallNonvirtualObjectMethodA = Ptrs.Proc<CallNonvirtualObjectMethodA>(env->functions->CallNonvirtualObjectMethodA);

            // CallNonvirtualBooleanMethod = Ptrs.Proc<CallNonvirtualBooleanMethod>(env->functions->CallNonvirtualBooleanMethod);
            // CallNonvirtualBooleanMethodV = Ptrs.Proc<CallNonvirtualBooleanMethodV>(env->functions->CallNonvirtualBooleanMethodV);
            CallNonvirtualBooleanMethodA = Ptrs.Proc<CallNonvirtualBooleanMethodA>(env->functions->CallNonvirtualBooleanMethodA);

            // CallNonvirtualByteMethod = Ptrs.Proc<CallNonvirtualByteMethod>(env->functions->CallNonvirtualByteMethod);
            // CallNonvirtualByteMethodV = Ptrs.Proc<CallNonvirtualByteMethodV>(env->functions->CallNonvirtualByteMethodV);
            CallNonvirtualByteMethodA = Ptrs.Proc<CallNonvirtualByteMethodA>(env->functions->CallNonvirtualByteMethodA);

            // CallNonvirtualCharMethod = Ptrs.Proc<CallNonvirtualCharMethod>(env->functions->CallNonvirtualCharMethod);
            // CallNonvirtualCharMethodV = Ptrs.Proc<CallNonvirtualCharMethodV>(env->functions->CallNonvirtualCharMethodV);
            CallNonvirtualCharMethodA = Ptrs.Proc<CallNonvirtualCharMethodA>(env->functions->CallNonvirtualCharMethodA);

            // CallNonvirtualShortMethod = Ptrs.Proc<CallNonvirtualShortMethod>(env->functions->CallNonvirtualShortMethod);
            // CallNonvirtualShortMethodV = Ptrs.Proc<CallNonvirtualShortMethodV>(env->functions->CallNonvirtualShortMethodV);
            CallNonvirtualShortMethodA = Ptrs.Proc<CallNonvirtualShortMethodA>(env->functions->CallNonvirtualShortMethodA);

            // CallNonvirtualIntMethod = Ptrs.Proc<CallNonvirtualIntMethod>(env->functions->CallNonvirtualIntMethod);
            // CallNonvirtualIntMethodV = Ptrs.Proc<CallNonvirtualIntMethodV>(env->functions->CallNonvirtualIntMethodV);
            CallNonvirtualIntMethodA = Ptrs.Proc<CallNonvirtualIntMethodA>(env->functions->CallNonvirtualIntMethodA);

            // CallNonvirtualLongMethod = Ptrs.Proc<CallNonvirtualLongMethod>(env->functions->CallNonvirtualLongMethod);
            // CallNonvirtualLongMethodV = Ptrs.Proc<CallNonvirtualLongMethodV>(env->functions->CallNonvirtualLongMethodV);
            CallNonvirtualLongMethodA = Ptrs.Proc<CallNonvirtualLongMethodA>(env->functions->CallNonvirtualLongMethodA);

            // CallNonvirtualFloatMethod = Ptrs.Proc<CallNonvirtualFloatMethod>(env->functions->CallNonvirtualFloatMethod);
            // CallNonvirtualFloatMethodV = Ptrs.Proc<CallNonvirtualFloatMethodV>(env->functions->CallNonvirtualFloatMethodV);
            CallNonvirtualFloatMethodA = Ptrs.Proc<CallNonvirtualFloatMethodA>(env->functions->CallNonvirtualFloatMethodA);

            // CallNonvirtualDoubleMethod = Ptrs.Proc<CallNonvirtualDoubleMethod>(env->functions->CallNonvirtualDoubleMethod);
            // CallNonvirtualDoubleMethodV = Ptrs.Proc<CallNonvirtualDoubleMethodV>(env->functions->CallNonvirtualDoubleMethodV);
            CallNonvirtualDoubleMethodA = Ptrs.Proc<CallNonvirtualDoubleMethodA>(env->functions->CallNonvirtualDoubleMethodA);

            // CallNonvirtualVoidMethod = Ptrs.Proc<CallNonvirtualVoidMethod>(env->functions->CallNonvirtualVoidMethod);
            // CallNonvirtualVoidMethodV = Ptrs.Proc<CallNonvirtualVoidMethodV>(env->functions->CallNonvirtualVoidMethodV);
            CallNonvirtualVoidMethodA = Ptrs.Proc<CallNonvirtualVoidMethodA>(env->functions->CallNonvirtualVoidMethodA);

            GetFieldID = Ptrs.Proc<GetFieldID>(env->functions->GetFieldID);

            GetObjectField = Ptrs.Proc<GetObjectField>(env->functions->GetObjectField);
            GetBooleanField = Ptrs.Proc<GetBooleanField>(env->functions->GetBooleanField);
            GetByteField = Ptrs.Proc<GetByteField>(env->functions->GetByteField);
            GetCharField = Ptrs.Proc<GetCharField>(env->functions->GetCharField);
            GetShortField = Ptrs.Proc<GetShortField>(env->functions->GetShortField);
            GetIntField = Ptrs.Proc<GetIntField>(env->functions->GetIntField);
            GetLongField = Ptrs.Proc<GetLongField>(env->functions->GetLongField);
            GetFloatField = Ptrs.Proc<GetFloatField>(env->functions->GetFloatField);
            GetDoubleField = Ptrs.Proc<GetDoubleField>(env->functions->GetDoubleField);

            SetObjectField = Ptrs.Proc<SetObjectField>(env->functions->SetObjectField);
            SetBooleanField = Ptrs.Proc<SetBooleanField>(env->functions->SetBooleanField);
            SetByteField = Ptrs.Proc<SetByteField>(env->functions->SetByteField);
            SetCharField = Ptrs.Proc<SetCharField>(env->functions->SetCharField);
            SetShortField = Ptrs.Proc<SetShortField>(env->functions->SetShortField);
            SetIntField = Ptrs.Proc<SetIntField>(env->functions->SetIntField);
            SetLongField = Ptrs.Proc<SetLongField>(env->functions->SetLongField);
            SetFloatField = Ptrs.Proc<SetFloatField>(env->functions->SetFloatField);
            SetDoubleField = Ptrs.Proc<SetDoubleField>(env->functions->SetDoubleField);

            GetStaticMethodID = Ptrs.Proc<GetStaticMethodID>(env->functions->GetStaticMethodID);

            // CallStaticObjectMethod = Ptrs.Proc<CallStaticObjectMethod>(env->functions->CallStaticObjectMethod);
            // CallStaticObjectMethodV = Ptrs.Proc<CallStaticObjectMethodV>(env->functions->CallStaticObjectMethodV);
            CallStaticObjectMethodA = Ptrs.Proc<CallStaticObjectMethodA>(env->functions->CallStaticObjectMethodA);

            // CallStaticBooleanMethod = Ptrs.Proc<CallStaticBooleanMethod>(env->functions->CallStaticBooleanMethod);
            // CallStaticBooleanMethodV = Ptrs.Proc<CallStaticBooleanMethodV>(env->functions->CallStaticBooleanMethodV);
            CallStaticBooleanMethodA = Ptrs.Proc<CallStaticBooleanMethodA>(env->functions->CallStaticBooleanMethodA);

            // CallStaticByteMethod = Ptrs.Proc<CallStaticByteMethod>(env->functions->CallStaticByteMethod);
            // CallStaticByteMethodV = Ptrs.Proc<CallStaticByteMethodV>(env->functions->CallStaticByteMethodV);
            CallStaticByteMethodA = Ptrs.Proc<CallStaticByteMethodA>(env->functions->CallStaticByteMethodA);

            // CallStaticCharMethod = Ptrs.Proc<CallStaticCharMethod>(env->functions->CallStaticCharMethod);
            // CallStaticCharMethodV = Ptrs.Proc<CallStaticCharMethodV>(env->functions->CallStaticCharMethodV);
            CallStaticCharMethodA = Ptrs.Proc<CallStaticCharMethodA>(env->functions->CallStaticCharMethodA);

            // CallStaticShortMethod = Ptrs.Proc<CallStaticShortMethod>(env->functions->CallStaticShortMethod);
            // CallStaticShortMethodV = Ptrs.Proc<CallStaticShortMethodV>(env->functions->CallStaticShortMethodV);
            CallStaticShortMethodA = Ptrs.Proc<CallStaticShortMethodA>(env->functions->CallStaticShortMethodA);

            // CallStaticIntMethod = Ptrs.Proc<CallStaticIntMethod>(env->functions->CallStaticIntMethod);
            // CallStaticIntMethodV = Ptrs.Proc<CallStaticIntMethodV>(env->functions->CallStaticIntMethodV);
            CallStaticIntMethodA = Ptrs.Proc<CallStaticIntMethodA>(env->functions->CallStaticIntMethodA);

            // CallStaticLongMethod = Ptrs.Proc<CallStaticLongMethod>(env->functions->CallStaticLongMethod);
            // CallStaticLongMethodV = Ptrs.Proc<CallStaticLongMethodV>(env->functions->CallStaticLongMethodV);
            CallStaticLongMethodA = Ptrs.Proc<CallStaticLongMethodA>(env->functions->CallStaticLongMethodA);

            // CallStaticFloatMethod = Ptrs.Proc<CallStaticFloatMethod>(env->functions->CallStaticFloatMethod);
            // CallStaticFloatMethodV = Ptrs.Proc<CallStaticFloatMethodV>(env->functions->CallStaticFloatMethodV);
            CallStaticFloatMethodA = Ptrs.Proc<CallStaticFloatMethodA>(env->functions->CallStaticFloatMethodA);

            // CallStaticDoubleMethod = Ptrs.Proc<CallStaticDoubleMethod>(env->functions->CallStaticDoubleMethod);
            // CallStaticDoubleMethodV = Ptrs.Proc<CallStaticDoubleMethodV>(env->functions->CallStaticDoubleMethodV);
            CallStaticDoubleMethodA = Ptrs.Proc<CallStaticDoubleMethodA>(env->functions->CallStaticDoubleMethodA);

            // CallStaticVoidMethod = Ptrs.Proc<CallStaticVoidMethod>(env->functions->CallStaticVoidMethod);
            // CallStaticVoidMethodV = Ptrs.Proc<CallStaticVoidMethodV>(env->functions->CallStaticVoidMethodV);
            CallStaticVoidMethodA = Ptrs.Proc<CallStaticVoidMethodA>(env->functions->CallStaticVoidMethodA);

            GetStaticFieldID = Ptrs.Proc<GetStaticFieldID>(env->functions->GetStaticFieldID);
            GetStaticObjectField = Ptrs.Proc<GetStaticObjectField>(env->functions->GetStaticObjectField);
            GetStaticBooleanField = Ptrs.Proc<GetStaticBooleanField>(env->functions->GetStaticBooleanField);
            GetStaticByteField = Ptrs.Proc<GetStaticByteField>(env->functions->GetStaticByteField);
            GetStaticCharField = Ptrs.Proc<GetStaticCharField>(env->functions->GetStaticCharField);
            GetStaticShortField = Ptrs.Proc<GetStaticShortField>(env->functions->GetStaticShortField);
            GetStaticIntField = Ptrs.Proc<GetStaticIntField>(env->functions->GetStaticIntField);
            GetStaticLongField = Ptrs.Proc<GetStaticLongField>(env->functions->GetStaticLongField);
            GetStaticFloatField = Ptrs.Proc<GetStaticFloatField>(env->functions->GetStaticFloatField);
            GetStaticDoubleField = Ptrs.Proc<GetStaticDoubleField>(env->functions->GetStaticDoubleField);

            SetStaticObjectField = Ptrs.Proc<SetStaticObjectField>(env->functions->SetStaticObjectField);
            SetStaticBooleanField = Ptrs.Proc<SetStaticBooleanField>(env->functions->SetStaticBooleanField);
            SetStaticByteField = Ptrs.Proc<SetStaticByteField>(env->functions->SetStaticByteField);
            SetStaticCharField = Ptrs.Proc<SetStaticCharField>(env->functions->SetStaticCharField);
            SetStaticShortField = Ptrs.Proc<SetStaticShortField>(env->functions->SetStaticShortField);
            SetStaticIntField = Ptrs.Proc<SetStaticIntField>(env->functions->SetStaticIntField);
            SetStaticLongField = Ptrs.Proc<SetStaticLongField>(env->functions->SetStaticLongField);
            SetStaticFloatField = Ptrs.Proc<SetStaticFloatField>(env->functions->SetStaticFloatField);
            SetStaticDoubleField = Ptrs.Proc<SetStaticDoubleField>(env->functions->SetStaticDoubleField);

            NewString = Ptrs.Proc<NewString>(env->functions->NewString);
            GetStringLength = Ptrs.Proc<GetStringLength>(env->functions->GetStringLength);
            GetStringChars = Ptrs.Proc<GetStringChars>(env->functions->GetStringChars);
            ReleaseStringChars = Ptrs.Proc<ReleaseStringChars>(env->functions->ReleaseStringChars);

            NewStringUTF = Ptrs.Proc<NewStringUTF>(env->functions->NewStringUTF);
            GetStringUTFLength = Ptrs.Proc<GetStringUTFLength>(env->functions->GetStringUTFLength);
            GetStringUTFChars = Ptrs.Proc<GetStringUTFChars>(env->functions->GetStringUTFChars);
            ReleaseStringUTFChars = Ptrs.Proc<ReleaseStringUTFChars>(env->functions->ReleaseStringUTFChars);


            GetArrayLength = Ptrs.Proc<GetArrayLength>(env->functions->GetArrayLength);

            NewObjectArray = Ptrs.Proc<NewObjectArray>(env->functions->NewObjectArray);
            GetObjectArrayElement = Ptrs.Proc<GetObjectArrayElement>(env->functions->GetObjectArrayElement);
            SetObjectArrayElement = Ptrs.Proc<SetObjectArrayElement>(env->functions->SetObjectArrayElement);

            NewBooleanArray = Ptrs.Proc<NewBooleanArray>(env->functions->NewBooleanArray);
            NewByteArray = Ptrs.Proc<NewByteArray>(env->functions->NewByteArray);
            NewCharArray = Ptrs.Proc<NewCharArray>(env->functions->NewCharArray);
            NewShortArray = Ptrs.Proc<NewShortArray>(env->functions->NewShortArray);
            NewIntArray = Ptrs.Proc<NewIntArray>(env->functions->NewIntArray);
            NewLongArray = Ptrs.Proc<NewLongArray>(env->functions->NewLongArray);
            NewFloatArray = Ptrs.Proc<NewFloatArray>(env->functions->NewFloatArray);
            NewDoubleArray = Ptrs.Proc<NewDoubleArray>(env->functions->NewDoubleArray);

            GetBooleanArrayElements = Ptrs.Proc<GetBooleanArrayElements>(env->functions->GetBooleanArrayElements);
            GetByteArrayElements = Ptrs.Proc<GetByteArrayElements>(env->functions->GetByteArrayElements);
            GetCharArrayElements = Ptrs.Proc<GetCharArrayElements>(env->functions->GetCharArrayElements);
            GetShortArrayElements = Ptrs.Proc<GetShortArrayElements>(env->functions->GetShortArrayElements);
            GetIntArrayElements = Ptrs.Proc<GetIntArrayElements>(env->functions->GetIntArrayElements);
            GetLongArrayElements = Ptrs.Proc<GetLongArrayElements>(env->functions->GetLongArrayElements);
            GetFloatArrayElements = Ptrs.Proc<GetFloatArrayElements>(env->functions->GetFloatArrayElements);
            GetDoubleArrayElements = Ptrs.Proc<GetDoubleArrayElements>(env->functions->GetDoubleArrayElements);

            ReleaseBooleanArrayElements = Ptrs.Proc<ReleaseBooleanArrayElements>(env->functions->ReleaseBooleanArrayElements);
            ReleaseByteArrayElements = Ptrs.Proc<ReleaseByteArrayElements>(env->functions->ReleaseByteArrayElements);
            ReleaseCharArrayElements = Ptrs.Proc<ReleaseCharArrayElements>(env->functions->ReleaseCharArrayElements);
            ReleaseShortArrayElements = Ptrs.Proc<ReleaseShortArrayElements>(env->functions->ReleaseShortArrayElements);
            ReleaseIntArrayElements = Ptrs.Proc<ReleaseIntArrayElements>(env->functions->ReleaseIntArrayElements);
            ReleaseLongArrayElements = Ptrs.Proc<ReleaseLongArrayElements>(env->functions->ReleaseLongArrayElements);
            ReleaseFloatArrayElements = Ptrs.Proc<ReleaseFloatArrayElements>(env->functions->ReleaseFloatArrayElements);
            ReleaseDoubleArrayElements = Ptrs.Proc<ReleaseDoubleArrayElements>(env->functions->ReleaseDoubleArrayElements);

            GetBooleanArrayRegion = Ptrs.Proc<GetBooleanArrayRegion>(env->functions->GetBooleanArrayRegion);
            GetByteArrayRegion = Ptrs.Proc<GetByteArrayRegion>(env->functions->GetByteArrayRegion);
            GetCharArrayRegion = Ptrs.Proc<GetCharArrayRegion>(env->functions->GetCharArrayRegion);
            GetShortArrayRegion = Ptrs.Proc<GetShortArrayRegion>(env->functions->GetShortArrayRegion);
            GetIntArrayRegion = Ptrs.Proc<GetIntArrayRegion>(env->functions->GetIntArrayRegion);
            GetLongArrayRegion = Ptrs.Proc<GetLongArrayRegion>(env->functions->GetLongArrayRegion);
            GetFloatArrayRegion = Ptrs.Proc<GetFloatArrayRegion>(env->functions->GetFloatArrayRegion);
            GetDoubleArrayRegion = Ptrs.Proc<GetDoubleArrayRegion>(env->functions->GetDoubleArrayRegion);

            SetBooleanArrayRegion = Ptrs.Proc<SetBooleanArrayRegion>(env->functions->SetBooleanArrayRegion);
            SetByteArrayRegion = Ptrs.Proc<SetByteArrayRegion>(env->functions->SetByteArrayRegion);
            SetCharArrayRegion = Ptrs.Proc<SetCharArrayRegion>(env->functions->SetCharArrayRegion);
            SetShortArrayRegion = Ptrs.Proc<SetShortArrayRegion>(env->functions->SetShortArrayRegion);
            SetIntArrayRegion = Ptrs.Proc<SetIntArrayRegion>(env->functions->SetIntArrayRegion);
            SetLongArrayRegion = Ptrs.Proc<SetLongArrayRegion>(env->functions->SetLongArrayRegion);
            SetFloatArrayRegion = Ptrs.Proc<SetFloatArrayRegion>(env->functions->SetFloatArrayRegion);
            SetDoubleArrayRegion = Ptrs.Proc<SetDoubleArrayRegion>(env->functions->SetDoubleArrayRegion);

            RegisterNatives = Ptrs.Proc<RegisterNatives>(env->functions->RegisterNatives);
            UnregisterNatives = Ptrs.Proc<UnregisterNatives>(env->functions->UnregisterNatives);

            MonitorEnter = Ptrs.Proc<MonitorEnter>(env->functions->MonitorEnter);
            MonitorExit = Ptrs.Proc<MonitorExit>(env->functions->MonitorExit);

            GetJavaVM = Ptrs.Proc<GetJavaVM>(env->functions->GetJavaVM);

            GetStringRegion = Ptrs.Proc<GetStringRegion>(env->functions->GetStringRegion);
            GetStringUTFRegion = Ptrs.Proc<GetStringUTFRegion>(env->functions->GetStringUTFRegion);

            GetPrimitiveArrayCritical = Ptrs.Proc<GetPrimitiveArrayCritical>(env->functions->GetPrimitiveArrayCritical);
            ReleasePrimitiveArrayCritical = Ptrs.Proc<ReleasePrimitiveArrayCritical>(env->functions->ReleasePrimitiveArrayCritical);

            GetStringCritical = Ptrs.Proc<GetStringCritical>(env->functions->GetStringCritical);
            ReleaseStringCritical = Ptrs.Proc<ReleaseStringCritical>(env->functions->ReleaseStringCritical);

            NewWeakGlobalRef = Ptrs.Proc<NewWeakGlobalRef>(env->functions->NewWeakGlobalRef);
            DeleteWeakGlobalRef = Ptrs.Proc<DeleteWeakGlobalRef>(env->functions->DeleteWeakGlobalRef);

            ExceptionCheck = Ptrs.Proc<ExceptionCheck>(env->functions->ExceptionCheck);

            NewDirectByteBuffer = Ptrs.Proc<NewDirectByteBuffer>(env->functions->NewDirectByteBuffer);
            GetDirectBufferAddress = Ptrs.Proc<GetDirectBufferAddress>(env->functions->GetDirectBufferAddress);
            GetDirectBufferCapacity = Ptrs.Proc<GetDirectBufferCapacity>(env->functions->GetDirectBufferCapacity);
            
            /* New JNI 1.6 Features */
            
            GetObjectRefType = Ptrs.Proc<GetObjectRefType>(env->functions->GetObjectRefType);
        }

        public readonly GetVersion GetVersion;

        public readonly DefineClass DefineClass;
        public readonly FindClass FindClass;

        public readonly FromReflectedMethod FromReflectedMethod;
        public readonly FromReflectedField FromReflectedField;

        public readonly ToReflectedMethod ToReflectedMethod;

        public readonly GetSuperclass GetSuperclass;
        public readonly IsAssignableFrom IsAssignableFrom;

        public readonly ToReflectedField ToReflectedField;

        public readonly Throw Throw;
        public readonly ThrowNew ThrowNew;
        public readonly ExceptionOccurred ExceptionOccurred;
        public readonly ExceptionDescribe ExceptionDescribe;
        public readonly ExceptionClear ExceptionClear;
        public readonly FatalError FatalError;

        public readonly PushLocalFrame PushLocalFrame;
        public readonly PopLocalFrame PopLocalFrame;

        public readonly NewGlobalRef NewGlobalRef;
        public readonly DeleteGlobalRef DeleteGlobalRef;
        public readonly DeleteLocalRef DeleteLocalRef;
        public readonly IsSameObject IsSameObject;
        public readonly NewLocalRef NewLocalRef;
        public readonly EnsureLocalCapacity EnsureLocalCapacity;

        public readonly AllocObject AllocObject;
        // public readonly NewObject NewObject;
        // public readonly NewObjectV NewObjectV;
        public readonly NewObjectA NewObjectA;

        public readonly GetObjectClass GetObjectClass;
        public readonly IsInstanceOf IsInstanceOf;

        public readonly GetMethodID GetMethodID;

        // public readonly CallObjectMethod CallObjectMethod;
        // public readonly CallObjectMethodV CallObjectMethodV;
        public readonly CallObjectMethodA CallObjectMethodA;

        // public readonly CallBooleanMethod CallBooleanMethod;
        // public readonly CallBooleanMethodV CallBooleanMethodV;
        public readonly CallBooleanMethodA CallBooleanMethodA;

        // public readonly CallByteMethod CallByteMethod;
        // public readonly CallByteMethodV CallByteMethodV;
        public readonly CallByteMethodA CallByteMethodA;

        // public readonly CallCharMethod CallCharMethod;
        // public readonly CallCharMethodV CallCharMethodV;
        public readonly CallCharMethodA CallCharMethodA;

        // public readonly CallShortMethod CallShortMethod;
        // public readonly CallShortMethodV CallShortMethodV;
        public readonly CallShortMethodA CallShortMethodA;

        // public readonly CallIntMethod CallIntMethod;
        // public readonly CallIntMethodV CallIntMethodV;
        public readonly CallIntMethodA CallIntMethodA;

        // public readonly CallLongMethod CallLongMethod;
        // public readonly CallLongMethodV CallLongMethodV;
        public readonly CallLongMethodA CallLongMethodA;

        // public readonly CallFloatMethod CallFloatMethod;
        // public readonly CallFloatMethodV CallFloatMethodV;
        public readonly CallFloatMethodA CallFloatMethodA;

        // public readonly CallDoubleMethod CallDoubleMethod;
        // public readonly CallDoubleMethodV CallDoubleMethodV;
        public readonly CallDoubleMethodA CallDoubleMethodA;

        // public readonly CallVoidMethod CallVoidMethod;
        // public readonly CallVoidMethodV CallVoidMethodV;
        public readonly CallVoidMethodA CallVoidMethodA;

        // public readonly CallNonvirtualObjectMethod CallNonvirtualObjectMethod;
        // public readonly CallNonvirtualObjectMethodV CallNonvirtualObjectMethodV;
        public readonly CallNonvirtualObjectMethodA CallNonvirtualObjectMethodA;

        // public readonly CallNonvirtualBooleanMethod CallNonvirtualBooleanMethod;
        // public readonly CallNonvirtualBooleanMethodV CallNonvirtualBooleanMethodV;
        public readonly CallNonvirtualBooleanMethodA CallNonvirtualBooleanMethodA;

        // public readonly CallNonvirtualByteMethod CallNonvirtualByteMethod;
        // public readonly CallNonvirtualByteMethodV CallNonvirtualByteMethodV;
        public readonly CallNonvirtualByteMethodA CallNonvirtualByteMethodA;

        // public readonly CallNonvirtualCharMethod CallNonvirtualCharMethod;
        // public readonly CallNonvirtualCharMethodV CallNonvirtualCharMethodV;
        public readonly CallNonvirtualCharMethodA CallNonvirtualCharMethodA;

        // public readonly CallNonvirtualShortMethod CallNonvirtualShortMethod;
        // public readonly CallNonvirtualShortMethodV CallNonvirtualShortMethodV;
        public readonly CallNonvirtualShortMethodA CallNonvirtualShortMethodA;

        // public readonly CallNonvirtualIntMethod CallNonvirtualIntMethod;
        // public readonly CallNonvirtualIntMethodV CallNonvirtualIntMethodV;
        public readonly CallNonvirtualIntMethodA CallNonvirtualIntMethodA;

        // public readonly CallNonvirtualLongMethod CallNonvirtualLongMethod;
        // public readonly CallNonvirtualLongMethodV CallNonvirtualLongMethodV;
        public readonly CallNonvirtualLongMethodA CallNonvirtualLongMethodA;

        // public readonly CallNonvirtualFloatMethod CallNonvirtualFloatMethod;
        // public readonly CallNonvirtualFloatMethodV CallNonvirtualFloatMethodV;
        public readonly CallNonvirtualFloatMethodA CallNonvirtualFloatMethodA;

        // public readonly CallNonvirtualDoubleMethod CallNonvirtualDoubleMethod;
        // public readonly CallNonvirtualDoubleMethodV CallNonvirtualDoubleMethodV;
        public readonly CallNonvirtualDoubleMethodA CallNonvirtualDoubleMethodA;

        // public readonly CallNonvirtualVoidMethod CallNonvirtualVoidMethod;
        // public readonly CallNonvirtualVoidMethodV CallNonvirtualVoidMethodV;
        public readonly CallNonvirtualVoidMethodA CallNonvirtualVoidMethodA;

        public readonly GetFieldID GetFieldID;

        public readonly GetObjectField GetObjectField;
        public readonly GetBooleanField GetBooleanField;
        public readonly GetByteField GetByteField;
        public readonly GetCharField GetCharField;
        public readonly GetShortField GetShortField;
        public readonly GetIntField GetIntField;
        public readonly GetLongField GetLongField;
        public readonly GetFloatField GetFloatField;
        public readonly GetDoubleField GetDoubleField;

        public readonly SetObjectField SetObjectField;
        public readonly SetBooleanField SetBooleanField;
        public readonly SetByteField SetByteField;
        public readonly SetCharField SetCharField;
        public readonly SetShortField SetShortField;
        public readonly SetIntField SetIntField;
        public readonly SetLongField SetLongField;
        public readonly SetFloatField SetFloatField;
        public readonly SetDoubleField SetDoubleField;

        public readonly GetStaticMethodID GetStaticMethodID;

        // public readonly CallStaticObjectMethod CallStaticObjectMethod;
        // public readonly CallStaticObjectMethodV CallStaticObjectMethodV;
        public readonly CallStaticObjectMethodA CallStaticObjectMethodA;

        // public readonly CallStaticBooleanMethod CallStaticBooleanMethod;
        // public readonly CallStaticBooleanMethodV CallStaticBooleanMethodV;
        public readonly CallStaticBooleanMethodA CallStaticBooleanMethodA;

        // public readonly CallStaticByteMethod CallStaticByteMethod;
        // public readonly CallStaticByteMethodV CallStaticByteMethodV;
        public readonly CallStaticByteMethodA CallStaticByteMethodA;

        // public readonly CallStaticCharMethod CallStaticCharMethod;
        // public readonly CallStaticCharMethodV CallStaticCharMethodV;
        public readonly CallStaticCharMethodA CallStaticCharMethodA;

        // public readonly CallStaticShortMethod CallStaticShortMethod;
        // public readonly CallStaticShortMethodV CallStaticShortMethodV;
        public readonly CallStaticShortMethodA CallStaticShortMethodA;

        // public readonly CallStaticIntMethod CallStaticIntMethod;
        // public readonly CallStaticIntMethodV CallStaticIntMethodV;
        public readonly CallStaticIntMethodA CallStaticIntMethodA;

        // public readonly CallStaticLongMethod CallStaticLongMethod;
        // public readonly CallStaticLongMethodV CallStaticLongMethodV;
        public readonly CallStaticLongMethodA CallStaticLongMethodA;

        // public readonly CallStaticFloatMethod CallStaticFloatMethod;
        // public readonly CallStaticFloatMethodV CallStaticFloatMethodV;
        public readonly CallStaticFloatMethodA CallStaticFloatMethodA;

        // public readonly CallStaticDoubleMethod CallStaticDoubleMethod;
        // public readonly CallStaticDoubleMethodV CallStaticDoubleMethodV;
        public readonly CallStaticDoubleMethodA CallStaticDoubleMethodA;

        // public readonly CallStaticVoidMethod CallStaticVoidMethod;
        // public readonly CallStaticVoidMethodV CallStaticVoidMethodV;
        public readonly CallStaticVoidMethodA CallStaticVoidMethodA;

        public readonly GetStaticFieldID GetStaticFieldID;
        public readonly GetStaticObjectField GetStaticObjectField;
        public readonly GetStaticBooleanField GetStaticBooleanField;
        public readonly GetStaticByteField GetStaticByteField;
        public readonly GetStaticCharField GetStaticCharField;
        public readonly GetStaticShortField GetStaticShortField;
        public readonly GetStaticIntField GetStaticIntField;
        public readonly GetStaticLongField GetStaticLongField;
        public readonly GetStaticFloatField GetStaticFloatField;
        public readonly GetStaticDoubleField GetStaticDoubleField;

        public readonly SetStaticObjectField SetStaticObjectField;
        public readonly SetStaticBooleanField SetStaticBooleanField;
        public readonly SetStaticByteField SetStaticByteField;
        public readonly SetStaticCharField SetStaticCharField;
        public readonly SetStaticShortField SetStaticShortField;
        public readonly SetStaticIntField SetStaticIntField;
        public readonly SetStaticLongField SetStaticLongField;
        public readonly SetStaticFloatField SetStaticFloatField;
        public readonly SetStaticDoubleField SetStaticDoubleField;

        public readonly NewString NewString;
        public readonly GetStringLength GetStringLength;
        public readonly GetStringChars GetStringChars;
        public readonly ReleaseStringChars ReleaseStringChars;

        public readonly NewStringUTF NewStringUTF;
        public readonly GetStringUTFLength GetStringUTFLength;
        public readonly GetStringUTFChars GetStringUTFChars;
        public readonly ReleaseStringUTFChars ReleaseStringUTFChars;


        public readonly GetArrayLength GetArrayLength;

        public readonly NewObjectArray NewObjectArray;
        public readonly GetObjectArrayElement GetObjectArrayElement;
        public readonly SetObjectArrayElement SetObjectArrayElement;

        public readonly NewBooleanArray NewBooleanArray;
        public readonly NewByteArray NewByteArray;
        public readonly NewCharArray NewCharArray;
        public readonly NewShortArray NewShortArray;
        public readonly NewIntArray NewIntArray;
        public readonly NewLongArray NewLongArray;
        public readonly NewFloatArray NewFloatArray;
        public readonly NewDoubleArray NewDoubleArray;

        public readonly GetBooleanArrayElements GetBooleanArrayElements;
        public readonly GetByteArrayElements GetByteArrayElements;
        public readonly GetCharArrayElements GetCharArrayElements;
        public readonly GetShortArrayElements GetShortArrayElements;
        public readonly GetIntArrayElements GetIntArrayElements;
        public readonly GetLongArrayElements GetLongArrayElements;
        public readonly GetFloatArrayElements GetFloatArrayElements;
        public readonly GetDoubleArrayElements GetDoubleArrayElements;

        public readonly ReleaseBooleanArrayElements ReleaseBooleanArrayElements;
        public readonly ReleaseByteArrayElements ReleaseByteArrayElements;
        public readonly ReleaseCharArrayElements ReleaseCharArrayElements;
        public readonly ReleaseShortArrayElements ReleaseShortArrayElements;
        public readonly ReleaseIntArrayElements ReleaseIntArrayElements;
        public readonly ReleaseLongArrayElements ReleaseLongArrayElements;
        public readonly ReleaseFloatArrayElements ReleaseFloatArrayElements;
        public readonly ReleaseDoubleArrayElements ReleaseDoubleArrayElements;

        public readonly GetBooleanArrayRegion GetBooleanArrayRegion;
        public readonly GetByteArrayRegion GetByteArrayRegion;
        public readonly GetCharArrayRegion GetCharArrayRegion;
        public readonly GetShortArrayRegion GetShortArrayRegion;
        public readonly GetIntArrayRegion GetIntArrayRegion;
        public readonly GetLongArrayRegion GetLongArrayRegion;
        public readonly GetFloatArrayRegion GetFloatArrayRegion;
        public readonly GetDoubleArrayRegion GetDoubleArrayRegion;

        public readonly SetBooleanArrayRegion SetBooleanArrayRegion;
        public readonly SetByteArrayRegion SetByteArrayRegion;
        public readonly SetCharArrayRegion SetCharArrayRegion;
        public readonly SetShortArrayRegion SetShortArrayRegion;
        public readonly SetIntArrayRegion SetIntArrayRegion;
        public readonly SetLongArrayRegion SetLongArrayRegion;
        public readonly SetFloatArrayRegion SetFloatArrayRegion;
        public readonly SetDoubleArrayRegion SetDoubleArrayRegion;

        public readonly RegisterNatives RegisterNatives;
        public readonly UnregisterNatives UnregisterNatives;

        public readonly MonitorEnter MonitorEnter;
        public readonly MonitorExit MonitorExit;

        public readonly GetJavaVM GetJavaVM;

        public readonly GetStringRegion GetStringRegion;
        public readonly GetStringUTFRegion GetStringUTFRegion;

        public readonly GetPrimitiveArrayCritical GetPrimitiveArrayCritical;
        public readonly ReleasePrimitiveArrayCritical ReleasePrimitiveArrayCritical;

        public readonly GetStringCritical GetStringCritical;
        public readonly ReleaseStringCritical ReleaseStringCritical;

        public readonly NewWeakGlobalRef NewWeakGlobalRef;
        public readonly DeleteWeakGlobalRef DeleteWeakGlobalRef;

        public readonly ExceptionCheck ExceptionCheck;

        public readonly NewDirectByteBuffer NewDirectByteBuffer;
        public readonly GetDirectBufferAddress GetDirectBufferAddress;
        public readonly GetDirectBufferCapacity GetDirectBufferCapacity;
        
        /* New JNI 1.6 Features */
        
        public readonly GetObjectRefType GetObjectRefType;
    }
}