using Jni4CSharp.CommonUtils;

namespace Jni4CSharp.Bindings.Functions
{
    // ReSharper disable InconsistentNaming
    // ReSharper disable CommentTypo
    // ReSharper disable IdentifierTypo
    public class InvocationAPIFunctions
    {
        public InvocationAPIFunctions(DynamicLibrary library)
        {
            JNI_GetDefaultJavaVMInitArgs = library.GetProc<JNI_GetDefaultJavaVMInitArgs>();
            JNI_CreateJavaVM = library.GetProc<JNI_CreateJavaVM>();
            JNI_GetCreatedJavaVMs = library.GetProc<JNI_GetCreatedJavaVMs>();
        }

        public readonly JNI_GetDefaultJavaVMInitArgs JNI_GetDefaultJavaVMInitArgs;
        public readonly JNI_CreateJavaVM JNI_CreateJavaVM;
        public readonly JNI_GetCreatedJavaVMs JNI_GetCreatedJavaVMs;
        
    }
}