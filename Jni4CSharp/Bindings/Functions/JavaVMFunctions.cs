using Jni4CSharp.CommonUtils;

namespace Jni4CSharp.Bindings.Functions
{
    // ReSharper disable InconsistentNaming
    // ReSharper disable CommentTypo
    // ReSharper disable IdentifierTypo
    public unsafe class JavaVMFunctions
    {

        public JavaVMFunctions(JavaVM_* vm)
        {
            DestroyJavaVM = Ptrs.Proc<DestroyJavaVM>(vm->functions->DestroyJavaVM);
            
            AttachCurrentThread = Ptrs.Proc<AttachCurrentThread>(vm->functions->AttachCurrentThread);
            
            DetachCurrentThread = Ptrs.Proc<DetachCurrentThread>(vm->functions->DetachCurrentThread);
            
            GetEnv = Ptrs.Proc<GetEnv>(vm->functions->GetEnv);
            
            AttachCurrentThreadAsDaemon = Ptrs.Proc<AttachCurrentThreadAsDaemon>(vm->functions->AttachCurrentThreadAsDaemon);
        }
    

        public readonly DestroyJavaVM DestroyJavaVM;

        public readonly AttachCurrentThread AttachCurrentThread;

        public readonly DetachCurrentThread DetachCurrentThread;

        public readonly GetEnv GetEnv;

        public readonly AttachCurrentThreadAsDaemon AttachCurrentThreadAsDaemon;
    }
}