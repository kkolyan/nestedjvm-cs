using Jni4CSharp.Bindings.Functions;

namespace Jni4CSharp.Bindings
{
    // ReSharper disable InconsistentNaming
    // ReSharper disable CommentTypo
    // ReSharper disable IdentifierTypo
    // ReSharper disable UnusedMember.Global
    // ReSharper disable once UnusedType.Global
    
    using jbyte = System.Byte;
    using jshort = System.Int16;
    using jint = System.Int32;
    using jlong = System.Int64;
    using jfloat = System.Single;
    using jdouble = System.Double;
    using jchar = System.UInt16;
    using jsize = System.Int32;
    
    public readonly unsafe struct JNIEnv
    {

        private readonly JNIEnv_* _env;
        private readonly JNIEnvFunctions _functions;

        public JNIEnv(JNIEnv_* env)
        {
            _env = env;
            _functions = new JNIEnvFunctions(env);
        }

        public jint GetVersion() {
            return _functions.GetVersion(_env);
        }
        public jclass DefineClass(string name, jobject loader, jbyte *buf,
                           jsize len) {
            return _functions.DefineClass(_env, name, loader, buf, len);
        }
        public jclass FindClass(string name) {
            return _functions.FindClass(_env, name);
        }
        public jmethodID FromReflectedMethod(jobject method) {
            return _functions.FromReflectedMethod(_env,method);
        }
        public jfieldID FromReflectedField(jobject field) {
            return _functions.FromReflectedField(_env,field);
        }

        public jobject ToReflectedMethod(jclass cls, jmethodID methodID, jboolean isStatic) {
            return _functions.ToReflectedMethod(_env, cls, methodID, isStatic);
        }

        public jclass GetSuperclass(jclass sub) {
            return _functions.GetSuperclass(_env, sub);
        }
        public jboolean IsAssignableFrom(jclass sub, jclass sup) {
            return _functions.IsAssignableFrom(_env, sub, sup);
        }

        public jobject ToReflectedField(jclass cls, jfieldID fieldID, jboolean isStatic) {
            return _functions.ToReflectedField(_env,cls,fieldID,isStatic);
        }

        public jint Throw(jthrowable obj) {
            return _functions.Throw(_env, obj);
        }
        public jint ThrowNew(jclass clazz, byte* msg) {
            return _functions.ThrowNew(_env, clazz, msg);
        }
        public jthrowable ExceptionOccurred() {
            return _functions.ExceptionOccurred(_env);
        }
        public void ExceptionDescribe() {
            _functions.ExceptionDescribe(_env);
        }
        public void ExceptionClear() {
            _functions.ExceptionClear(_env);
        }
        public void FatalError(string msg) {
            _functions.FatalError(_env, msg);
        }

        public jint PushLocalFrame(jint capacity) {
            return _functions.PushLocalFrame(_env,capacity);
        }
        public jobject PopLocalFrame(jobject result) {
            return _functions.PopLocalFrame(_env,result);
        }

        public jobject NewGlobalRef(jobject lobj) {
            return _functions.NewGlobalRef(_env,lobj);
        }
        public void DeleteGlobalRef(jobject gref) {
            _functions.DeleteGlobalRef(_env,gref);
        }
        public void DeleteLocalRef(jobject obj) {
            _functions.DeleteLocalRef(_env, obj);
        }

         public jboolean IsSameObject(jobject obj1, jobject obj2) {
            return _functions.IsSameObject(_env,obj1,obj2);
        }

        public jobject NewLocalRef(jobject _ref) {
            return _functions.NewLocalRef(_env,_ref);
        }
        public jint EnsureLocalCapacity(jint capacity) {
            return _functions.EnsureLocalCapacity(_env,capacity);
        }

        public jobject AllocObject(jclass clazz) {
            return _functions.AllocObject(_env,clazz);
        }
        // jobject NewObject(jclass clazz, jmethodID methodID, ...) {
        //     va_list args;
        //     jobject result;
        //     va_start(args, methodID);
        //     result = functions.NewObjectV(_env,clazz,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jobject NewObjectV(jclass clazz, jmethodID methodID,
        //                    va_list args) {
        //     return functions.NewObjectV(_env,clazz,methodID,args);
        // }
        public jobject NewObjectA(jclass clazz, jmethodID methodID,
                           jvalue *args) {
            return _functions.NewObjectA(_env,clazz,methodID,args);
        }

        public jclass GetObjectClass(jobject obj) {
            return _functions.GetObjectClass(_env,obj);
        }
        public jboolean IsInstanceOf(jobject obj, jclass clazz) {
            return _functions.IsInstanceOf(_env,obj,clazz);
        }

        public jmethodID GetMethodID(jclass clazz, string name,
                              string sig) {
            return _functions.GetMethodID(_env,clazz,name,sig);
        }

        // jobject CallObjectMethod(jobject obj, jmethodID methodID, ...) {
        //     va_list args;
        //     jobject result;
        //     va_start(args,methodID);
        //     result = functions.CallObjectMethodV(_env,obj,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jobject CallObjectMethodV(jobject obj, jmethodID methodID,
        //                     va_list args) {
        //     return functions.CallObjectMethodV(_env,obj,methodID,args);
        // }
        public jobject CallObjectMethodA(jobject obj, jmethodID methodID,
                            jvalue * args) {
            return _functions.CallObjectMethodA(_env,obj,methodID,args);
        }

        // jboolean CallBooleanMethod(jobject obj,
        //                            jmethodID methodID, ...) {
        //     va_list args;
        //     jboolean result;
        //     va_start(args,methodID);
        //     result = functions.CallBooleanMethodV(_env,obj,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jboolean CallBooleanMethodV(jobject obj, jmethodID methodID,
        //                             va_list args) {
        //     return functions.CallBooleanMethodV(_env,obj,methodID,args);
        // }
        public jboolean CallBooleanMethodA(jobject obj, jmethodID methodID,
                                    jvalue * args) {
            return _functions.CallBooleanMethodA(_env,obj,methodID, args);
        }

        // jbyte CallByteMethod(jobject obj, jmethodID methodID, ...) {
        //     va_list args;
        //     jbyte result;
        //     va_start(args,methodID);
        //     result = functions.CallByteMethodV(_env,obj,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jbyte CallByteMethodV(jobject obj, jmethodID methodID,
        //                       va_list args) {
        //     return functions.CallByteMethodV(_env,obj,methodID,args);
        // }
        public jbyte CallByteMethodA(jobject obj, jmethodID methodID,
                              jvalue * args) {
            return _functions.CallByteMethodA(_env,obj,methodID,args);
        }

        // jchar CallCharMethod(jobject obj, jmethodID methodID, ...) {
        //     va_list args;
        //     jchar result;
        //     va_start(args,methodID);
        //     result = functions.CallCharMethodV(_env,obj,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jchar CallCharMethodV(jobject obj, jmethodID methodID,
        //                       va_list args) {
        //     return functions.CallCharMethodV(_env,obj,methodID,args);
        // }
        public jchar CallCharMethodA(jobject obj, jmethodID methodID,
                              jvalue * args) {
            return _functions.CallCharMethodA(_env,obj,methodID,args);
        }

        // jshort CallShortMethod(jobject obj, jmethodID methodID, ...) {
        //     va_list args;
        //     jshort result;
        //     va_start(args,methodID);
        //     result = functions.CallShortMethodV(_env,obj,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jshort CallShortMethodV(jobject obj, jmethodID methodID,
        //                         va_list args) {
        //     return functions.CallShortMethodV(_env,obj,methodID,args);
        // }
        public jshort CallShortMethodA(jobject obj, jmethodID methodID,
                                jvalue * args) {
            return _functions.CallShortMethodA(_env,obj,methodID,args);
        }

        // jint CallIntMethod(jobject obj, jmethodID methodID, ...) {
        //     va_list args;
        //     jint result;
        //     va_start(args,methodID);
        //     result = functions.CallIntMethodV(_env,obj,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jint CallIntMethodV(jobject obj, jmethodID methodID,
        //                     va_list args) {
        //     return functions.CallIntMethodV(_env,obj,methodID,args);
        // }
        public jint CallIntMethodA(jobject obj, jmethodID methodID,
                            jvalue * args) {
            return _functions.CallIntMethodA(_env,obj,methodID,args);
        }

        // jlong CallLongMethod(jobject obj, jmethodID methodID, ...) {
        //     va_list args;
        //     jlong result;
        //     va_start(args,methodID);
        //     result = functions.CallLongMethodV(_env,obj,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jlong CallLongMethodV(jobject obj, jmethodID methodID,
        //                       va_list args) {
        //     return functions.CallLongMethodV(_env,obj,methodID,args);
        // }
        public jlong CallLongMethodA(jobject obj, jmethodID methodID,
                              jvalue * args) {
            return _functions.CallLongMethodA(_env,obj,methodID,args);
        }

        // jfloat CallFloatMethod(jobject obj, jmethodID methodID, ...) {
        //     va_list args;
        //     jfloat result;
        //     va_start(args,methodID);
        //     result = functions.CallFloatMethodV(_env,obj,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jfloat CallFloatMethodV(jobject obj, jmethodID methodID,
        //                         va_list args) {
        //     return functions.CallFloatMethodV(_env,obj,methodID,args);
        // }
        public jfloat CallFloatMethodA(jobject obj, jmethodID methodID,
                                jvalue * args) {
            return _functions.CallFloatMethodA(_env,obj,methodID,args);
        }

        // jdouble CallDoubleMethod(jobject obj, jmethodID methodID, ...) {
        //     va_list args;
        //     jdouble result;
        //     va_start(args,methodID);
        //     result = functions.CallDoubleMethodV(_env,obj,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jdouble CallDoubleMethodV(jobject obj, jmethodID methodID,
        //                     va_list args) {
        //     return functions.CallDoubleMethodV(_env,obj,methodID,args);
        // }
        public jdouble CallDoubleMethodA(jobject obj, jmethodID methodID,
                            jvalue * args) {
            return _functions.CallDoubleMethodA(_env,obj,methodID,args);
        }

        // void CallVoidMethod(jobject obj, jmethodID methodID, ...) {
        //     va_list args;
        //     va_start(args,methodID);
        //     functions.CallVoidMethodV(_env,obj,methodID,args);
        //     va_end(args);
        // }
        // void CallVoidMethodV(jobject obj, jmethodID methodID,
        //                      va_list args) {
        //     functions.CallVoidMethodV(_env,obj,methodID,args);
        // }
        public void CallVoidMethodA(jobject obj, jmethodID methodID,
                             jvalue * args) {
            _functions.CallVoidMethodA(_env,obj,methodID,args);
        }

        // jobject CallNonvirtualObjectMethod(jobject obj, jclass clazz,
        //                                    jmethodID methodID, ...) {
        //     va_list args;
        //     jobject result;
        //     va_start(args,methodID);
        //     result = functions.CallNonvirtualObjectMethodV(_env,obj,clazz,
        //                                                     methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jobject CallNonvirtualObjectMethodV(jobject obj, jclass clazz,
        //                                     jmethodID methodID, va_list args) {
        //     return functions.CallNonvirtualObjectMethodV(_env,obj,clazz,
        //                                                   methodID,args);
        // }
        public jobject CallNonvirtualObjectMethodA(jobject obj, jclass clazz,
                                            jmethodID methodID, jvalue * args) {
            return _functions.CallNonvirtualObjectMethodA(_env,obj,clazz,
                                                          methodID,args);
        }

        // jboolean CallNonvirtualBooleanMethod(jobject obj, jclass clazz,
        //                                      jmethodID methodID, ...) {
        //     va_list args;
        //     jboolean result;
        //     va_start(args,methodID);
        //     result = functions.CallNonvirtualBooleanMethodV(_env,obj,clazz,
        //                                                      methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jboolean CallNonvirtualBooleanMethodV(jobject obj, jclass clazz,
        //                                       jmethodID methodID, va_list args) {
        //     return functions.CallNonvirtualBooleanMethodV(_env,obj,clazz,
        //                                                    methodID,args);
        // }
        public jboolean CallNonvirtualBooleanMethodA(jobject obj, jclass clazz,
                                              jmethodID methodID, jvalue * args) {
            return _functions.CallNonvirtualBooleanMethodA(_env,obj,clazz,
                                                           methodID, args);
        }

        // jbyte CallNonvirtualByteMethod(jobject obj, jclass clazz,
        //                                jmethodID methodID, ...) {
        //     va_list args;
        //     jbyte result;
        //     va_start(args,methodID);
        //     result = functions.CallNonvirtualByteMethodV(_env,obj,clazz,
        //                                                   methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jbyte CallNonvirtualByteMethodV(jobject obj, jclass clazz,
        //                                 jmethodID methodID, va_list args) {
        //     return functions.CallNonvirtualByteMethodV(_env,obj,clazz,
        //                                                 methodID,args);
        // }
        public jbyte CallNonvirtualByteMethodA(jobject obj, jclass clazz,
                                        jmethodID methodID, jvalue * args) {
            return _functions.CallNonvirtualByteMethodA(_env,obj,clazz,
                                                        methodID,args);
        }

        // jchar CallNonvirtualCharMethod(jobject obj, jclass clazz,
        //                                jmethodID methodID, ...) {
        //     va_list args;
        //     jchar result;
        //     va_start(args,methodID);
        //     result = functions.CallNonvirtualCharMethodV(_env,obj,clazz,
        //                                                   methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jchar CallNonvirtualCharMethodV(jobject obj, jclass clazz,
        //                                 jmethodID methodID, va_list args) {
        //     return functions.CallNonvirtualCharMethodV(_env,obj,clazz,
        //                                                 methodID,args);
        // }
        public jchar CallNonvirtualCharMethodA(jobject obj, jclass clazz,
                                        jmethodID methodID, jvalue * args) {
            return _functions.CallNonvirtualCharMethodA(_env,obj,clazz,
                                                        methodID,args);
        }

        // jshort CallNonvirtualShortMethod(jobject obj, jclass clazz,
        //                                  jmethodID methodID, ...) {
        //     va_list args;
        //     jshort result;
        //     va_start(args,methodID);
        //     result = functions.CallNonvirtualShortMethodV(_env,obj,clazz,
        //                                                    methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jshort CallNonvirtualShortMethodV(jobject obj, jclass clazz,
        //                                   jmethodID methodID, va_list args) {
        //     return functions.CallNonvirtualShortMethodV(_env,obj,clazz,
        //                                                  methodID,args);
        // }
        public jshort CallNonvirtualShortMethodA(jobject obj, jclass clazz,
                                          jmethodID methodID, jvalue * args) {
            return _functions.CallNonvirtualShortMethodA(_env,obj,clazz,
                                                         methodID,args);
        }

        // jint CallNonvirtualIntMethod(jobject obj, jclass clazz,
        //                              jmethodID methodID, ...) {
        //     va_list args;
        //     jint result;
        //     va_start(args,methodID);
        //     result = functions.CallNonvirtualIntMethodV(_env,obj,clazz,
        //                                                  methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jint CallNonvirtualIntMethodV(jobject obj, jclass clazz,
        //                               jmethodID methodID, va_list args) {
        //     return functions.CallNonvirtualIntMethodV(_env,obj,clazz,
        //                                                methodID,args);
        // }
        public jint CallNonvirtualIntMethodA(jobject obj, jclass clazz,
                                      jmethodID methodID, jvalue * args) {
            return _functions.CallNonvirtualIntMethodA(_env,obj,clazz,
                                                       methodID,args);
        }

        // jlong CallNonvirtualLongMethod(jobject obj, jclass clazz,
        //                                jmethodID methodID, ...) {
        //     va_list args;
        //     jlong result;
        //     va_start(args,methodID);
        //     result = functions.CallNonvirtualLongMethodV(_env,obj,clazz,
        //                                                   methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jlong CallNonvirtualLongMethodV(jobject obj, jclass clazz,
        //                                 jmethodID methodID, va_list args) {
        //     return functions.CallNonvirtualLongMethodV(_env,obj,clazz,
        //                                                 methodID,args);
        // }
        public jlong CallNonvirtualLongMethodA(jobject obj, jclass clazz,
                                        jmethodID methodID, jvalue * args) {
            return _functions.CallNonvirtualLongMethodA(_env,obj,clazz,
                                                        methodID,args);
        }

        // jfloat CallNonvirtualFloatMethod(jobject obj, jclass clazz,
        //                                  jmethodID methodID, ...) {
        //     va_list args;
        //     jfloat result;
        //     va_start(args,methodID);
        //     result = functions.CallNonvirtualFloatMethodV(_env,obj,clazz,
        //                                                    methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jfloat CallNonvirtualFloatMethodV(jobject obj, jclass clazz,
        //                                   jmethodID methodID,
        //                                   va_list args) {
        //     return functions.CallNonvirtualFloatMethodV(_env,obj,clazz,
        //                                                  methodID,args);
        // }
        public jfloat CallNonvirtualFloatMethodA(jobject obj, jclass clazz,
                                          jmethodID methodID,
                                          jvalue * args) {
            return _functions.CallNonvirtualFloatMethodA(_env,obj,clazz,
                                                         methodID,args);
        }

        // jdouble CallNonvirtualDoubleMethod(jobject obj, jclass clazz,
        //                                    jmethodID methodID, ...) {
        //     va_list args;
        //     jdouble result;
        //     va_start(args,methodID);
        //     result = functions.CallNonvirtualDoubleMethodV(_env,obj,clazz,
        //                                                     methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jdouble CallNonvirtualDoubleMethodV(jobject obj, jclass clazz,
        //                                     jmethodID methodID,
        //                                     va_list args) {
        //     return functions.CallNonvirtualDoubleMethodV(_env,obj,clazz,
        //                                                   methodID,args);
        // }
        public jdouble CallNonvirtualDoubleMethodA(jobject obj, jclass clazz,
                                            jmethodID methodID,
                                            jvalue * args) {
            return _functions.CallNonvirtualDoubleMethodA(_env,obj,clazz,
                                                          methodID,args);
        }

        // void CallNonvirtualVoidMethod(jobject obj, jclass clazz,
        //                               jmethodID methodID, ...) {
        //     va_list args;
        //     va_start(args,methodID);
        //     functions.CallNonvirtualVoidMethodV(_env,obj,clazz,methodID,args);
        //     va_end(args);
        // }
        // void CallNonvirtualVoidMethodV(jobject obj, jclass clazz,
        //                                jmethodID methodID,
        //                                va_list args) {
        //     functions.CallNonvirtualVoidMethodV(_env,obj,clazz,methodID,args);
        // }
        public void CallNonvirtualVoidMethodA(jobject obj, jclass clazz,
                                       jmethodID methodID,
                                       jvalue * args) {
            _functions.CallNonvirtualVoidMethodA(_env,obj,clazz,methodID,args);
        }

        public jfieldID GetFieldID(jclass clazz, string name,
                            string sig) {
            return _functions.GetFieldID(_env,clazz,name,sig);
        }

        public jobject GetObjectField(jobject obj, jfieldID fieldID) {
            return _functions.GetObjectField(_env,obj,fieldID);
        }
        public jboolean GetBooleanField(jobject obj, jfieldID fieldID) {
            return _functions.GetBooleanField(_env,obj,fieldID);
        }
        public jbyte GetByteField(jobject obj, jfieldID fieldID) {
            return _functions.GetByteField(_env,obj,fieldID);
        }
        public jchar GetCharField(jobject obj, jfieldID fieldID) {
            return _functions.GetCharField(_env,obj,fieldID);
        }
        public jshort GetShortField(jobject obj, jfieldID fieldID) {
            return _functions.GetShortField(_env,obj,fieldID);
        }
        public jint GetIntField(jobject obj, jfieldID fieldID) {
            return _functions.GetIntField(_env,obj,fieldID);
        }
        public jlong GetLongField(jobject obj, jfieldID fieldID) {
            return _functions.GetLongField(_env,obj,fieldID);
        }
        public jfloat GetFloatField(jobject obj, jfieldID fieldID) {
            return _functions.GetFloatField(_env,obj,fieldID);
        }
        public jdouble GetDoubleField(jobject obj, jfieldID fieldID) {
            return _functions.GetDoubleField(_env,obj,fieldID);
        }

        public void SetObjectField(jobject obj, jfieldID fieldID, jobject val) {
            _functions.SetObjectField(_env,obj,fieldID,val);
        }
        public void SetBooleanField(jobject obj, jfieldID fieldID,
                             jboolean val) {
            _functions.SetBooleanField(_env,obj,fieldID,val);
        }
        public void SetByteField(jobject obj, jfieldID fieldID,
                          jbyte val) {
            _functions.SetByteField(_env,obj,fieldID,val);
        }
        public void SetCharField(jobject obj, jfieldID fieldID,
                          jchar val) {
            _functions.SetCharField(_env,obj,fieldID,val);
        }
        public void SetShortField(jobject obj, jfieldID fieldID,
                           jshort val) {
            _functions.SetShortField(_env,obj,fieldID,val);
        }
        public void SetIntField(jobject obj, jfieldID fieldID,
                         jint val) {
            _functions.SetIntField(_env,obj,fieldID,val);
        }
        public void SetLongField(jobject obj, jfieldID fieldID,
                          jlong val) {
            _functions.SetLongField(_env,obj,fieldID,val);
        }
        public void SetFloatField(jobject obj, jfieldID fieldID,
                           jfloat val) {
            _functions.SetFloatField(_env,obj,fieldID,val);
        }
        public void SetDoubleField(jobject obj, jfieldID fieldID,
                            jdouble val) {
            _functions.SetDoubleField(_env,obj,fieldID,val);
        }

        public jmethodID GetStaticMethodID(jclass clazz, string name,
                                    string sig) {
            return _functions.GetStaticMethodID(_env,clazz,name,sig);
        }

        // jobject CallStaticObjectMethod(jclass clazz, jmethodID methodID,
        //                          ...) {
        //     va_list args;
        //     jobject result;
        //     va_start(args,methodID);
        //     result = functions.CallStaticObjectMethodV(_env,clazz,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jobject CallStaticObjectMethodV(jclass clazz, jmethodID methodID,
        //                           va_list args) {
        //     return functions.CallStaticObjectMethodV(_env,clazz,methodID,args);
        // }
        public jobject CallStaticObjectMethodA(jclass clazz, jmethodID methodID,
                                  jvalue *args) {
            return _functions.CallStaticObjectMethodA(_env,clazz,methodID,args);
        }

        // jboolean CallStaticBooleanMethod(jclass clazz,
        //                                  jmethodID methodID, ...) {
        //     va_list args;
        //     jboolean result;
        //     va_start(args,methodID);
        //     result = functions.CallStaticBooleanMethodV(_env,clazz,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jboolean CallStaticBooleanMethodV(jclass clazz,
        //                                   jmethodID methodID, va_list args) {
        //     return functions.CallStaticBooleanMethodV(_env,clazz,methodID,args);
        // }
        public jboolean CallStaticBooleanMethodA(jclass clazz,
                                          jmethodID methodID, jvalue *args) {
            return _functions.CallStaticBooleanMethodA(_env,clazz,methodID,args);
        }

        // jbyte CallStaticByteMethod(jclass clazz,
        //                            jmethodID methodID, ...) {
        //     va_list args;
        //     jbyte result;
        //     va_start(args,methodID);
        //     result = functions.CallStaticByteMethodV(_env,clazz,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jbyte CallStaticByteMethodV(jclass clazz,
        //                             jmethodID methodID, va_list args) {
        //     return functions.CallStaticByteMethodV(_env,clazz,methodID,args);
        // }
        public jbyte CallStaticByteMethodA(jclass clazz,
                                    jmethodID methodID, jvalue *args) {
            return _functions.CallStaticByteMethodA(_env,clazz,methodID,args);
        }

        // jchar CallStaticCharMethod(jclass clazz,
        //                            jmethodID methodID, ...) {
        //     va_list args;
        //     jchar result;
        //     va_start(args,methodID);
        //     result = functions.CallStaticCharMethodV(_env,clazz,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jchar CallStaticCharMethodV(jclass clazz,
        //                             jmethodID methodID, va_list args) {
        //     return functions.CallStaticCharMethodV(_env,clazz,methodID,args);
        // }
        public jchar CallStaticCharMethodA(jclass clazz,
                                    jmethodID methodID, jvalue *args) {
            return _functions.CallStaticCharMethodA(_env,clazz,methodID,args);
        }

        // jshort CallStaticShortMethod(jclass clazz,
        //                              jmethodID methodID, ...) {
        //     va_list args;
        //     jshort result;
        //     va_start(args,methodID);
        //     result = functions.CallStaticShortMethodV(_env,clazz,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jshort CallStaticShortMethodV(jclass clazz,
        //                               jmethodID methodID, va_list args) {
        //     return functions.CallStaticShortMethodV(_env,clazz,methodID,args);
        // }
        public jshort CallStaticShortMethodA(jclass clazz,
                                      jmethodID methodID, jvalue *args) {
            return _functions.CallStaticShortMethodA(_env,clazz,methodID,args);
        }

        // jint CallStaticIntMethod(jclass clazz,
        //                          jmethodID methodID, ...) {
        //     va_list args;
        //     jint result;
        //     va_start(args,methodID);
        //     result = functions.CallStaticIntMethodV(_env,clazz,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jint CallStaticIntMethodV(jclass clazz,
        //                           jmethodID methodID, va_list args) {
        //     return functions.CallStaticIntMethodV(_env,clazz,methodID,args);
        // }
        public jint CallStaticIntMethodA(jclass clazz,
                                  jmethodID methodID, jvalue *args) {
            return _functions.CallStaticIntMethodA(_env,clazz,methodID,args);
        }

        // jlong CallStaticLongMethod(jclass clazz,
        //                            jmethodID methodID, ...) {
        //     va_list args;
        //     jlong result;
        //     va_start(args,methodID);
        //     result = functions.CallStaticLongMethodV(_env,clazz,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jlong CallStaticLongMethodV(jclass clazz,
        //                             jmethodID methodID, va_list args) {
        //     return functions.CallStaticLongMethodV(_env,clazz,methodID,args);
        // }
        public jlong CallStaticLongMethodA(jclass clazz,
                                    jmethodID methodID, jvalue *args) {
            return _functions.CallStaticLongMethodA(_env,clazz,methodID,args);
        }

        // jfloat CallStaticFloatMethod(jclass clazz,
        //                              jmethodID methodID, ...) {
        //     va_list args;
        //     jfloat result;
        //     va_start(args,methodID);
        //     result = functions.CallStaticFloatMethodV(_env,clazz,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jfloat CallStaticFloatMethodV(jclass clazz,
        //                               jmethodID methodID, va_list args) {
        //     return functions.CallStaticFloatMethodV(_env,clazz,methodID,args);
        // }
        public jfloat CallStaticFloatMethodA(jclass clazz,
                                      jmethodID methodID, jvalue *args) {
            return _functions.CallStaticFloatMethodA(_env,clazz,methodID,args);
        }

        // jdouble CallStaticDoubleMethod(jclass clazz,
        //                                jmethodID methodID, ...) {
        //     va_list args;
        //     jdouble result;
        //     va_start(args,methodID);
        //     result = functions.CallStaticDoubleMethodV(_env,clazz,methodID,args);
        //     va_end(args);
        //     return result;
        // }
        // jdouble CallStaticDoubleMethodV(jclass clazz,
        //                                 jmethodID methodID, va_list args) {
        //     return functions.CallStaticDoubleMethodV(_env,clazz,methodID,args);
        // }
        public jdouble CallStaticDoubleMethodA(jclass clazz,
                                        jmethodID methodID, jvalue *args) {
            return _functions.CallStaticDoubleMethodA(_env,clazz,methodID,args);
        }

        // void CallStaticVoidMethod(jclass cls, jmethodID methodID, ...) {
        //     va_list args;
        //     va_start(args,methodID);
        //     functions.CallStaticVoidMethodV(_env,cls,methodID,args);
        //     va_end(args);
        // }
        // void CallStaticVoidMethodV(jclass cls, jmethodID methodID,
        //                            va_list args) {
        //     functions.CallStaticVoidMethodV(_env,cls,methodID,args);
        // }
        public void CallStaticVoidMethodA(jclass cls, jmethodID methodID,
                                   jvalue * args) {
            _functions.CallStaticVoidMethodA(_env,cls,methodID,args);
        }

        public jfieldID GetStaticFieldID(jclass clazz, string name,
                                  string sig) {
            return _functions.GetStaticFieldID(_env,clazz,name,sig);
        }
        public jobject GetStaticObjectField(jclass clazz, jfieldID fieldID) {
            return _functions.GetStaticObjectField(_env,clazz,fieldID);
        }
        public jboolean GetStaticBooleanField(jclass clazz, jfieldID fieldID) {
            return _functions.GetStaticBooleanField(_env,clazz,fieldID);
        }
        public jbyte GetStaticByteField(jclass clazz, jfieldID fieldID) {
            return _functions.GetStaticByteField(_env,clazz,fieldID);
        }
        public jchar GetStaticCharField(jclass clazz, jfieldID fieldID) {
            return _functions.GetStaticCharField(_env,clazz,fieldID);
        }
        public jshort GetStaticShortField(jclass clazz, jfieldID fieldID) {
            return _functions.GetStaticShortField(_env,clazz,fieldID);
        }
        public jint GetStaticIntField(jclass clazz, jfieldID fieldID) {
            return _functions.GetStaticIntField(_env,clazz,fieldID);
        }
        public jlong GetStaticLongField(jclass clazz, jfieldID fieldID) {
            return _functions.GetStaticLongField(_env,clazz,fieldID);
        }
        public jfloat GetStaticFloatField(jclass clazz, jfieldID fieldID) {
            return _functions.GetStaticFloatField(_env,clazz,fieldID);
        }
        public jdouble GetStaticDoubleField(jclass clazz, jfieldID fieldID) {
            return _functions.GetStaticDoubleField(_env,clazz,fieldID);
        }

        public void SetStaticObjectField(jclass clazz, jfieldID fieldID,
                            jobject value) {
          _functions.SetStaticObjectField(_env,clazz,fieldID,value);
        }
        public void SetStaticBooleanField(jclass clazz, jfieldID fieldID,
                            jboolean value) {
          _functions.SetStaticBooleanField(_env,clazz,fieldID,value);
        }
        public void SetStaticByteField(jclass clazz, jfieldID fieldID,
                            jbyte value) {
          _functions.SetStaticByteField(_env,clazz,fieldID,value);
        }
        public void SetStaticCharField(jclass clazz, jfieldID fieldID,
                            jchar value) {
          _functions.SetStaticCharField(_env,clazz,fieldID,value);
        }
        public void SetStaticShortField(jclass clazz, jfieldID fieldID,
                            jshort value) {
          _functions.SetStaticShortField(_env,clazz,fieldID,value);
        }
        public void SetStaticIntField(jclass clazz, jfieldID fieldID,
                            jint value) {
          _functions.SetStaticIntField(_env,clazz,fieldID,value);
        }
        public void SetStaticLongField(jclass clazz, jfieldID fieldID,
                            jlong value) {
          _functions.SetStaticLongField(_env,clazz,fieldID,value);
        }
        public void SetStaticFloatField(jclass clazz, jfieldID fieldID,
                            jfloat value) {
          _functions.SetStaticFloatField(_env,clazz,fieldID,value);
        }
        public void SetStaticDoubleField(jclass clazz, jfieldID fieldID,
                            jdouble value) {
          _functions.SetStaticDoubleField(_env,clazz,fieldID,value);
        }

        public jstring NewString(jchar *unicode, jsize len) {
            return _functions.NewString(_env,unicode,len);
        }
        public jsize GetStringLength(jstring str) {
            return _functions.GetStringLength(_env,str);
        }
        public jchar *GetStringChars(jstring str, jboolean *isCopy) {
            return _functions.GetStringChars(_env,str,isCopy);
        }
        public void ReleaseStringChars(jstring str, jchar *chars) {
            _functions.ReleaseStringChars(_env,str,chars);
        }

        public jstring NewStringUTF(byte* utf) {
            return _functions.NewStringUTF(_env, utf);
        }
        public jsize GetStringUTFLength(jstring str) {
            return _functions.GetStringUTFLength(_env,str);
        }
        public byte* GetStringUTFChars(jstring str, jboolean *isCopy) {
            return _functions.GetStringUTFChars(_env,str,isCopy);
        }
        public void ReleaseStringUTFChars(jstring str, byte* chars) {
            _functions.ReleaseStringUTFChars(_env,str,chars);
        }

        public jsize GetArrayLength(jarray array) {
            return _functions.GetArrayLength(_env,array);
        }

        public jobjectArray NewObjectArray(jsize len, jclass clazz,
                                    jobject init) {
            return _functions.NewObjectArray(_env,len,clazz,init);
        }
        public jobject GetObjectArrayElement(jobjectArray array, jsize index) {
            return _functions.GetObjectArrayElement(_env,array,index);
        }
        public void SetObjectArrayElement(jobjectArray array, jsize index,
                                   jobject val) {
            _functions.SetObjectArrayElement(_env,array,index,val);
        }

        public jbooleanArray NewBooleanArray(jsize len) {
            return _functions.NewBooleanArray(_env,len);
        }
        public jbyteArray NewByteArray(jsize len) {
            return _functions.NewByteArray(_env,len);
        }
        public jcharArray NewCharArray(jsize len) {
            return _functions.NewCharArray(_env,len);
        }
        public jshortArray NewShortArray(jsize len) {
            return _functions.NewShortArray(_env,len);
        }
        public jintArray NewIntArray(jsize len) {
            return _functions.NewIntArray(_env,len);
        }
        public jlongArray NewLongArray(jsize len) {
            return _functions.NewLongArray(_env,len);
        }
        public jfloatArray NewFloatArray(jsize len) {
            return _functions.NewFloatArray(_env,len);
        }
        public jdoubleArray NewDoubleArray(jsize len) {
            return _functions.NewDoubleArray(_env,len);
        }

        public jboolean * GetBooleanArrayElements(jbooleanArray array, jboolean *isCopy) {
            return _functions.GetBooleanArrayElements(_env,array,isCopy);
        }
        public jbyte * GetByteArrayElements(jbyteArray array, jboolean *isCopy) {
            return _functions.GetByteArrayElements(_env,array,isCopy);
        }
        public jchar * GetCharArrayElements(jcharArray array, jboolean *isCopy) {
            return _functions.GetCharArrayElements(_env,array,isCopy);
        }
        public jshort * GetShortArrayElements(jshortArray array, jboolean *isCopy) {
            return _functions.GetShortArrayElements(_env,array,isCopy);
        }
        public jint * GetIntArrayElements(jintArray array, jboolean *isCopy) {
            return _functions.GetIntArrayElements(_env,array,isCopy);
        }
        public jlong * GetLongArrayElements(jlongArray array, jboolean *isCopy) {
            return _functions.GetLongArrayElements(_env,array,isCopy);
        }
        public jfloat * GetFloatArrayElements(jfloatArray array, jboolean *isCopy) {
            return _functions.GetFloatArrayElements(_env,array,isCopy);
        }
        public jdouble * GetDoubleArrayElements(jdoubleArray array, jboolean *isCopy) {
            return _functions.GetDoubleArrayElements(_env,array,isCopy);
        }

        public void ReleaseBooleanArrayElements(jbooleanArray array,
                                         jboolean *elems,
                                         ScalarArrayReleaseMode mode) {
            _functions.ReleaseBooleanArrayElements(_env,array,elems,mode);
        }
        public void ReleaseByteArrayElements(jbyteArray array,
                                      jbyte *elems,
                                      ScalarArrayReleaseMode mode) {
            _functions.ReleaseByteArrayElements(_env,array,elems,mode);
        }
        public void ReleaseCharArrayElements(jcharArray array,
                                      jchar *elems,
                                      ScalarArrayReleaseMode mode) {
            _functions.ReleaseCharArrayElements(_env,array,elems,mode);
        }
        public void ReleaseShortArrayElements(jshortArray array,
                                       jshort *elems,
                                       ScalarArrayReleaseMode mode) {
            _functions.ReleaseShortArrayElements(_env,array,elems,mode);
        }
        public void ReleaseIntArrayElements(jintArray array,
                                     jint *elems,
                                     ScalarArrayReleaseMode mode) {
            _functions.ReleaseIntArrayElements(_env,array,elems,mode);
        }
        public void ReleaseLongArrayElements(jlongArray array,
                                      jlong *elems,
                                      ScalarArrayReleaseMode mode) {
            _functions.ReleaseLongArrayElements(_env,array,elems,mode);
        }
        public void ReleaseFloatArrayElements(jfloatArray array,
                                       jfloat *elems,
                                       ScalarArrayReleaseMode mode) {
            _functions.ReleaseFloatArrayElements(_env,array,elems,mode);
        }
        public void ReleaseDoubleArrayElements(jdoubleArray array,
                                        jdouble *elems,
                                        ScalarArrayReleaseMode mode) {
            _functions.ReleaseDoubleArrayElements(_env,array,elems,mode);
        }

        public void GetBooleanArrayRegion(jbooleanArray array,
                                   jsize start, jsize len, jboolean *buf) {
            _functions.GetBooleanArrayRegion(_env,array,start,len,buf);
        }
        public void GetByteArrayRegion(jbyteArray array,
                                jsize start, jsize len, jbyte *buf) {
            _functions.GetByteArrayRegion(_env,array,start,len,buf);
        }
        public void GetCharArrayRegion(jcharArray array,
                                jsize start, jsize len, jchar *buf) {
            _functions.GetCharArrayRegion(_env,array,start,len,buf);
        }
        public void GetShortArrayRegion(jshortArray array,
                                 jsize start, jsize len, jshort *buf) {
            _functions.GetShortArrayRegion(_env,array,start,len,buf);
        }
        public void GetIntArrayRegion(jintArray array,
                               jsize start, jsize len, jint *buf) {
            _functions.GetIntArrayRegion(_env,array,start,len,buf);
        }
        public void GetLongArrayRegion(jlongArray array,
                                jsize start, jsize len, jlong *buf) {
            _functions.GetLongArrayRegion(_env,array,start,len,buf);
        }
        public void GetFloatArrayRegion(jfloatArray array,
                                 jsize start, jsize len, jfloat *buf) {
            _functions.GetFloatArrayRegion(_env,array,start,len,buf);
        }
        public void GetDoubleArrayRegion(jdoubleArray array,
                                  jsize start, jsize len, jdouble *buf) {
            _functions.GetDoubleArrayRegion(_env,array,start,len,buf);
        }

        public void SetBooleanArrayRegion(jbooleanArray array, jsize start, jsize len,
                                   jboolean *buf) {
            _functions.SetBooleanArrayRegion(_env,array,start,len,buf);
        }
        public void SetByteArrayRegion(jbyteArray array, jsize start, jsize len,
                                jbyte *buf) {
            _functions.SetByteArrayRegion(_env,array,start,len,buf);
        }
        public void SetCharArrayRegion(jcharArray array, jsize start, jsize len,
                                jchar *buf) {
            _functions.SetCharArrayRegion(_env,array,start,len,buf);
        }
        public void SetShortArrayRegion(jshortArray array, jsize start, jsize len,
                                 jshort *buf) {
            _functions.SetShortArrayRegion(_env,array,start,len,buf);
        }
        public void SetIntArrayRegion(jintArray array, jsize start, jsize len,
                               jint *buf) {
            _functions.SetIntArrayRegion(_env,array,start,len,buf);
        }
        public void SetLongArrayRegion(jlongArray array, jsize start, jsize len,
                                jlong *buf) {
            _functions.SetLongArrayRegion(_env,array,start,len,buf);
        }
        public void SetFloatArrayRegion(jfloatArray array, jsize start, jsize len,
                                 jfloat *buf) {
            _functions.SetFloatArrayRegion(_env,array,start,len,buf);
        }
        public void SetDoubleArrayRegion(jdoubleArray array, jsize start, jsize len,
                                  jdouble *buf) {
            _functions.SetDoubleArrayRegion(_env,array,start,len,buf);
        }

        public jint RegisterNatives(jclass clazz, JNINativeMethod *methods,
                             jint nMethods) {
            return _functions.RegisterNatives(_env,clazz,methods,nMethods);
        }
        public jint UnregisterNatives(jclass clazz) {
            return _functions.UnregisterNatives(_env,clazz);
        }

        public jint MonitorEnter(jobject obj) {
            return _functions.MonitorEnter(_env,obj);
        }
        public jint MonitorExit(jobject obj) {
            return _functions.MonitorExit(_env,obj);
        }

        public JniResult GetJavaVM(JavaVM_ **vm) {
            return _functions.GetJavaVM(_env,vm);
        }

        public void GetStringRegion(jstring str, jsize start, jsize len, jchar *buf) {
            _functions.GetStringRegion(_env,str,start,len,buf);
        }
        public void GetStringUTFRegion(jstring str, jsize start, jsize len, byte *buf) {
            _functions.GetStringUTFRegion(_env,str,start,len,buf);
        }

        public void * GetPrimitiveArrayCritical(jarray array, jboolean *isCopy) {
            return _functions.GetPrimitiveArrayCritical(_env,array,isCopy);
        }
        public void ReleasePrimitiveArrayCritical(jarray array, void *carray, ScalarArrayReleaseMode mode) {
            _functions.ReleasePrimitiveArrayCritical(_env,array,carray,mode);
        }

        public jchar * GetStringCritical(jstring _string, jboolean *isCopy) {
            return _functions.GetStringCritical(_env,_string,isCopy);
        }
        public void ReleaseStringCritical(jstring _string, jchar *cstring) {
            _functions.ReleaseStringCritical(_env,_string,cstring);
        }

        public jweak NewWeakGlobalRef(jobject obj) {
            return _functions.NewWeakGlobalRef(_env,obj);
        }
        public void DeleteWeakGlobalRef(jweak _ref) {
            _functions.DeleteWeakGlobalRef(_env,_ref);
        }

        public jboolean ExceptionCheck() {
            return _functions.ExceptionCheck(_env);
        }

        public jobject NewDirectByteBuffer(void* address, jlong capacity) {
            return _functions.NewDirectByteBuffer(_env, address, capacity);
        }
        public void* GetDirectBufferAddress(jobject buf) {
            return _functions.GetDirectBufferAddress(_env, buf);
        }
        public jlong GetDirectBufferCapacity(jobject buf) {
            return _functions.GetDirectBufferCapacity(_env, buf);
        }
        public jobjectRefType GetObjectRefType(jobject obj) {
            return _functions.GetObjectRefType(_env, obj);
        }
    }
}