using System;
using System.Text;
using Jni4CSharp.Bindings;
using Jni4CSharp.JniUtils;

namespace Jni4CSharp.Decorated
{
    public class JavaClass
    {
        private readonly JNIEnv _env;
        public readonly jclass JniRef;
        public readonly string ClassName;
        private readonly JavaRuntime _runtime;

        public JavaClass(JNIEnv env, in jclass jniRef, string className, JavaRuntime runtime)
        {
            _env = env;
            JniRef = jniRef;
            ClassName = className;
            _runtime = runtime;
        }

        public JavaValue NewArray(int size)
        {
            jarray array = _env.NewObjectArray(size, JniRef, jobject.NULL);
            _env.EscalateException();

            return new JavaValue(_env, new jvalue(array), _runtime);
        }

        public JavaFunction GetConstructor(string signature)
        {
            var methodId = _env.GetMethodID(JniRef, "<init>", signature);
            if (methodId.IsNull())
            {
                throw new Exception($@"constructor not found: {ClassName}{signature}");
            }

            return new JavaFunction(_env, methodId, true, this, null, signature, "<init>", _runtime);
        }

        public JavaFunction GetStaticMethod(string name, string signature)
        {
            var methodId = _env.GetStaticMethodID(JniRef, name, signature);
            if (methodId.IsNull())
            {
                throw new Exception($@"method not found: {ClassName}.{name}{signature}");
            }

            return new JavaFunction(_env, methodId, false, this, null, signature, name, _runtime);
        }

        public JavaField GetStaticField(string name, string signature)
        {
            var fieldId = _env.GetStaticFieldID(JniRef, name, signature);
            if (fieldId.IsNull())
            {
                throw new Exception($@"field not found: {ClassName}.{name} : {signature}");
            }

            return new JavaField(_env, null, signature, fieldId, this, name, _runtime);
        }

        public RegisterNativesWizard BeginRegisterNatives()
        {
            return new RegisterNativesWizard(_env, this);
        }

        public unsafe void ThrowNewAnsi(string message)
        {
            var array = Encoding.ASCII.GetBytes(message);
            int result;
            fixed (byte* bytes = array)
                result = _env.ThrowNew(JniRef, bytes);
            if (result != 0)
            {
                Console.WriteLine($@"ERROR: failed to throw exception (error code {result}): {ClassName}: {message}");
            }
        }

        //TODO test it
        public unsafe void ThrowNewUtf8(string message)
        {
            try
            {
                var bytes = Encoding.UTF8.GetBytes(message);

                // todo extract to DSL work with primitive arrays
                var jbytes = _env.NewByteArray(bytes.Length);
                fixed (byte* pbytes = bytes)
                    _env.SetByteArrayRegion(jbytes, 0, bytes.Length, pbytes);

                var jcharset = _runtime.FindClass("java.nio.charset.StandardCharsets")
                    .GetStaticField("UTF_8", "Ljava/nio/charset/Charset;")
                    .GetValue();

                var jmessage = _runtime.FindClass("Ljava/lang/String;")
                    .GetConstructor("([BLjava/nio/charset/Charset;)V")
                    .Invoke(new jvalue(jbytes), new jvalue(jcharset.jValue.Ref));

                _runtime.FindClass("java.lang.Exception")
                    .GetConstructor("(Ljava/lang/String;)V")
                    .Invoke(new jvalue(jmessage.jValue.Ref))
                    .Throw();
            }
            catch (Exception e)
            {
                Console.WriteLine($@"ERROR: failed to throw exception: {ClassName} with message:\n    {message}\n    due to {e}");
                throw;
            }
        }

        public void UnregisterNatives()
        {
            _env.UnregisterNatives(JniRef);
        }

        public override string ToString()
        {
            return $"{nameof(JavaClass)}({ClassName})";
        }
    }
}