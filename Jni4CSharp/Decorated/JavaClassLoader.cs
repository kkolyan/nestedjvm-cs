using System;
using Jni4CSharp.Bindings;

namespace Jni4CSharp.Decorated
{
    public class JavaClassLoader
    {
        private readonly JavaFunction _loadClass;
        private readonly JavaRuntime _runtime;
        public readonly JavaObject ObjectInstance;

        public static JavaClassLoader Wrap(JavaRuntime runtime, JavaObject classLoaderInstance)
        {
            return new JavaClassLoader(
                classLoaderInstance.GetInstanceMethod("loadClass", "(Ljava/lang/String;)Ljava/lang/Class;"), runtime,
                classLoaderInstance);
        }

        public JavaClassLoader(JavaFunction loadClass, JavaRuntime runtime, JavaObject objectInstance)
        {
            _loadClass = loadClass;
            _runtime = runtime;
            ObjectInstance = objectInstance;
        }

        public void SetContextClassLoader()
        {
            var currentThread = _runtime.FindClass("java.lang.Thread")
                .GetStaticMethod("currentThread", "()Ljava/lang/Thread;")
                .Invoke()
                .FetchObject();
            currentThread.GetInstanceMethod("setContextClassLoader", "(Ljava/lang/ClassLoader;)V")
                .Invoke(new jvalue(ObjectInstance.JniRef));
        }

        public JavaClass LoadClass(string className)
        {
            _runtime.PushLocalFrame(16);
            try
            {
                if (className.Contains("/"))
                {
                    throw new Exception("dot-delimited class name expected here instead of JNI style with slashes");
                }

                var cls = _loadClass.Invoke(new jvalue(_runtime.AllocateStringAnsi(className))).jValue.Ref.AsClass();
                if (cls.IsNull())
                {
                    throw new Exception("class not found: " + className);
                }

                return _runtime.WrapClass(cls, className);
            }
            finally
            {
                _runtime.PopLocalFrame();
            }
        }

        public void ReleaseGlobalRefs()
        {
            _loadClass.ReleaseGlobalRefs();
        }
    }
}