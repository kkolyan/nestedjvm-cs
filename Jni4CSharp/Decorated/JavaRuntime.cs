using System;
using System.Collections.Generic;
using System.IO;
using Jni4CSharp.Bindings;
using Jni4CSharp.CommonUtils;
using Jni4CSharp.JniUtils;

namespace Jni4CSharp.Decorated
{
    public class JavaRuntime
    {
        private readonly JNIEnv _env;
        private readonly JavaVM _vm;

        public void PushLocalFrame(int capacity)
        {
            _env.PushLocalFrame(capacity);
        }

        public jstring AllocateStringAnsi(string s)
        {
            return _env.NewStringAnsi(s);
        }

        private JavaRuntime(JNIEnv env, JavaVM vm)
        {
            _env = env;
            _vm = vm;
        }

        public JavaValue WrapValue(jobject o)
        {
            return new JavaValue(_env, new jvalue(o), this);
        }

        public static unsafe JavaRuntime Wrap(JNIEnv env)
        {
            JavaVM_* javaVm;
            var result = env.GetJavaVM(&javaVm);
            if (result != JniResult.JNI_OK)
            {
                throw new Exception("GetJavaVM failed: " + result);
            }

            return new JavaRuntime(env, new JavaVM(javaVm));
        }

        public static JavaRuntime Create(DynamicLibrary jvmLibrary, IList<string> jvmOpts)
        {
            var session = JniSession.CreateJvm(jvmLibrary, jvmOpts);

            return new JavaRuntime(session.Env, session.Vm);
        }

        public static JavaRuntime DetectExisting(DynamicLibrary jvmLibrary)
        {
            var session = JniSession.DetectExistingJvm(jvmLibrary);
            if (session == null)
            {
                return null;
            }

            return new JavaRuntime(session.Env, session.Vm);
        }

        public JavaClass FindClass(string className)
        {
            if (className.Contains("/"))
            {
                throw new Exception("dot-delimited class name expected here instead of JNI style with slashes");
            }

            var cls = _env.FindClass(className.Replace(".", "/"));
            if (cls.IsNull())
            {
                throw new Exception("class not found: " + className);
            }

            return new JavaClass(_env, cls, className, this);
        }

        public void DetachCurrentThread()
        {
            _vm.DetachCurrentThread();
        }

        public void PopLocalFrame()
        {
            _env.PopLocalFrame(jobject.NULL);
        }

        public JavaClassLoader AddClassPath(params string[] classPath)
        {
            PushLocalFrame(16);
            try
            {
                var classPathLength = classPath.Length;
                var urls = FindClass("java.net.URL")
                    .NewArray(classPathLength)
                    .FetchObject();
                for (var index = 0; index < classPath.Length; index++)
                {
                    var classPathEntry = Path.GetFullPath(classPath[index]);
                    Console.WriteLine("adding classpath entry: " + classPathEntry);
                    var url = FindClass("java.io.File")
                        .GetConstructor("(Ljava/lang/String;)V")
                        .Invoke(new jvalue(AllocateStringAnsi(classPathEntry)))
                        .FetchObject()
                        .GetInstanceMethod("toURI", "()Ljava/net/URI;")
                        .Invoke()
                        .FetchObject()
                        .GetInstanceMethod("toURL", "()Ljava/net/URL;")
                        .Invoke();
                    urls.SetArrayElement(index, url.jValue.Ref);
                }

                var currentThread = FindClass("java.lang.Thread")
                    .GetStaticMethod("currentThread", "()Ljava/lang/Thread;")
                    .Invoke()
                    .FetchObject();
                var contextClassLoader = currentThread
                    .GetInstanceMethod("getContextClassLoader", "()Ljava/lang/ClassLoader;")
                    .Invoke();
                var customClassLoader = FindClass("java.net.URLClassLoader")
                    .GetConstructor("([Ljava/net/URL;Ljava/lang/ClassLoader;)V")
                    .Invoke(new jvalue(urls.JniRef), new jvalue(contextClassLoader.jValue.Ref))
                    .GoGlobal()
                    .FetchObject();

                var javaClassLoader = JavaClassLoader.Wrap(this, customClassLoader);
                javaClassLoader.SetContextClassLoader();
                return javaClassLoader;
            }
            finally
            {
                PopLocalFrame();
            }
        }

        public JavaClass WrapClass(in jclass cls, string className)
        {
            return new JavaClass(_env, cls, className, this);
        }
    }
}