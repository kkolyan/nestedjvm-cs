using System;
using Jni4CSharp.Bindings;
using Jni4CSharp.JniUtils;

namespace Jni4CSharp.Decorated
{
    public class JavaObject
    {
        public readonly jobject JniRef;
        private readonly JNIEnv _env;
        private readonly JavaClass _class;
        private readonly JavaRuntime _runtime;

        public JavaObject(JNIEnv env, jobject jniRef, JavaClass @class, JavaRuntime runtime)
        {
            JniRef = jniRef;
            _env = env;
            _class = @class;
            _runtime = runtime;
        }

        public JavaFunction GetInstanceMethod(string name, string signature)
        {
            var methodId = _env.GetMethodID(_class.JniRef, name, signature);
            if (methodId.IsNull())
            {
                throw new Exception($@"method not found: {_class.ClassName}.{name}{signature}");
            }
            return new JavaFunction(_env, methodId, false, _class, this, signature, name, _runtime);
        }

        public JavaField GetInstanceField(string name, string signature)
        {
            var fieldId = _env.GetFieldID(_class.JniRef, name, signature);
            if (fieldId.IsNull())
            {
                throw new Exception($@"field not found: {_class.ClassName}.{name} : {signature}");
            }
            return new JavaField(_env, this, signature, fieldId, _class, name, _runtime);
        }

        public void SetArrayElement(int index, jobject value)
        {
            _env.SetObjectArrayElement(JniRef.AsObjectArray(), index, value);
            _env.EscalateException();
        }

        public override string ToString()
        {
            return $"{nameof(JavaObject)}({JniRef}: {_class.ClassName})";
        }

        public void DeleteGlobalRef()
        {
            _env.DeleteGlobalRef(JniRef);
        }
    }
}