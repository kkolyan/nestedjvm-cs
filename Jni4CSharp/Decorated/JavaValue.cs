using System;
using Jni4CSharp.Bindings;
using Jni4CSharp.JniUtils;

namespace Jni4CSharp.Decorated
{
    public class JavaValue
    {
        private readonly JNIEnv _env;
        private readonly JavaRuntime _runtime;
        public readonly jvalue jValue;

        public JavaValue(JNIEnv env, jvalue jValue, JavaRuntime runtime)
        {
            _env = env;
            this.jValue = jValue;
            _runtime = runtime;
        }

        public bool IsNull()
        {
            return jValue.Ref.IsNull();
        }

        public string FetchString()
        {
            _notNull();
            return _env.GetText(jValue.Ref.AsString());
        }

        public unsafe IntPtr FetchDirectBufferAddress()
        {
            _notNull();
            return new IntPtr(_env.GetDirectBufferAddress(jValue.Ref));
        }

        public JavaValue GoGlobal()
        {
            var globalRef = _env.NewGlobalRef(jValue.Ref);
            return new JavaValue(_env, new jvalue(globalRef), _runtime);
        }

        public unsafe JavaObject FetchObject()
        {
            _notNull();
            
            var cls_Object = _env.FindClass("java/lang/Object");
            var getClass = _env.GetMethodID(cls_Object, "getClass", "()Ljava/lang/Class;");
            var cls = _env.CallObjectMethodA(jValue.Ref, getClass, null).AsClass();
            _env.EscalateException();
            
            var cls_Class = _env.FindClass("java/lang/Class");
            _env.EscalateException();
            var getName = _env.GetMethodID(cls_Class, "getName", "()Ljava/lang/String;");

            var clsName = _env.CallObjectMethodA(cls, getName, null).AsString();
            _env.EscalateException();

            return new JavaObject(_env, jValue.Ref, new JavaClass(_env, cls, _env.GetText(clsName), _runtime), _runtime);
        }

        private void _notNull()
        {
            if (IsNull())
            {
                throw new Exception("Attempt to fetch object by NULL reference");
            }
        }

        public void Throw()
        {
            _notNull();
            _env.Throw(jValue.Ref.AsThrowable());
        }

        public override string ToString()
        {
            return $"{nameof(JavaValue)}({jValue})";
        }
    }
}