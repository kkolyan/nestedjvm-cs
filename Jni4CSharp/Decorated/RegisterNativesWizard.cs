using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Jni4CSharp.Bindings;

namespace Jni4CSharp.Decorated
{
    public class RegisterNativesWizard
    {
        private readonly JNIEnv _env;
        private readonly JavaClass _class;
        private readonly IList<JNINativeMethod> _methods = new List<JNINativeMethod>();

        public RegisterNativesWizard(JNIEnv env, JavaClass @class)
        {
            _env = env;
            _class = @class;
        }

        public unsafe RegisterNativesWizard AddDelegate<T>(string signature, T func) where T : Delegate
        {
            _methods.Add(new JNINativeMethod
            {
                name = (byte*) Marshal.StringToHGlobalAnsi(typeof(T).Name).ToPointer(),
                signature = (byte*) Marshal.StringToHGlobalAnsi(signature).ToPointer(),
                fnPtr = Marshal.GetFunctionPointerForDelegate(func).ToPointer()
            });
            return this;
        }

        public unsafe void Complete()
        {
            var arrayOfMethods = _methods.ToArray();
            fixed (JNINativeMethod* methods = arrayOfMethods)
                _env.RegisterNatives(_class.JniRef, methods, arrayOfMethods.Length);
        }
    }
}