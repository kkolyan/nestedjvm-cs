using System.Collections.Generic;
using Jni4CSharp.Bindings;
using Jni4CSharp.CommonUtils;

namespace Jni4CSharp.Decorated
{
    public class JavaField
    {
        private readonly JNIEnv _env;
        private readonly JavaObject _instance;
        private readonly string _signature;
        public readonly jfieldID JniId;
        private readonly JavaClass _class;
        public readonly string FieldName;
        private readonly JavaRuntime _runtime;

        public JavaField(JNIEnv env, JavaObject instance, string signature, jfieldID jniId, JavaClass @class, string fieldName, JavaRuntime runtime)
        {
            _env = env;
            _instance = instance;
            _signature = signature;
            JniId = jniId;
            _class = @class;
            FieldName = fieldName;
            _runtime = runtime;
        }

        public JavaValue GetValue()
        {
            var value = InternalGetValue();
            return new JavaValue(_env, value, _runtime);
        }

        private jvalue InternalGetValue()
        {
            if (_instance == null)
            {
                return StaticCalls.GetRequired(_signature[0])
                    .Invoke(_env, _class.JniRef, JniId);
            }
            return InstanceCalls.GetRequired(_signature[0])
                .Invoke(_env, _instance.JniRef, JniId);
        }

        public override string ToString()
        {
            return $"{nameof(JavaField)}({FieldName}: {_signature})";
        }


        private delegate jvalue StaticCall(JNIEnv env, jclass cls, jfieldID jfieldId);
        private delegate jvalue InstanceCall(JNIEnv env, jobject instance, jfieldID jfieldId);
        private static readonly IDictionary<char, StaticCall> StaticCalls = new Dictionary<char, StaticCall>();
        private static readonly IDictionary<char, InstanceCall> InstanceCalls = new Dictionary<char, InstanceCall>();

        static JavaField()
        {
            StaticCalls['Z'] = (env, cls, jfieldId) => new jvalue(env.GetStaticBooleanField(cls, jfieldId));
            StaticCalls['B'] = (env, cls, jfieldId) => new jvalue(env.GetStaticByteField(cls, jfieldId));
            StaticCalls['C'] = (env, cls, jfieldId) => new jvalue(env.GetStaticCharField(cls, jfieldId));
            StaticCalls['S'] = (env, cls, jfieldId) => new jvalue(env.GetStaticShortField(cls, jfieldId));
            StaticCalls['I'] = (env, cls, jfieldId) => new jvalue(env.GetStaticIntField(cls, jfieldId));
            StaticCalls['J'] = (env, cls, jfieldId) => new jvalue(env.GetStaticLongField(cls, jfieldId));
            StaticCalls['F'] = (env, cls, jfieldId) => new jvalue(env.GetStaticFloatField(cls, jfieldId));
            StaticCalls['D'] = (env, cls, jfieldId) => new jvalue(env.GetStaticDoubleField(cls, jfieldId));
            StaticCalls['L'] = (env, cls, jfieldId) => new jvalue(env.GetStaticObjectField(cls, jfieldId));
            StaticCalls['['] = (env, cls, jfieldId) => new jvalue(env.GetStaticObjectField(cls, jfieldId));
            InstanceCalls['Z'] = (env, instance, jfieldId) => new jvalue(env.GetBooleanField(instance, jfieldId));
            InstanceCalls['B'] = (env, instance, jfieldId) => new jvalue(env.GetByteField(instance, jfieldId));
            InstanceCalls['C'] = (env, instance, jfieldId) => new jvalue(env.GetCharField(instance, jfieldId));
            InstanceCalls['S'] = (env, instance, jfieldId) => new jvalue(env.GetShortField(instance, jfieldId));
            InstanceCalls['I'] = (env, instance, jfieldId) => new jvalue(env.GetIntField(instance, jfieldId));
            InstanceCalls['J'] = (env, instance, jfieldId) => new jvalue(env.GetLongField(instance, jfieldId));
            InstanceCalls['F'] = (env, instance, jfieldId) => new jvalue(env.GetFloatField(instance, jfieldId));
            InstanceCalls['D'] = (env, instance, jfieldId) => new jvalue(env.GetDoubleField(instance, jfieldId));
            InstanceCalls['L'] = (env, instance, jfieldId) => new jvalue(env.GetObjectField(instance, jfieldId));
            InstanceCalls['['] = (env, instance, jfieldId) => new jvalue(env.GetObjectField(instance, jfieldId));
        }
    }
}