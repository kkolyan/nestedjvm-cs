using System;
using System.Collections.Generic;
using Jni4CSharp.Bindings;
using Jni4CSharp.JniUtils;

namespace Jni4CSharp.Decorated
{
    public class JavaFunction
    {
        private JNIEnv _env;
        public jmethodID JniId;
        private bool _constructor;
        private JavaClass _class;
        private JavaObject _object;
        private readonly string _signature;
        private readonly char _returnType;
        public readonly string MethodName;

        public JavaFunction(JNIEnv env, jmethodID jniId, bool constructor, JavaClass @class, JavaObject instance,
            string signature, string methodName, JavaRuntime runtime)
        {
            _env = env;
            JniId = jniId;
            _constructor = constructor;
            _class = @class;
            _object = instance;
            _signature = signature;
            MethodName = methodName;
            _runtime = runtime;

            var bracket = _signature.LastIndexOf(')');
            if (bracket < 0)
            {
                throw new Exception("invalid signature: " + _signature);
            }

            _returnType = _signature[bracket + 1];
        }


        private unsafe delegate jvalue StaticCall(JNIEnv env, jclass cls, jmethodID jmethodId, jvalue* args);

        private unsafe delegate jvalue InstanceCall(JNIEnv env, jobject instance, jmethodID jmethodId,
            jvalue* args);

        private static readonly IDictionary<char, StaticCall> StaticCalls = new Dictionary<char, StaticCall>();
        private static readonly IDictionary<char, InstanceCall> InstanceCalls = new Dictionary<char, InstanceCall>();
        private readonly JavaRuntime _runtime;

        static unsafe JavaFunction()
        {
            StaticCalls['Z'] = (env, cls, jmethodId, args) =>
                new jvalue(env.CallStaticBooleanMethodA(cls, jmethodId, args));
            StaticCalls['B'] = (env, cls, jmethodId, args) =>
                new jvalue(env.CallStaticByteMethodA(cls, jmethodId, args));
            StaticCalls['C'] = (env, cls, jmethodId, args) =>
                new jvalue(env.CallStaticCharMethodA(cls, jmethodId, args));
            StaticCalls['S'] = (env, cls, jmethodId, args) =>
                new jvalue(env.CallStaticShortMethodA(cls, jmethodId, args));
            StaticCalls['I'] = (env, cls, jmethodId, args) =>
                new jvalue(env.CallStaticIntMethodA(cls, jmethodId, args));
            StaticCalls['J'] = (env, cls, jmethodId, args) =>
                new jvalue(env.CallStaticLongMethodA(cls, jmethodId, args));
            StaticCalls['F'] = (env, cls, jmethodId, args) =>
                new jvalue(env.CallStaticFloatMethodA(cls, jmethodId, args));
            StaticCalls['D'] = (env, cls, jmethodId, args) =>
                new jvalue(env.CallStaticDoubleMethodA(cls, jmethodId, args));
            StaticCalls['L'] = (env, cls, jmethodId, args) =>
                new jvalue(env.CallStaticObjectMethodA(cls, jmethodId, args));
            StaticCalls['['] = (env, cls, jmethodId, args) =>
                new jvalue(env.CallStaticObjectMethodA(cls, jmethodId, args));
            StaticCalls['V'] = (env, cls, jmethodId, args) =>
            {
                env.CallStaticVoidMethodA(cls, jmethodId, args);
                return new jvalue();
            };
            InstanceCalls['Z'] = (env, instance, jmethodId, args) =>
                new jvalue(env.CallBooleanMethodA(instance, jmethodId, args));
            InstanceCalls['B'] = (env, instance, jmethodId, args) =>
                new jvalue(env.CallByteMethodA(instance, jmethodId, args));
            InstanceCalls['C'] = (env, instance, jmethodId, args) =>
                new jvalue(env.CallCharMethodA(instance, jmethodId, args));
            InstanceCalls['S'] = (env, instance, jmethodId, args) =>
                new jvalue(env.CallShortMethodA(instance, jmethodId, args));
            InstanceCalls['I'] = (env, instance, jmethodId, args) =>
                new jvalue(env.CallIntMethodA(instance, jmethodId, args));
            InstanceCalls['J'] = (env, instance, jmethodId, args) =>
                new jvalue(env.CallLongMethodA(instance, jmethodId, args));
            InstanceCalls['F'] = (env, instance, jmethodId, args) =>
                new jvalue(env.CallFloatMethodA(instance, jmethodId, args));
            InstanceCalls['D'] = (env, instance, jmethodId, args) =>
                new jvalue(env.CallDoubleMethodA(instance, jmethodId, args));
            InstanceCalls['L'] = (env, instance, jmethodId, args) =>
                new jvalue(env.CallObjectMethodA(instance, jmethodId, args));
            InstanceCalls['['] = (env, instance, jmethodId, args) =>
                new jvalue(env.CallObjectMethodA(instance, jmethodId, args));
            InstanceCalls['V'] = (env, instance, jmethodId, args) =>
            {
                env.CallVoidMethodA(instance, jmethodId, args);
                return new jvalue();
            };
        }

        public JavaValue Invoke(params jvalue[] args)
        {
            jvalue result = InvokeInternal(args);
            _env.EscalateException();
            return new JavaValue(_env, result, _runtime);
        }

        private unsafe jvalue InvokeInternal(jvalue[] args)
        {
            fixed (jvalue* pargs = args)
            {
                if (_constructor)
                {
                    return new jvalue(_env.NewObjectA(_class.JniRef, JniId, pargs));
                }

                if (_object != null)
                {
                    return InstanceCalls[_returnType].Invoke(_env, _object.JniRef, JniId, pargs);
                }

                return StaticCalls[_returnType].Invoke(_env, _class.JniRef, JniId, pargs);
            }
        }

        public override string ToString()
        {
            if (_constructor)
            {
                return $"{nameof(JavaFunction)}({_class.ClassName}{_signature})";
            }

            return $"{nameof(JavaFunction)}({_class.ClassName}.{MethodName}{_signature})";
        }

        public void ReleaseGlobalRefs()
        {
            _object?.DeleteGlobalRef();
        }
    }
}