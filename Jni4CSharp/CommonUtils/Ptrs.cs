using System;
using System.Runtime.InteropServices;

namespace Jni4CSharp.CommonUtils
{
    public static class Ptrs
    {
        public static unsafe T Proc<T>(void* ptr) where T : Delegate
        {
            try
            {
                return Marshal.GetDelegateForFunctionPointer<T>(new IntPtr(ptr));
            }
            catch (Exception e)
            {
                throw new Exception($@"failed to create delegate of type {typeof(T)} for 0x{(int) ptr:X}", e);
            }
        }
    }
}