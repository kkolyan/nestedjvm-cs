using System;
using System.Runtime.InteropServices;

namespace Jni4CSharp.CommonUtils
{
    public class DynamicLibrary
    {
        private readonly IntPtr _libraryAddress;

        public DynamicLibrary(IntPtr libraryAddress)
        {
            _libraryAddress = libraryAddress;
        }

        public static DynamicLibrary Load(string path)
        {
            var address = LoadLibrary(path);
            if (address == IntPtr.Zero)
            {
                throw new Exception("failed to load library: " + path);
            }

            return new DynamicLibrary(address);
        }

        public void Free()
        {
            FreeLibrary(_libraryAddress);
        }

        public T GetProc<T>() where T : Delegate
        {
            var procName = typeof(T).Name;
            var functionAddress = GetProcAddress(_libraryAddress, procName);
            if (functionAddress == IntPtr.Zero)
            {
                throw new Exception("procedure not found: " + procName);
            }

            return Marshal.GetDelegateForFunctionPointer<T>(functionAddress);
        }


        [DllImport("kernel32", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool FreeLibrary(IntPtr hModule);

        [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern IntPtr LoadLibrary(string lpFileName);

        [DllImport("kernel32")]
        private static extern IntPtr GetProcAddress(IntPtr hModule, string procedureName);
    }
}