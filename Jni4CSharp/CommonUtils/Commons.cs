using System;
using System.Collections.Generic;

namespace Jni4CSharp.CommonUtils
{
    public static class Commons
    {
        public static V GetRequired<K, V>(this IDictionary<K, V> dict, K key)
        {
            if (dict.TryGetValue(key, out var value))
            {
                return value;
            }

            throw new Exception("key not found: " + key);
        }
    }
}