using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.InteropServices;
using NestedJvm;
using UnityEngine;

namespace NestedJvm_Unity.MonitoringWindows
{
    public class InputWindowConfigurator : MonoBehaviour, IExtensionConfigurator
    {
        private static readonly IEnumerable<KeyCode> AllKeyCodes = Enum.GetValues(typeof(KeyCode)).Cast<KeyCode>();

        private readonly IDictionary<KeyCode, MonitoringWindow> _keyCodeWindows =
            new Dictionary<KeyCode, MonitoringWindow>();

        public void Configure(IForeignExtension extension)
        {
            foreach (var keyCode in AllKeyCodes)
            {
                _keyCodeWindows[keyCode] = extension.AllocateMonitoringWindow("Input." + keyCode, 4);
            }
        }

        [SuppressMessage("ReSharper", "RedundantAssignment")]
        private void Update()
        {
            foreach (var keyCode in AllKeyCodes)
            {
                var buffer = _keyCodeWindows[keyCode].OutgoingBufferPtr;
                unsafe
                {
                    var ptr = (byte*) buffer.ToPointer();
                    *ptr++ = (byte) (Input.GetKey(keyCode) ? 1 : 0);
                    *ptr++ = (byte) (Input.GetKeyDown(keyCode) ? 1 : 0);
                    *ptr++ = (byte) (Input.GetKeyDown(keyCode) ? 1 : 0);
                }
            }
        }
    }
}