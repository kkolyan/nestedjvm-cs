using NestedJvm;
using UnityEngine;

namespace NestedJvm_Unity.CommandQueues
{
    public class GeneralEngineCommandQueueConfigurator : MonoBehaviour, IExtensionConfigurator
    {
        public void Configure(IForeignExtension extension)
        {
            extension.AddCommandQueueHandler("General", queue =>
            {
                // read different commands from queue buffer
            });
        }
    }
}