using NestedJvm;
using UnityEngine;

namespace NestedJvm_Unity.CommandQueues
{
    public class DebugLogCommandQueueConfigurator : MonoBehaviour, IExtensionConfigurator
    {
        public void Configure(IForeignExtension extension)
        {
            extension.AddCommandQueueHandler("Log", queue => { Debug.Log(queue.QueueName); });
            extension.AddCommandQueueHandler("LogWarning", queue => { Debug.LogWarning(queue.QueueName); });
            extension.AddCommandQueueHandler("LogError", queue => { Debug.LogError(queue.QueueName); });
        }
    }
}