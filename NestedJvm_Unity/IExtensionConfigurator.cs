using NestedJvm;

namespace NestedJvm_Unity
{
    public interface IExtensionConfigurator
    {
        void Configure(IForeignExtension extension);
    }
}