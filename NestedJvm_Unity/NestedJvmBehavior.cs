﻿using System.IO;
using Jni4CSharp.Decorated;
using Jni4CSharp.JniUtils;
using NestedJvm;
using UnityEngine;

namespace NestedJvm_Unity
{
    public class NestedJvmBehavior : MonoBehaviour, ISerializationCallbackReceiver
    {
        public string extensionClassName;
        private JavaPoweredExtension _extension;

        [SerializeField] private byte[] dumpArray;

        public void Start()
        {
            var jvmLibrary = JreLibrary.Load("jre");
            var runtime = JavaRuntime.DetectExisting(jvmLibrary);
            if (runtime == null)
            {
                runtime = JavaRuntime.Create(jvmLibrary, new string[] { });
            }

            _extension = JavaPoweredExtension.Load(runtime, extensionClassName, new[] {"classes", "nestedjvm-api.jar"});

            ConfigureExtension();
            _extension.Start();
        }

        public void OnAfterDeserialize()
        {
            var jvmLibrary = JreLibrary.Load("jre");
            var runtime = JavaRuntime.DetectExisting(jvmLibrary);
            _extension = JavaPoweredExtension.Restore(runtime, new BinaryReader(new MemoryStream(dumpArray)));
            ConfigureExtension();
        }

        private void ConfigureExtension()
        {
            foreach (var configurator in GetComponents<IExtensionConfigurator>())
            {
                configurator.Configure(_extension);
            }
        }

        public void OnBeforeSerialize()
        {
            using (var dumpAsStream = new MemoryStream())
            {
                using (var writer = new BinaryWriter(dumpAsStream))
                {
                    _extension.DumpAndDispose(writer);
                    writer.Flush();
                }

                dumpArray = dumpAsStream.ToArray();
            }
        }

        private void Update()
        {
            _extension?.Update();
        }

        private void LateUpdate()
        {
            _extension?.LateUpdate();
        }

        private void OnDestroy()
        {
            _extension?.Unload();
        }
    }
}