﻿using System;
using Jni4CSharp.Bindings;
using Jni4CSharp.CommonUtils;
using Jni4CSharp.JniUtils;

namespace Jni4CSharp_Showcase
{
    public class RawShowcase
    {
        private const string JreHome = "jre";
        private static DynamicLibrary _jvmLibrary = JreLibrary.Load(JreHome);

        public static void Main()
        {
            Console.WriteLine("RunShowCase");

            var session = JniSession.DetectExistingJvm(_jvmLibrary);
            Console.WriteLine("expected null: " + (session?.ToString() ?? "null"));

            session = JniSession.CreateJvm(
                _jvmLibrary,
                new[]
                {
                    "-Xmx256m",
                    // "-Djava.class.path=",
                    // "-verbose:jni"
                }
            );
            session.Env.PushLocalFrame(256);
            TestPrintInt(session.Env);
            session.Env.PopLocalFrame(jobject.NULL);
            session.Vm.DetachCurrentThread();
            

            _jvmLibrary = JreLibrary.Load(JreHome);
            
            session = JniSession.DetectExistingJvm(_jvmLibrary);
            session.Env.PushLocalFrame(256);
            TestPrintString(session.Env);
            TestException(session.Env);

            session.Env.PopLocalFrame(jobject.NULL);

        }

        private static unsafe void TestPrintInt(JNIEnv env)
        {
            var class_System = env.FindClass("java/lang/System");
            var class_PrintStream = env.FindClass("java/io/PrintStream");
            var system_out = env.GetStaticObjectField(class_System,
                env.GetStaticFieldID(class_System, "out", "Ljava/io/PrintStream;"));
            var printlnString = env.GetMethodID(class_PrintStream, "println", "(I)V");
            fixed (jvalue* args = new[] {new jvalue(42)})
            {
                env.CallObjectMethodA(system_out, printlnString, args);
            }
        }

        private static unsafe void TestException(JNIEnv env)
        {
            try
            {
                var jclass = env.FindClass("java/io/FileInputStream");
                var ctor = env.GetMethodID(jclass, "<init>", "(Ljava/lang/String;)V");
                var nowhere = env.NewStringAnsi("x:/nowhere");
                fixed (jvalue* args = new[] {new jvalue(nowhere)})
                {
                    env.NewObjectA(jclass, ctor, args);
                }
            
                env.EscalateException();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static unsafe void TestPrintString(JNIEnv env)
        {
            var class_System = env.FindClass("java/lang/System");
            var class_PrintStream = env.FindClass("java/io/PrintStream");
            var system_out = env.GetStaticObjectField(class_System,
                env.GetStaticFieldID(class_System, "out", "Ljava/io/PrintStream;"));
            var printlnString = env.GetMethodID(class_PrintStream, "println", "(Ljava/lang/String;)V");
            var string_helloJvm = env.NewStringAnsi("Hello JVM!");
            fixed (jvalue* args = new[] {new jvalue(string_helloJvm)})
            {
                env.CallObjectMethodA(system_out, printlnString, args);
            }
        }
    }
}