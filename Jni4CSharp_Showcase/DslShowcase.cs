﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using Jni4CSharp.Bindings;
using Jni4CSharp.CommonUtils;
using Jni4CSharp.Decorated;
using Jni4CSharp.JniUtils;

namespace Jni4CSharp_Showcase
{
    public class DslShowcase
    {
        private const string JreHome = "jre";
        private static DynamicLibrary _jvmLibrary = JreLibrary.Load(JreHome);

        public static void Main()
        {
            Console.WriteLine("RunShowCase");
            
            var runtime = JavaRuntime.DetectExisting(_jvmLibrary);
            Console.WriteLine("expected null: " + (runtime?.ToString() ?? "null"));
            

            runtime = JavaRuntime.Create(
                _jvmLibrary,
                new[]
                {
                    "-Xmx256m",
                    // "-Djava.class.path=",
                    // "-verbose:jni"
                }
            );
            runtime.PushLocalFrame(256);
            TestPrintInt(runtime);
            runtime.PopLocalFrame();
            runtime.DetachCurrentThread();

            _jvmLibrary = JreLibrary.Load(JreHome);

            runtime = JavaRuntime.DetectExisting(_jvmLibrary);
            runtime.PushLocalFrame(256);
            TestPrintString(runtime);
            TestException(runtime);
            TestReverseHello(runtime);
            TestReverseHelloStrings(runtime);
            TestReverseHelloDirectBuffer(runtime);
            TestReverseHelloException(runtime);
            runtime.PopLocalFrame();
        }

        private static void TestPrintInt(JavaRuntime env)
        {
            env.FindClass("java.lang.System")
                .GetStaticField("out", "Ljava/io/PrintStream;")
                .GetValue()
                .FetchObject()
                .GetInstanceMethod("println", "(I)V")
                .Invoke(new jvalue(42));
        }

        private static void TestException(JavaRuntime env)
        {
            try
            {
                env.FindClass("java.io.FileInputStream")
                    .GetConstructor("(Ljava/lang/String;)V")
                    .Invoke(new jvalue(env.AllocateStringAnsi("x:/nowhere")));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static void TestPrintString(JavaRuntime env)
        {
            env.FindClass("java.lang.System")
                .GetStaticField("out", "Ljava/io/PrintStream;")
                .GetValue()
                .FetchObject()
                .GetInstanceMethod("println", "(Ljava/lang/String;)V")
                .Invoke(new jvalue(env.AllocateStringAnsi("Hello JVM!")));
        }

        private unsafe delegate void hello(JNIEnv_* jenv, jclass cls, int x);

        private static unsafe void TestReverseHello(JavaRuntime env)
        {
            var classLoader = env.AddClassPath("classes");

            var javaShowcase = classLoader.LoadClass("JavaShowcase");

            javaShowcase.BeginRegisterNatives()
                .AddDelegate<hello>("(I)V", (jenv, jcls, x) => Console.WriteLine($@"hello({x})"))
                .Complete();

            javaShowcase.GetStaticMethod("sayHello", "()V").Invoke();
        }

        private unsafe delegate jstring helloStrings(JNIEnv_* jenv, jclass cls, jstring s);

        private static unsafe void TestReverseHelloStrings(JavaRuntime env)
        {
            var classLoader = env.AddClassPath("classes");

            var javaShowcase = classLoader.LoadClass("JavaShowcase");

            javaShowcase.BeginRegisterNatives()
                .AddDelegate<helloStrings>("(Ljava/lang/String;)Ljava/lang/String;", (jenv, jcls, s) =>
                {
                    var nestedEnv = new JNIEnv(jenv);
                    var text = nestedEnv.GetText(s);
                    return nestedEnv.NewStringAnsi("hello " + text);
                })
                .Complete();

            var result = javaShowcase.GetStaticMethod("sayHelloStrings", "(Ljava/lang/String;)Ljava/lang/String;")
                .Invoke(new jvalue(env.AllocateStringAnsi("lowercase")))
                .FetchString();
            Console.WriteLine(result);
        }

        private unsafe delegate jobject helloDirectBuffer(JNIEnv_* jenv, jclass cls);

        private static unsafe void TestReverseHelloDirectBuffer(JavaRuntime env)
        {
            var classLoader = env.AddClassPath("classes");

            var javaShowcase = classLoader.LoadClass("JavaShowcase");
            
            var buffer = env.FindClass("java.nio.ByteBuffer")
                .GetStaticMethod("allocateDirect", "(I)Ljava/nio/ByteBuffer;")
                .Invoke(new jvalue(512));
            var address = buffer.FetchDirectBufferAddress();
            
            javaShowcase.BeginRegisterNatives()
                .AddDelegate<helloDirectBuffer>("()Ljava/nio/ByteBuffer;", (jenv, cls) =>
                {
                    var message = Encoding.UTF8.GetBytes("hello lowercase");
                    Marshal.WriteInt16(address, (short) message.Length);
                    Marshal.Copy(message, 0, address + 2, message.Length);
                    return buffer.jValue.Ref;
                })
                .Complete();

            javaShowcase.GetStaticMethod("sayHelloDirectBuffer", "()V")
                .Invoke();
            var messageLen = Marshal.ReadInt16(address);
            var messageBytes = new byte[messageLen];
            Marshal.Copy(address + 2, messageBytes, 0, messageLen);
            Console.WriteLine(Encoding.UTF8.GetString(messageBytes));
        }

        private unsafe delegate void helloException(JNIEnv_* jenv, jclass cls);

        private static unsafe void TestReverseHelloException(JavaRuntime env)
        {
            var classLoader = env.AddClassPath("classes");

            var javaShowcase = classLoader.LoadClass("JavaShowcase");

            {
                javaShowcase.BeginRegisterNatives()
                    .AddDelegate<helloException>("()V", (jenv, cls) =>
                    {
                        var runtime = JavaRuntime.Wrap(new JNIEnv(jenv));
                        try
                        {
                            var zero = 0;
                            var x = 12 / zero;
                        }
                        catch (Exception e)
                        {
                            runtime.FindClass("java.lang.Exception")
                                .GetConstructor("(Ljava/lang/String;)V")
                                .Invoke(new jvalue(runtime.AllocateStringAnsi(e.ToString())))
                                .Throw();
                        }
                        Console.WriteLine("throwing WTF");
                    })
                    .Complete();

                var result = javaShowcase.GetStaticMethod("sayHelloException", "()Ljava/lang/String;")
                    .Invoke()
                    .FetchString();
                Console.WriteLine(result);                
            }
            
            {
            
                javaShowcase.BeginRegisterNatives()
                    .AddDelegate<helloException>("()V", (jenv, cls) =>
                    {
                        var runtime = JavaRuntime.Wrap(new JNIEnv(jenv));
                        try
                        {
                            var zero = 0;
                            var x = 12 / zero;
                        }
                        catch (Exception e)
                        {
                            runtime.FindClass("java.lang.Exception")
                                .ThrowNewAnsi(e.ToString());
                        }
                        Console.WriteLine("throwing WTF");
                    })
                    .Complete();

                var result = javaShowcase.GetStaticMethod("sayHelloException", "()Ljava/lang/String;")
                    .Invoke()
                    .FetchString();
                Console.WriteLine(result);                
            }
        }
    }
}