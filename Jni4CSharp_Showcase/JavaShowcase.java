
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class JavaShowcase {
    public static native void hello(int x);
    public static native String helloStrings(String s);
    public static native ByteBuffer helloDirectBuffer();
    public static native void helloException();

    public static void sayHello() {
        hello(42);
    }

    public static String sayHelloStrings(String s) {
        return helloStrings(s.toUpperCase());
    }

    public static void sayHelloDirectBuffer() {
        ByteBuffer byteBuffer = helloDirectBuffer();
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);

        byteBuffer.clear();
        short length = byteBuffer.getShort();
        byte[] b = new byte[length];
        byteBuffer.get(b);

        b = new String(b).toUpperCase().getBytes();

        byteBuffer.position(0);
        byteBuffer.putShort(length);
        byteBuffer.put(b);
    }

    public static String sayHelloException() {
        try {
            helloException();
            return "NO EXCEPTION";
        } catch (Exception e) {
            StringWriter b = new StringWriter();
            e.printStackTrace(new PrintWriter(b, true));
            return b.toString();
        }
    }
}
