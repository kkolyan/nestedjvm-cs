﻿using System.Collections.Generic;
using System.IO;
using Jni4CSharp.Bindings;
using Jni4CSharp.Decorated;
using Jni4CSharp.JniUtils;
using Xunit;

namespace Jni4CSharp_xUnit
{
    public class Tests
    {
        private JavaClassLoader _classLoader;

        public Tests()
        {
            var jvmLibrary = JreLibrary.Load(Path.GetFullPath("jre"));
            var runtime = JavaRuntime.DetectExisting(jvmLibrary);
            if (runtime == null)
            {
                runtime = JavaRuntime.Create(jvmLibrary, new List<string> { });
            }

            _classLoader = runtime.AddClassPath("classes");
        }

        [Fact]
        public void StaticMethodReturnsInt()
        {
            var result = _classLoader.LoadClass("Sample")
                .GetStaticMethod("StaticMethod", "()I")
                .Invoke();
            Assert.Equal(42, result.jValue.Int);
        }

        [Fact]
        public void ConstructorNotCrashes()
        {
            _classLoader.LoadClass("Sample")
                .GetConstructor("(I)V")
                .Invoke(new jvalue(41));
        }

        [Fact]
        public void ConstructorSetsField()
        {
            var result = _classLoader.LoadClass("Sample")
                .GetConstructor("(I)V")
                .Invoke(new jvalue(41))
                .FetchObject();
            Assert.Equal(41, result.GetInstanceField("InstanceField", "I").GetValue().jValue.Int);
        }

        [Fact]
        public void ConstructorSetFieldReturnedByInstanceMethod()
        {
            var result = _classLoader.LoadClass("Sample")
                .GetConstructor("(I)V")
                .Invoke(new jvalue(43))
                .FetchObject();
            Assert.Equal(43, result.GetInstanceMethod("InstanceMethod", "()I").Invoke().jValue.Int);
        }

        [Fact]
        public void FloatReturnedCorrectly()
        {
            var result = _classLoader.LoadClass("Sample")
                .GetStaticMethod("Sum", "(IJ)F")
                .Invoke(new jvalue(5), new jvalue(7));
            Assert.Equal(12f, result.jValue.Float);
        }
    }
}