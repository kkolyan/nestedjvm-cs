
public class Sample {

    public Sample(final int instanceField) {
        InstanceField = instanceField;
    }

    public static int StaticMethod() {
        return 42;
    }

    public int InstanceMethod() {
        return InstanceField;
    }

    public static String StaticStringMethod() {
        return "Hello";
    }

    public static float Sum(int a, long b) {
        return a + b;
    }

    public static boolean IntEquals(int a, int b) {
        return a == b;
    }

    // can test NPE
    public static String ToUpperCase(String message) {
        return message.toUpperCase();
    }

    public static String StaticField;

    // tests void return type as well
    public static void SetStaticField(String value) {
        StaticField = value;
    }

    public int InstanceField;

    public void SetInstanceField(int value) {
        InstanceField = value;
    }
}