using System.IO;

namespace Jni4CSharp_xUnit
{
    public class RunTestsHere
    {
        public static void Main()
        {
            ManualXUnitRunner.Main(new[] {Path.GetFullPath("Jni4CSharp_xUnit.dll")});
        }
    }
}