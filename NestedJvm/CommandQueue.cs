using System;
using System.IO;
using Jni4CSharp.Bindings;
using Jni4CSharp.Decorated;

namespace NestedJvm
{
    public class CommandQueue
    {
        public readonly string QueueName;
        public readonly JavaObject IncomingBuffer;
        public readonly IntPtr IncomingBufferPtr;
        public readonly int IncomingBufferSize;

        public CommandQueue(string queueName, JavaObject incomingBuffer, IntPtr incomingBufferPtr, int incomingBufferSize)
        {
            QueueName = queueName;
            IncomingBuffer = incomingBuffer;
            IncomingBufferPtr = incomingBufferPtr;
            IncomingBufferSize = incomingBufferSize;
        }

        public void Dump(BinaryWriter writer)
        {
            writer.WriteString(QueueName);
            writer.WriteInt32(IncomingBuffer.JniRef.GetBinaryValue());
            writer.WriteInt64(IncomingBufferPtr.ToInt64());
            writer.WriteInt32(IncomingBufferSize);
        }

        public CommandQueue(BinaryReader dump, JavaRuntime runtime)
        {
            QueueName = dump.ReadString();
            IncomingBuffer = runtime.WrapValue(new jobject(dump.ReadInt32())).FetchObject();
            IncomingBufferPtr = new IntPtr(dump.ReadInt64());
            IncomingBufferSize = dump.ReadInt32();
        }

        public override string ToString()
        {
            return $"{nameof(QueueName)}: {QueueName}, {nameof(IncomingBuffer)}: {IncomingBuffer}, {nameof(IncomingBufferPtr)}: {IncomingBufferPtr}, {nameof(IncomingBufferSize)}: {IncomingBufferSize}";
        }
    }
}