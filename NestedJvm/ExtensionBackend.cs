using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Jni4CSharp.Decorated;

namespace NestedJvm
{
    internal class ExtensionBackend
    {
        private readonly IDictionary<long, CommandQueue> _commandQueues =
            new Dictionary<long, CommandQueue>();

        private readonly IDictionary<string, MonitoringWindow> _monitoringWindows =
            new Dictionary<string, MonitoringWindow>();

        private readonly IDictionary<string, Action<CommandQueue>> _commandQueueHandlers =
            new Dictionary<string, Action<CommandQueue>>();

        private long _commandQueueCounter;

        public delegate JavaValue AllocateDirectBuffer(int capacity);
        
        public void Dump(BinaryWriter writer)
        {
            writer.WriteInt64(_commandQueueCounter);
            writer.WriteInt32(_commandQueues.Count);
            foreach (var entry in _commandQueues)
            {
                writer.WriteInt64(entry.Key);
                entry.Value.Dump(writer);
            }
            writer.WriteInt32(_monitoringWindows.Count);
            foreach (var entry in _monitoringWindows)
            {
                writer.WriteString(entry.Key);
                entry.Value.Dump(writer);
            }
        }

        public void Restore(BinaryReader dump, JavaRuntime runtime)
        {
            _commandQueueCounter = dump.ReadInt64();
            var commandQueuesCount = dump.ReadInt32();
            for (var i = 0; i < commandQueuesCount; i++)
            {
                var queueId = dump.ReadInt64();
                _commandQueues[queueId] = new CommandQueue(dump, runtime);
            }

            var monitoringWindowsCount = dump.ReadInt32();
            for (var i = 0; i < monitoringWindowsCount; i++)
            {
                var windowName = dump.ReadString();
                _monitoringWindows[windowName] = new MonitoringWindow(dump, runtime);
            }
        }

        public void Unload()
        {
            foreach (var commandQueueHandle in _commandQueues.Values)
            {
                commandQueueHandle.IncomingBuffer.DeleteGlobalRef();
            }

            foreach (var window in _monitoringWindows.Values)
            {
                window.OutgoingBuffer.DeleteGlobalRef();
            }
        }

        public long AllocateCommandQueue(string queueName, int bufferSize, AllocateDirectBuffer allocateDirectBuffer)
        {
            if (!_commandQueueHandlers.TryGetValue(queueName, out var processQueue))
            {
                throw new Exception("unknown command: " + queueName);
            }

            if (_commandQueues.Values.Any(x => x.QueueName == queueName))
            {
                throw new Exception("queue with name is already allocated: " + queueName);
            }

            var queueId = ++_commandQueueCounter;
            var buffer = allocateDirectBuffer(bufferSize).GoGlobal();
            _commandQueues[queueId] = new CommandQueue(
                queueName: queueName,
                incomingBuffer: buffer.GoGlobal().FetchObject(),
                incomingBufferPtr: buffer.FetchDirectBufferAddress(),
                incomingBufferSize: bufferSize
            );
            return queueId;
        }

        public CommandQueue ResolveCommandQueue(long queueId)
        {
            if (_commandQueues.TryGetValue(queueId, out var commandQueue))
            {
                return commandQueue;
            }

            throw new Exception("queue not found: " + queueId);
        }

        public void SubmitCommandQueue(long queueId)
        {
            if (!_commandQueues.TryGetValue(queueId, out var commandQueue))
            {
                throw new Exception("queue not found: " + queueId);
            }

            if (!_commandQueueHandlers.TryGetValue(commandQueue.QueueName, out var queueHandler))
            {
                throw new Exception("queue handler not found for queue: " + commandQueue);
            }

            queueHandler(commandQueue);
        }

        public MonitoringWindow LookupMonitoringWindow(string windowName)
        {
            if (!_monitoringWindows.TryGetValue(windowName, out var window))
            {
                return null;
            }

            return window;
        }

        public void AddCommandQueueHandler(string queueName, Action<CommandQueue> handleCommand)
        {
            if (_commandQueueHandlers.ContainsKey(queueName))
            {
                throw new Exception("command handler already defined for queue: " + queueName);
            }

            _commandQueueHandlers[queueName] = handleCommand;
        }

        public MonitoringWindow AllocateMonitoringWindow(
            string windowName,
            int bufferSize,
            AllocateDirectBuffer allocateDirectBuffer
        )
        {
            if (_monitoringWindows.ContainsKey(windowName))
            {
                throw new Exception("monitoring window already exist: " + windowName);
            }

            var buffer = allocateDirectBuffer(bufferSize).GoGlobal();
            
            var monitoringWindow = _monitoringWindows[windowName] = new MonitoringWindow(
                windowName: windowName,
                outgoingBuffer: buffer.GoGlobal().FetchObject(),
                outgoingBufferPtr: buffer.FetchDirectBufferAddress(),
                outgoingBufferSize: bufferSize
            );
            return monitoringWindow;
        }
    }
}