using System;
using System.IO;
using Jni4CSharp.Bindings;
using Jni4CSharp.Decorated;

namespace NestedJvm
{
    public class MonitoringWindow
    {
        public readonly string WindowName;
        public readonly JavaObject OutgoingBuffer;
        public readonly IntPtr OutgoingBufferPtr;
        public readonly int OutgoingBufferSize;

        public MonitoringWindow(string windowName, JavaObject outgoingBuffer, IntPtr outgoingBufferPtr, int outgoingBufferSize)
        {
            WindowName = windowName;
            OutgoingBuffer = outgoingBuffer;
            OutgoingBufferPtr = outgoingBufferPtr;
            OutgoingBufferSize = outgoingBufferSize;
        }

        public void Dump(BinaryWriter writer)
        {
            writer.WriteString(WindowName);
            writer.WriteInt32(OutgoingBuffer.JniRef.GetBinaryValue());
            writer.WriteInt64(OutgoingBufferPtr.ToInt64());
            writer.WriteInt32(OutgoingBufferSize);
        }

        public MonitoringWindow(BinaryReader dump, JavaRuntime runtime)
        {
            WindowName = dump.ReadString();
            OutgoingBuffer = runtime.WrapValue(new jobject(dump.ReadInt32())).FetchObject();
            OutgoingBufferPtr = new IntPtr(dump.ReadInt64());
            OutgoingBufferSize = dump.ReadInt32();
        }
    }
}