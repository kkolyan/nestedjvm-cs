using System.IO;

namespace NestedJvm
{
    public interface IForeignExtensionLifecycle
    {

        void Start();
        
        void Update();
        
        void LateUpdate();
        
        void Unload();

        void DumpAndDispose(BinaryWriter dump);
    }
}