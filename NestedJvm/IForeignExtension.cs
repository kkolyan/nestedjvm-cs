using System;
using System.IO;

namespace NestedJvm
{
    public interface IForeignExtension
    {
        void AddCommandQueueHandler(string queueName, Action<CommandQueue> processQueue);

        MonitoringWindow AllocateMonitoringWindow(string windowName, int bufferSize);
    }
}