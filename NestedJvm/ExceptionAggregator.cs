using System;
using System.Collections.Generic;
using System.Linq;

namespace NestedJvm
{
    public static class ExceptionAggregator
    {
        public static void InvokeAll(params Action[] actions)
        {
            var exceptions = new List<Exception>();

            foreach (var action in actions)
            {
                try
                {
                    action.Invoke();
                }
                catch (Exception e)
                {
                    exceptions.Add(e);
                }
            }

            if (exceptions.Count <= 0)
            {
                return;
            }

            if (exceptions.Count == 1)
            {
                throw exceptions.Single();
            }

            throw new AggregateException(exceptions);
        }
    }
}