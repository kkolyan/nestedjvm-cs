using System.IO;

namespace NestedJvm
{
    public static class BinaryWriterUtils
    {
        public static void WriteInt64(this BinaryWriter writer, long value)
        {
            writer.Write(value);
        }

        public static void WriteInt32(this BinaryWriter writer, int value)
        {
            writer.Write(value);
        }

        public static void WriteString(this BinaryWriter writer, string value)
        {
            writer.Write(value);
        }
    }
}