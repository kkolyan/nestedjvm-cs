using System;
using System.IO;
using System.Threading;
using Jni4CSharp.Bindings;
using Jni4CSharp.Decorated;

namespace NestedJvm
{
    public class JavaPoweredExtension : IForeignExtension, IForeignExtensionLifecycle
    {
        private readonly ExtensionBackend _backend;
        private readonly JavaRuntime _runtime;
        private readonly JavaFunction _start;
        private readonly JavaFunction _stop;
        private readonly JavaFunction _update;
        private readonly JavaFunction _lateUpdate;

        private readonly ThreadLocal<bool> _validThreadFlag = new ThreadLocal<bool>();
        private readonly JavaClassLoader _classLoader;
        private readonly JavaObject _instance;

        public static JavaPoweredExtension Load(JavaRuntime runtime,
            string javaFactoryClassName,
            string[] classPath)
        {
            var classLoader = runtime.AddClassPath(classPath);
            var extensionInstance = classLoader.LoadClass("com.nplekhanov.riseofj.nestedjvm.api4host.ExposedJni")
                .GetStaticMethod(
                    "createHostExtension",
                    "(Ljava/lang/String;)Lcom/nplekhanov/riseofj/nestedjvm/api/HostExtension;"
                )
                .Invoke(new jvalue(runtime.AllocateStringAnsi(javaFactoryClassName)))
                .GoGlobal()
                .FetchObject();
            return new JavaPoweredExtension(runtime, classLoader, extensionInstance, new ExtensionBackend());
        }

        public static JavaPoweredExtension Restore(JavaRuntime runtime, BinaryReader dump)
        {
            var classLoaderRef = new jobject(dump.ReadInt32());
            var extensionInstanceRef = new jobject(dump.ReadInt32());
            
            var backend = new ExtensionBackend();
            backend.Restore(dump, runtime);
            
            var classLoaderInstance = runtime.WrapValue(classLoaderRef).FetchObject();
            var extensionInstance = runtime.WrapValue(extensionInstanceRef).FetchObject();
            
            var classLoader = JavaClassLoader.Wrap(runtime, classLoaderInstance);
            classLoader.SetContextClassLoader();
            return new JavaPoweredExtension(runtime, classLoader, extensionInstance, backend);
        }

        private JavaPoweredExtension(
            JavaRuntime runtime,
            JavaClassLoader classLoader, JavaObject instance, ExtensionBackend extensionBackend)
        {
            _runtime = runtime;
            _classLoader = classLoader;
            _instance = instance;

            _backend = extensionBackend;

            _start = instance.GetInstanceMethod("start", "()V");
            _stop = instance.GetInstanceMethod("stop", "()V");
            _update = instance.GetInstanceMethod("update", "()V");
            _lateUpdate = instance.GetInstanceMethod("lateUpdate", "()V");

            RegisterNatives(runtime, _classLoader);
        }

        public void DumpAndDispose(BinaryWriter dump)
        {
            dump.Write(_classLoader.ObjectInstance.JniRef.GetBinaryValue());
            dump.Write(_instance.JniRef.GetBinaryValue());
            _backend.Dump(dump);
            GetProvidedJniClass(_classLoader).UnregisterNatives();
        }

        private JavaValue _allocateDirectBuffer(int capacity)
        {
            return _runtime.FindClass("java/nio/ByteBuffer")
                .GetStaticMethod("allocateDirect", "(I)Ljava/nio/ByteBuffer;")
                .Invoke(new jvalue(capacity));
        }

        public void Start()
        {
            _start.Invoke();
        }

        public void Unload()
        {
            _checkThread();

            ExceptionAggregator.InvokeAll(
                () => _stop.Invoke(),
                _backend.Unload,
                _classLoader.ReleaseGlobalRefs,
                _runtime.DetachCurrentThread
            );
        }

        public void AddCommandQueueHandler(string queueName, Action<CommandQueue> processQueue)
        {
            _checkThread();
            _backend.AddCommandQueueHandler(queueName, processQueue);
        }

        public MonitoringWindow AllocateMonitoringWindow(string windowName, int bufferSize)
        {
            _checkThread();

            // it's called outside of JVM stack frames, so create stack frame to release local references right after.

            _runtime.PushLocalFrame(16);
            try
            {
                return _backend.AllocateMonitoringWindow(windowName, bufferSize, _allocateDirectBuffer);
            }
            finally
            {
                _runtime.PopLocalFrame();
            }
        }

        public void Update()
        {
            _checkThread();
            _update.Invoke();
        }

        public void LateUpdate()
        {
            _checkThread();
            _lateUpdate.Invoke();
        }

        private void _rethrowToJava(Exception e)
        {
            var illegalStateException = _runtime.FindClass("java.lang.IllegalStateException");
            illegalStateException.ThrowNewUtf8(e.ToString());
        }

        private void RegisterNatives(
            JavaRuntime runtime,
            JavaClassLoader classLoader)
        {
            GetProvidedJniClass(classLoader)
                .BeginRegisterNatives()
                .AddDelegate<allocateCommandQueue>(
                    "(Ljava/lang/String;)J",
                    (env, jclass, jqueueName, jsize) =>
                    {
                        try
                        {
                            _checkThread();
                            var queueName = runtime.WrapValue(jqueueName).FetchString();
                            return _backend.AllocateCommandQueue(queueName, jsize, _allocateDirectBuffer);
                        }
                        catch (Exception e)
                        {
                            _rethrowToJava(e);
                            return 0;
                        }
                    }
                )
                .AddDelegate<resolveCommandQueueBuffer>(
                    "(J)Ljava/nio/ByteBuffer;",
                    (env, jclass, queueId) =>
                    {
                        try
                        {
                            _checkThread();
                            return _backend.ResolveCommandQueue(queueId).IncomingBuffer.JniRef;
                        }
                        catch (Exception e)
                        {
                            _rethrowToJava(e);
                            return jobject.NULL;
                        }
                    }
                )
                .AddDelegate<submitCommandQueue>(
                    "(J)V",
                    (env, jclass, jqueueId) =>
                    {
                        try
                        {
                            _checkThread();
                            _backend.SubmitCommandQueue(jqueueId);
                        }
                        catch (Exception e)
                        {
                            _rethrowToJava(e);
                        }
                    }
                )
                .AddDelegate<lookupMonitoringWindowBuffer>(
                    "(Ljava/lang/String;)Ljava/nio/ByteBuffer;",
                    (env, jclass, jwindowName) =>
                    {
                        try
                        {
                            _checkThread();
                            var windowName = runtime.WrapValue(jwindowName).FetchString();
                            var window = _backend.LookupMonitoringWindow(windowName);
                            return window?.OutgoingBuffer?.JniRef ?? jobject.NULL;
                        }
                        catch (Exception e)
                        {
                            _rethrowToJava(e);
                            return jobject.NULL;
                        }
                    }
                )
                .Complete();
        }

        private static JavaClass GetProvidedJniClass(JavaClassLoader classLoader)
        {
            return classLoader.LoadClass("com.nplekhanov.riseofj.nestedjvm.api4host.ProvidedJni");
        }

        private void _checkThread()
        {
            if (!_validThreadFlag.Value)
            {
                throw new Exception("wrong thread: " + Thread.CurrentThread.Name);
            }
        }

        // ReSharper disable InconsistentNaming to match JVM names

        private delegate long allocateCommandQueue(IntPtr env, jclass jclass, jstring queueName, int bufferSize);

        private delegate jobject resolveCommandQueueBuffer(IntPtr env, jclass jclass, long queueId);

        private delegate void submitCommandQueue(IntPtr env, jclass jclass, long queueId);

        private delegate jobject lookupMonitoringWindowBuffer(IntPtr env, jclass jclass, jstring windowName);

        // ReSharper restore InconsistentNaming
    }
}