# Summary

This repository is home for libraries to embed Java code into C# applications with ease.

## Jni4CSharp

C# library to interoperate with Java via Java Native Interface (JNI). More details in [Jni4CSharp/README.md](Jni4CSharp/README.md).

_Related modules:_
* **Jni4CSharp_xUnit** - unit tests.
* **JniCSharp_Showcase** - examples of use.
* **InteropCTest** - examples of calling C from C#.

## NestedJvm
Draft of C# client library to interact with embedded Java application that 
implements [Nested JVM API](https://bitbucket.org/kkolyan/nestedjvm-api.git). Relies on Jni4CSharp.
 